import 'package:flutter/material.dart';

bool isPasswordValid(String password) {
  /*
        Here is a description about the regex used below
                    ^                 # start-of-string
                    (?=.*[0-9])       # a digit must occur at least once
                    (?=.*[a-z])       # a lower case letter must occur at least once
                    (?=.*[A-Z])       # an upper case letter must occur at least once
                    (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
                    (?=\\S+$)         # no whitespace allowed in the entire string
                    .{4,}             # anything, at least six places though
                    $                 # end-of-string
        */
  const String PASSWORD_PATTERN = '^(?=.*[0-9])(?=\\S+\$).{5,}\$';
  final regExp = RegExp(PASSWORD_PATTERN);
  return regExp.hasMatch(password);
}

String getInitials(fullName) {
  List<String> names = fullName.split(" ");
  String initials = "";
  int numWords = 2;

  if (fullName.trim().isEmpty) return '';
  if (numWords < names.length) {
    numWords = names.length;
  }
  for (var i = 0; i < numWords; i++) {
    initials += '${names[i][0]}';
  }
  return initials;
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}

//check for if string is number
bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}

// //check null or empty for a string
// bool (String s) {
//   if (s == null) {
//     return false;
//   }
//   return double.tryParse(s) != null;
// }
