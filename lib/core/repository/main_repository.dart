import 'dart:convert';
import 'dart:io';

import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/enums/common_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/core/services/api.dart';
import 'package:app/core/services/media_service.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MainRepository implements IMainRepository {
  final ApiClient apiClient;
  MediaService mediaService;

  MainRepository({@required this.apiClient}) : assert(apiClient != null) {
    mediaService = MediaService();
  }

  @override
  Future<File> getMedia(ImageSource imageSource, String mediaType) {
    return mediaService.getMedia(imageSource, mediaType);
  }

  @override
  Future<File> getCroppedImage(File imageFile) {
    return mediaService.getCropeImage(imageFile);
  }

  @override
  Future<User> editUser(Map<String, dynamic> user) async {
    final response = await apiClient.request(
      requestType: RequestType.PATCH,
      path: '/users/${user['id']}',
      data: user,
    );
    return User.fromJson(response);
  }

  @override
  Future<Course> createCourse(Map<String, dynamic> course) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/courses',
      data: course,
    );
    return Course.fromJson(response);
  }

  @override
  Future<ListResponse<CourseCategory>> getCategories() async {
    final response = await apiClient.request(
      requestType: RequestType.GET,
      path: '/course-categories',
    );
    return ListResponse<CourseCategory>.fromJson(response);
  }

  @override
  Future<ListResponse<Course>> getCourses(String userId) async {
    final response = await apiClient.request(
      requestType: RequestType.GET,
      path: '/get-courses/$userId',
    );
    return ListResponse<Course>.fromJson(response);
  }

  @override
  Future<ListResponse<UserCourses>> getUserCourses(String userId) async {
    final response = await apiClient.request(
        requestType: RequestType.GET,
        path: '/user-courses',
        queryParameters: {'userId': userId});
    return ListResponse<UserCourses>.fromJson(response);
  }

  @override
  Future<Lesson> addLesson(Map<String, dynamic> lesson) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/lessons',
      data: lesson,
    );
    return Lesson.fromJson(response);
  }

  @override
  Future<LessonContent> addLessonContent(
      Map<String, dynamic> lessonContent) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/lesson-contents',
      data: lessonContent,
    );
    return LessonContent.fromJson(response);
  }

  @override
  Future<Course> editCourse(Map<String, dynamic> course) async {
    final response = await apiClient.request(
      requestType: RequestType.PATCH,
      path: '/courses/${course['id']}',
      data: course,
    );
    return Course.fromJson(response);
  }

  @override
  Future<Article> addArticle(Map<String, dynamic> article) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/article',
      data: article,
    );
    return Article.fromJson(response);
  }

  @override
  Future<Question> addQuestion(Map<String, dynamic> question) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/questions',
      data: question,
    );
    return Question.fromJson(response);
  }

  @override
  Future<Rating> rate(Map<String, dynamic> rating) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/ratings',
      data: rating,
    );
    return Rating.fromJson(response);
  }

  @override
  Future<UserCourses> enrollCourse(
      Map<String, dynamic> courseEnrollPayload) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/course-enroll',
      data: courseEnrollPayload,
    );
    return UserCourses.fromJson(response);
  }

  @override
  Future<ListResponse<Student>> getStudents(String courseId) async {
    final response = await apiClient.request(
        requestType: RequestType.GET,
        path: '/get-submissions',
        queryParameters: {'courseId': courseId});
    return ListResponse<Student>.fromJson(response);
  }

  @override
  Future<ListResponse<SubmissionResponse>> getUserCourseSubmissions(
      String courseId, String userId) async {
    final response = await apiClient.request(
        requestType: RequestType.GET,
        path: '/get-submissions',
        queryParameters: {'courseId': courseId, 'userId': userId});
    return ListResponse<SubmissionResponse>.fromJson(response);
  }

  @override
  Future<List<BatchResponse<QuestionSubmission>>> sendSubmissions(
      Map<String, dynamic> submissions) async {
    final response = await apiClient.batchRequest(
      data: submissions,
    );
    return response
        .map<BatchResponse<QuestionSubmission>>(
            (i) => BatchResponse<QuestionSubmission>.fromJson(i))
        .toList();
  }
}
