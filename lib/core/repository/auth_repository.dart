import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/enums/common_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/core/services/api.dart';
import 'package:meta/meta.dart';

class AuthRepository implements IAuthRepository {
  final ApiClient apiClient;
  AuthRepository({@required this.apiClient}) : assert(apiClient != null);

  @override
  Future<User> login(String email, String password) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/authentication',
      data: {
        "strategy": "local",
        "email": email,
        "password": password,
      },
    );
    final loginResponse = LoginResponse.fromJson(response);
    User user = loginResponse.user;
    user = user.copyWith(accessToken: loginResponse.accessToken);
    return user;
  }

  @override
  Future<User> signup(Map<String, dynamic> signupPayload) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/users',
      data: signupPayload,
    );
    return User.fromJson(response);
  }

  @override
  Future<User> getUser(String id) async {
    final response = await apiClient.request(
      requestType: RequestType.GET,
      path: '/users/$id',
    );
    return User.fromJson(response);
  }

  @override
  Future<User> verifyEmail(String email, String verificationCode) async {
    final response = await apiClient.request(
      requestType: RequestType.POST,
      path: '/authmanagement',
      data: {
        "action": "verifySignupShort",
        "value": {
          "user": {
            "email": email,
          },
          "token": verificationCode
        }
      },
    );
    return User.fromJson(response);
  }
}
