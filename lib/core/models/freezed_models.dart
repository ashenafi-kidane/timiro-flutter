import 'package:freezed_annotation/freezed_annotation.dart';
part 'freezed_models.freezed.dart';
part 'freezed_models.g.dart';

@freezed
abstract class User with _$User {
  factory User(
      {@JsonKey(name: '_id') String id,
      String email,
      String firstName,
      String lastName,
      String gender,
      String phone,
      String address,
      String profileImageId,
      String language,
      String accessToken,
      bool isVerified,
      Files profileImage,
      DateTime createdAt,
      DateTime updatedAt}) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

@freezed
abstract class Course with _$Course {
  factory Course({
    @JsonKey(name: '_id') String id,
    String title,
    String description,
    String categoryId,
    double price,
    String status,
    String createdBy,
    String visibility,
    List<String> instructorIds,
    String accessCode,
    String courseImageId,
    String discussionRoomID,
    String classRoomId,
    RatingResponse rating,
    Files courseImage,
    CourseCategory category,
    List<Lesson> lessons,
    List<User> instructors,
    int studentsCount,
    int lessonsCount,
    DateTime createdAt,
    DateTime updatedAt,
  }) = _Course;

  factory Course.fromJson(Map<String, dynamic> json) => _$CourseFromJson(json);
}

@freezed
abstract class CourseCategory with _$CourseCategory {
  factory CourseCategory(
      {@JsonKey(name: '_id') String id,
      String name,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) = _CourseCategory;

  factory CourseCategory.fromJson(Map<String, dynamic> json) =>
      _$CourseCategoryFromJson(json);
}

@freezed
abstract class Lesson with _$Lesson {
  factory Lesson(
      {@JsonKey(name: '_id') String id,
      String courseId,
      String title,
      String description,
      List<LessonContent> lessonContents,
      DateTime createdAt,
      DateTime updatedAt}) = _Lesson;

  factory Lesson.fromJson(Map<String, dynamic> json) => _$LessonFromJson(json);
}

@freezed
abstract class LessonContent with _$LessonContent {
  factory LessonContent(
      {@JsonKey(name: '_id') String id,
      String lessonId,
      String title,
      String description,
      String type,
      int order,
      String fileId,
      String videoUrl,
      List<Article> articles,
      List<Question> questions,
      Files video,
      DateTime createdAt,
      DateTime updatedAt}) = _LessonContent;

  factory LessonContent.fromJson(Map<String, dynamic> json) =>
      _$LessonContentFromJson(json);
}

@freezed
abstract class ClassRoom with _$ClassRoom {
  factory ClassRoom(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String schoolId,
      List<String> courseIds,
      DateTime createdAt,
      DateTime updatedAt}) = _ClassRoom;

  factory ClassRoom.fromJson(Map<String, dynamic> json) =>
      _$ClassRoomFromJson(json);
}

@freezed
abstract class School with _$School {
  factory School(
      {@JsonKey(name: '_id') String id,
      String name,
      String about,
      List<String> classroomIds,
      List<String> instructorIds,
      DateTime createdAt,
      DateTime updatedAt}) = _School;

  factory School.fromJson(Map<String, dynamic> json) => _$SchoolFromJson(json);
}

@freezed
abstract class ChatRoom with _$ChatRoom {
  factory ChatRoom(
      {@JsonKey(name: '_id') String id,
      String name,
      String status,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) = _ChatRoom;

  factory ChatRoom.fromJson(Map<String, dynamic> json) =>
      _$ChatRoomFromJson(json);
}

@freezed
abstract class Files with _$Files {
  factory Files(
      {@JsonKey(name: '_id') String id,
      String name,
      String originalName,
      String contentType,
      String url,
      DateTime createdAt,
      DateTime updatedAt}) = _Files;

  factory Files.fromJson(Map<String, dynamic> json) => _$FilesFromJson(json);
}

@freezed
abstract class Question with _$Question {
  factory Question(
      {@JsonKey(name: '_id') String id,
      String lessonContentId,
      String textQuestion,
      List<String> fileQuestionIds,
      int order,
      double point,
      String questionType,
      List<String> multipleChoices,
      String textAnswer,
      List<String> fileAnswerIds,
      DateTime createdAt,
      DateTime updatedAt}) = _Question;

  factory Question.fromJson(Map<String, dynamic> json) =>
      _$QuestionFromJson(json);
}

@freezed
abstract class Article with _$Article {
  factory Article(
      {@JsonKey(name: '_id') String id,
      String title,
      String paragraph,
      int order,
      List<String> fileIds,
      List<Files> file,
      DateTime createdAt,
      DateTime updatedAt}) = _Article;

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);
}

@freezed
abstract class Rating with _$Rating {
  factory Rating(
      {@JsonKey(name: '_id') String id,
      double value,
      String ratedBy,
      String ratingFor,
      DateTime createdAt,
      DateTime updatedAt}) = _Rating;

  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);
}

@freezed
abstract class Message with _$Message {
  factory Message(
      {@JsonKey(name: '_id') String id,
      String sender,
      String text,
      String chatroomId,
      String fileId,
      DateTime createdAt,
      DateTime updatedAt}) = _Message;

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);
}

@freezed
abstract class QuestionSubmission with _$QuestionSubmission {
  factory QuestionSubmission(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String lessonId,
      String lessonContentId,
      String questionId,
      int choiceAnswer,
      String textAnswer,
      List<String> fileAnswerIds,
      double resultPoint,
      Question question,
      DateTime createdAt,
      DateTime updatedAt}) = _QuestionSubmission;

  factory QuestionSubmission.fromJson(Map<String, dynamic> json) =>
      _$QuestionSubmissionFromJson(json);
}

@freezed
abstract class LessonContentSubmission with _$LessonContentSubmission {
  factory LessonContentSubmission({
    String lessonContentId,
    LessonContent lessonContent,
    List<QuestionSubmission> submissions,
  }) = _LessonContentSubmission;

  factory LessonContentSubmission.fromJson(Map<String, dynamic> json) =>
      _$LessonContentSubmissionFromJson(json);
}

@freezed
abstract class SubmissionResponse with _$SubmissionResponse {
  factory SubmissionResponse({
    String lessonId,
    Lesson lesson,
    List<LessonContentSubmission> lessonContents,
  }) = _SubmissionResponse;

  factory SubmissionResponse.fromJson(Map<String, dynamic> json) =>
      _$SubmissionResponseFromJson(json);
}

@freezed
abstract class Student with _$Student {
  factory Student({
    User profile,
    List<SubmissionResponse> submissionResponses,
  }) = _Student;

  factory Student.fromJson(Map<String, dynamic> json) =>
      _$StudentFromJson(json);
}

@freezed
abstract class LoginResponse with _$LoginResponse {
  factory LoginResponse({
    String accessToken,
    User user,
  }) = _LoginResponse;

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);
}

@freezed
abstract class UserCourses with _$UserCourses {
  factory UserCourses(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String relation,
      Course course,
      double courseRating,
      DateTime createdAt,
      DateTime updatedAt}) = _UserCourses;

  factory UserCourses.fromJson(Map<String, dynamic> json) =>
      _$UserCoursesFromJson(json);
}

@freezed
abstract class RatingResponse with _$RatingResponse {
  factory RatingResponse({double value, int totalRates}) = _RatingResponse;

  factory RatingResponse.fromJson(Map<String, dynamic> json) =>
      _$RatingResponseFromJson(json);
}

@freezed
abstract class ListResponse<T> with _$ListResponse<T> {
  factory ListResponse({
    int total,
    int limit,
    int skip,
    @DataConverter() List<T> data,
  }) = _ListResponse<T>;

  factory ListResponse.fromJson(Map<String, dynamic> json) =>
      _$ListResponseFromJson<T>(json);
}

@freezed
abstract class BatchResponse<T> with _$BatchResponse<T> {
  factory BatchResponse({
    String status,
    @DataConverter() T value,
  }) = _BatchResponse<T>;

  factory BatchResponse.fromJson(Map<String, dynamic> json) =>
      _$BatchResponseFromJson<T>(json);
}

// T _dataFromJson<T>(Map<String, dynamic> input) => input['value'] as T;

// Map<String, dynamic> _dataToJson<T>(T input) => {'value': input};

@freezed
abstract class Item with _$Item {
  factory Item(String name) = _Item;

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);
}

class DataConverter<T> implements JsonConverter<T, Object> {
  const DataConverter();

  //N:B - if this switch case checking is tomuch, use looping from the repository
  //from the List<Map<String, dynamic>> response. e.g
  // Iterable l = json.decode(response.body);
  // List<Post> posts = List<Post>.from(l).map((Map model)=> Post.fromJson(model)).toList();

  @override
  T fromJson(Object json) {
    switch (T) {
      case CourseCategory:
        return CourseCategory.fromJson(json) as T;
        break;
      case Course:
        return Course.fromJson(json) as T;
      case UserCourses:
        return UserCourses.fromJson(json) as T;
        break;
      case Student:
        return Student.fromJson(json) as T;
        break;
      case SubmissionResponse:
        return SubmissionResponse.fromJson(json) as T;
        break;
      case QuestionSubmission:
        return QuestionSubmission.fromJson(json) as T;
        break;
      default:
    }
  }

  @override
  Object toJson(T object) {
    return object;
  }
}
