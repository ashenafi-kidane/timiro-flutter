// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'freezed_models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
User _$UserFromJson(Map<String, dynamic> json) {
  return _User.fromJson(json);
}

/// @nodoc
class _$UserTearOff {
  const _$UserTearOff();

// ignore: unused_element
  _User call(
      {@JsonKey(name: '_id') String id,
      String email,
      String firstName,
      String lastName,
      String gender,
      String phone,
      String address,
      String profileImageId,
      String language,
      String accessToken,
      bool isVerified,
      Files profileImage,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _User(
      id: id,
      email: email,
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      phone: phone,
      address: address,
      profileImageId: profileImageId,
      language: language,
      accessToken: accessToken,
      isVerified: isVerified,
      profileImage: profileImage,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  User fromJson(Map<String, Object> json) {
    return User.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $User = _$UserTearOff();

/// @nodoc
mixin _$User {
  @JsonKey(name: '_id')
  String get id;
  String get email;
  String get firstName;
  String get lastName;
  String get gender;
  String get phone;
  String get address;
  String get profileImageId;
  String get language;
  String get accessToken;
  bool get isVerified;
  Files get profileImage;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $UserCopyWith<User> get copyWith;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String email,
      String firstName,
      String lastName,
      String gender,
      String phone,
      String address,
      String profileImageId,
      String language,
      String accessToken,
      bool isVerified,
      Files profileImage,
      DateTime createdAt,
      DateTime updatedAt});

  $FilesCopyWith<$Res> get profileImage;
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;

  @override
  $Res call({
    Object id = freezed,
    Object email = freezed,
    Object firstName = freezed,
    Object lastName = freezed,
    Object gender = freezed,
    Object phone = freezed,
    Object address = freezed,
    Object profileImageId = freezed,
    Object language = freezed,
    Object accessToken = freezed,
    Object isVerified = freezed,
    Object profileImage = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      email: email == freezed ? _value.email : email as String,
      firstName: firstName == freezed ? _value.firstName : firstName as String,
      lastName: lastName == freezed ? _value.lastName : lastName as String,
      gender: gender == freezed ? _value.gender : gender as String,
      phone: phone == freezed ? _value.phone : phone as String,
      address: address == freezed ? _value.address : address as String,
      profileImageId: profileImageId == freezed
          ? _value.profileImageId
          : profileImageId as String,
      language: language == freezed ? _value.language : language as String,
      accessToken:
          accessToken == freezed ? _value.accessToken : accessToken as String,
      isVerified:
          isVerified == freezed ? _value.isVerified : isVerified as bool,
      profileImage:
          profileImage == freezed ? _value.profileImage : profileImage as Files,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }

  @override
  $FilesCopyWith<$Res> get profileImage {
    if (_value.profileImage == null) {
      return null;
    }
    return $FilesCopyWith<$Res>(_value.profileImage, (value) {
      return _then(_value.copyWith(profileImage: value));
    });
  }
}

/// @nodoc
abstract class _$UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$UserCopyWith(_User value, $Res Function(_User) then) =
      __$UserCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String email,
      String firstName,
      String lastName,
      String gender,
      String phone,
      String address,
      String profileImageId,
      String language,
      String accessToken,
      bool isVerified,
      Files profileImage,
      DateTime createdAt,
      DateTime updatedAt});

  @override
  $FilesCopyWith<$Res> get profileImage;
}

/// @nodoc
class __$UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$UserCopyWith<$Res> {
  __$UserCopyWithImpl(_User _value, $Res Function(_User) _then)
      : super(_value, (v) => _then(v as _User));

  @override
  _User get _value => super._value as _User;

  @override
  $Res call({
    Object id = freezed,
    Object email = freezed,
    Object firstName = freezed,
    Object lastName = freezed,
    Object gender = freezed,
    Object phone = freezed,
    Object address = freezed,
    Object profileImageId = freezed,
    Object language = freezed,
    Object accessToken = freezed,
    Object isVerified = freezed,
    Object profileImage = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_User(
      id: id == freezed ? _value.id : id as String,
      email: email == freezed ? _value.email : email as String,
      firstName: firstName == freezed ? _value.firstName : firstName as String,
      lastName: lastName == freezed ? _value.lastName : lastName as String,
      gender: gender == freezed ? _value.gender : gender as String,
      phone: phone == freezed ? _value.phone : phone as String,
      address: address == freezed ? _value.address : address as String,
      profileImageId: profileImageId == freezed
          ? _value.profileImageId
          : profileImageId as String,
      language: language == freezed ? _value.language : language as String,
      accessToken:
          accessToken == freezed ? _value.accessToken : accessToken as String,
      isVerified:
          isVerified == freezed ? _value.isVerified : isVerified as bool,
      profileImage:
          profileImage == freezed ? _value.profileImage : profileImage as Files,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_User implements _User {
  _$_User(
      {@JsonKey(name: '_id') this.id,
      this.email,
      this.firstName,
      this.lastName,
      this.gender,
      this.phone,
      this.address,
      this.profileImageId,
      this.language,
      this.accessToken,
      this.isVerified,
      this.profileImage,
      this.createdAt,
      this.updatedAt});

  factory _$_User.fromJson(Map<String, dynamic> json) =>
      _$_$_UserFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String email;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String gender;
  @override
  final String phone;
  @override
  final String address;
  @override
  final String profileImageId;
  @override
  final String language;
  @override
  final String accessToken;
  @override
  final bool isVerified;
  @override
  final Files profileImage;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'User(id: $id, email: $email, firstName: $firstName, lastName: $lastName, gender: $gender, phone: $phone, address: $address, profileImageId: $profileImageId, language: $language, accessToken: $accessToken, isVerified: $isVerified, profileImage: $profileImage, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _User &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.firstName, firstName) ||
                const DeepCollectionEquality()
                    .equals(other.firstName, firstName)) &&
            (identical(other.lastName, lastName) ||
                const DeepCollectionEquality()
                    .equals(other.lastName, lastName)) &&
            (identical(other.gender, gender) ||
                const DeepCollectionEquality().equals(other.gender, gender)) &&
            (identical(other.phone, phone) ||
                const DeepCollectionEquality().equals(other.phone, phone)) &&
            (identical(other.address, address) ||
                const DeepCollectionEquality()
                    .equals(other.address, address)) &&
            (identical(other.profileImageId, profileImageId) ||
                const DeepCollectionEquality()
                    .equals(other.profileImageId, profileImageId)) &&
            (identical(other.language, language) ||
                const DeepCollectionEquality()
                    .equals(other.language, language)) &&
            (identical(other.accessToken, accessToken) ||
                const DeepCollectionEquality()
                    .equals(other.accessToken, accessToken)) &&
            (identical(other.isVerified, isVerified) ||
                const DeepCollectionEquality()
                    .equals(other.isVerified, isVerified)) &&
            (identical(other.profileImage, profileImage) ||
                const DeepCollectionEquality()
                    .equals(other.profileImage, profileImage)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(firstName) ^
      const DeepCollectionEquality().hash(lastName) ^
      const DeepCollectionEquality().hash(gender) ^
      const DeepCollectionEquality().hash(phone) ^
      const DeepCollectionEquality().hash(address) ^
      const DeepCollectionEquality().hash(profileImageId) ^
      const DeepCollectionEquality().hash(language) ^
      const DeepCollectionEquality().hash(accessToken) ^
      const DeepCollectionEquality().hash(isVerified) ^
      const DeepCollectionEquality().hash(profileImage) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$UserCopyWith<_User> get copyWith =>
      __$UserCopyWithImpl<_User>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserToJson(this);
  }
}

abstract class _User implements User {
  factory _User(
      {@JsonKey(name: '_id') String id,
      String email,
      String firstName,
      String lastName,
      String gender,
      String phone,
      String address,
      String profileImageId,
      String language,
      String accessToken,
      bool isVerified,
      Files profileImage,
      DateTime createdAt,
      DateTime updatedAt}) = _$_User;

  factory _User.fromJson(Map<String, dynamic> json) = _$_User.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get email;
  @override
  String get firstName;
  @override
  String get lastName;
  @override
  String get gender;
  @override
  String get phone;
  @override
  String get address;
  @override
  String get profileImageId;
  @override
  String get language;
  @override
  String get accessToken;
  @override
  bool get isVerified;
  @override
  Files get profileImage;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$UserCopyWith<_User> get copyWith;
}

Course _$CourseFromJson(Map<String, dynamic> json) {
  return _Course.fromJson(json);
}

/// @nodoc
class _$CourseTearOff {
  const _$CourseTearOff();

// ignore: unused_element
  _Course call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String categoryId,
      double price,
      String status,
      String createdBy,
      String visibility,
      List<String> instructorIds,
      String accessCode,
      String courseImageId,
      String discussionRoomID,
      String classRoomId,
      RatingResponse rating,
      Files courseImage,
      CourseCategory category,
      List<Lesson> lessons,
      List<User> instructors,
      int studentsCount,
      int lessonsCount,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Course(
      id: id,
      title: title,
      description: description,
      categoryId: categoryId,
      price: price,
      status: status,
      createdBy: createdBy,
      visibility: visibility,
      instructorIds: instructorIds,
      accessCode: accessCode,
      courseImageId: courseImageId,
      discussionRoomID: discussionRoomID,
      classRoomId: classRoomId,
      rating: rating,
      courseImage: courseImage,
      category: category,
      lessons: lessons,
      instructors: instructors,
      studentsCount: studentsCount,
      lessonsCount: lessonsCount,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Course fromJson(Map<String, Object> json) {
    return Course.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Course = _$CourseTearOff();

/// @nodoc
mixin _$Course {
  @JsonKey(name: '_id')
  String get id;
  String get title;
  String get description;
  String get categoryId;
  double get price;
  String get status;
  String get createdBy;
  String get visibility;
  List<String> get instructorIds;
  String get accessCode;
  String get courseImageId;
  String get discussionRoomID;
  String get classRoomId;
  RatingResponse get rating;
  Files get courseImage;
  CourseCategory get category;
  List<Lesson> get lessons;
  List<User> get instructors;
  int get studentsCount;
  int get lessonsCount;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $CourseCopyWith<Course> get copyWith;
}

/// @nodoc
abstract class $CourseCopyWith<$Res> {
  factory $CourseCopyWith(Course value, $Res Function(Course) then) =
      _$CourseCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String categoryId,
      double price,
      String status,
      String createdBy,
      String visibility,
      List<String> instructorIds,
      String accessCode,
      String courseImageId,
      String discussionRoomID,
      String classRoomId,
      RatingResponse rating,
      Files courseImage,
      CourseCategory category,
      List<Lesson> lessons,
      List<User> instructors,
      int studentsCount,
      int lessonsCount,
      DateTime createdAt,
      DateTime updatedAt});

  $RatingResponseCopyWith<$Res> get rating;
  $FilesCopyWith<$Res> get courseImage;
  $CourseCategoryCopyWith<$Res> get category;
}

/// @nodoc
class _$CourseCopyWithImpl<$Res> implements $CourseCopyWith<$Res> {
  _$CourseCopyWithImpl(this._value, this._then);

  final Course _value;
  // ignore: unused_field
  final $Res Function(Course) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object categoryId = freezed,
    Object price = freezed,
    Object status = freezed,
    Object createdBy = freezed,
    Object visibility = freezed,
    Object instructorIds = freezed,
    Object accessCode = freezed,
    Object courseImageId = freezed,
    Object discussionRoomID = freezed,
    Object classRoomId = freezed,
    Object rating = freezed,
    Object courseImage = freezed,
    Object category = freezed,
    Object lessons = freezed,
    Object instructors = freezed,
    Object studentsCount = freezed,
    Object lessonsCount = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      categoryId:
          categoryId == freezed ? _value.categoryId : categoryId as String,
      price: price == freezed ? _value.price : price as double,
      status: status == freezed ? _value.status : status as String,
      createdBy: createdBy == freezed ? _value.createdBy : createdBy as String,
      visibility:
          visibility == freezed ? _value.visibility : visibility as String,
      instructorIds: instructorIds == freezed
          ? _value.instructorIds
          : instructorIds as List<String>,
      accessCode:
          accessCode == freezed ? _value.accessCode : accessCode as String,
      courseImageId: courseImageId == freezed
          ? _value.courseImageId
          : courseImageId as String,
      discussionRoomID: discussionRoomID == freezed
          ? _value.discussionRoomID
          : discussionRoomID as String,
      classRoomId:
          classRoomId == freezed ? _value.classRoomId : classRoomId as String,
      rating: rating == freezed ? _value.rating : rating as RatingResponse,
      courseImage:
          courseImage == freezed ? _value.courseImage : courseImage as Files,
      category:
          category == freezed ? _value.category : category as CourseCategory,
      lessons: lessons == freezed ? _value.lessons : lessons as List<Lesson>,
      instructors: instructors == freezed
          ? _value.instructors
          : instructors as List<User>,
      studentsCount: studentsCount == freezed
          ? _value.studentsCount
          : studentsCount as int,
      lessonsCount:
          lessonsCount == freezed ? _value.lessonsCount : lessonsCount as int,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }

  @override
  $RatingResponseCopyWith<$Res> get rating {
    if (_value.rating == null) {
      return null;
    }
    return $RatingResponseCopyWith<$Res>(_value.rating, (value) {
      return _then(_value.copyWith(rating: value));
    });
  }

  @override
  $FilesCopyWith<$Res> get courseImage {
    if (_value.courseImage == null) {
      return null;
    }
    return $FilesCopyWith<$Res>(_value.courseImage, (value) {
      return _then(_value.copyWith(courseImage: value));
    });
  }

  @override
  $CourseCategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CourseCategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
abstract class _$CourseCopyWith<$Res> implements $CourseCopyWith<$Res> {
  factory _$CourseCopyWith(_Course value, $Res Function(_Course) then) =
      __$CourseCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String categoryId,
      double price,
      String status,
      String createdBy,
      String visibility,
      List<String> instructorIds,
      String accessCode,
      String courseImageId,
      String discussionRoomID,
      String classRoomId,
      RatingResponse rating,
      Files courseImage,
      CourseCategory category,
      List<Lesson> lessons,
      List<User> instructors,
      int studentsCount,
      int lessonsCount,
      DateTime createdAt,
      DateTime updatedAt});

  @override
  $RatingResponseCopyWith<$Res> get rating;
  @override
  $FilesCopyWith<$Res> get courseImage;
  @override
  $CourseCategoryCopyWith<$Res> get category;
}

/// @nodoc
class __$CourseCopyWithImpl<$Res> extends _$CourseCopyWithImpl<$Res>
    implements _$CourseCopyWith<$Res> {
  __$CourseCopyWithImpl(_Course _value, $Res Function(_Course) _then)
      : super(_value, (v) => _then(v as _Course));

  @override
  _Course get _value => super._value as _Course;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object categoryId = freezed,
    Object price = freezed,
    Object status = freezed,
    Object createdBy = freezed,
    Object visibility = freezed,
    Object instructorIds = freezed,
    Object accessCode = freezed,
    Object courseImageId = freezed,
    Object discussionRoomID = freezed,
    Object classRoomId = freezed,
    Object rating = freezed,
    Object courseImage = freezed,
    Object category = freezed,
    Object lessons = freezed,
    Object instructors = freezed,
    Object studentsCount = freezed,
    Object lessonsCount = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Course(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      categoryId:
          categoryId == freezed ? _value.categoryId : categoryId as String,
      price: price == freezed ? _value.price : price as double,
      status: status == freezed ? _value.status : status as String,
      createdBy: createdBy == freezed ? _value.createdBy : createdBy as String,
      visibility:
          visibility == freezed ? _value.visibility : visibility as String,
      instructorIds: instructorIds == freezed
          ? _value.instructorIds
          : instructorIds as List<String>,
      accessCode:
          accessCode == freezed ? _value.accessCode : accessCode as String,
      courseImageId: courseImageId == freezed
          ? _value.courseImageId
          : courseImageId as String,
      discussionRoomID: discussionRoomID == freezed
          ? _value.discussionRoomID
          : discussionRoomID as String,
      classRoomId:
          classRoomId == freezed ? _value.classRoomId : classRoomId as String,
      rating: rating == freezed ? _value.rating : rating as RatingResponse,
      courseImage:
          courseImage == freezed ? _value.courseImage : courseImage as Files,
      category:
          category == freezed ? _value.category : category as CourseCategory,
      lessons: lessons == freezed ? _value.lessons : lessons as List<Lesson>,
      instructors: instructors == freezed
          ? _value.instructors
          : instructors as List<User>,
      studentsCount: studentsCount == freezed
          ? _value.studentsCount
          : studentsCount as int,
      lessonsCount:
          lessonsCount == freezed ? _value.lessonsCount : lessonsCount as int,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Course implements _Course {
  _$_Course(
      {@JsonKey(name: '_id') this.id,
      this.title,
      this.description,
      this.categoryId,
      this.price,
      this.status,
      this.createdBy,
      this.visibility,
      this.instructorIds,
      this.accessCode,
      this.courseImageId,
      this.discussionRoomID,
      this.classRoomId,
      this.rating,
      this.courseImage,
      this.category,
      this.lessons,
      this.instructors,
      this.studentsCount,
      this.lessonsCount,
      this.createdAt,
      this.updatedAt});

  factory _$_Course.fromJson(Map<String, dynamic> json) =>
      _$_$_CourseFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String title;
  @override
  final String description;
  @override
  final String categoryId;
  @override
  final double price;
  @override
  final String status;
  @override
  final String createdBy;
  @override
  final String visibility;
  @override
  final List<String> instructorIds;
  @override
  final String accessCode;
  @override
  final String courseImageId;
  @override
  final String discussionRoomID;
  @override
  final String classRoomId;
  @override
  final RatingResponse rating;
  @override
  final Files courseImage;
  @override
  final CourseCategory category;
  @override
  final List<Lesson> lessons;
  @override
  final List<User> instructors;
  @override
  final int studentsCount;
  @override
  final int lessonsCount;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Course(id: $id, title: $title, description: $description, categoryId: $categoryId, price: $price, status: $status, createdBy: $createdBy, visibility: $visibility, instructorIds: $instructorIds, accessCode: $accessCode, courseImageId: $courseImageId, discussionRoomID: $discussionRoomID, classRoomId: $classRoomId, rating: $rating, courseImage: $courseImage, category: $category, lessons: $lessons, instructors: $instructors, studentsCount: $studentsCount, lessonsCount: $lessonsCount, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Course &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.categoryId, categoryId) ||
                const DeepCollectionEquality()
                    .equals(other.categoryId, categoryId)) &&
            (identical(other.price, price) ||
                const DeepCollectionEquality().equals(other.price, price)) &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.createdBy, createdBy) ||
                const DeepCollectionEquality()
                    .equals(other.createdBy, createdBy)) &&
            (identical(other.visibility, visibility) ||
                const DeepCollectionEquality()
                    .equals(other.visibility, visibility)) &&
            (identical(other.instructorIds, instructorIds) ||
                const DeepCollectionEquality()
                    .equals(other.instructorIds, instructorIds)) &&
            (identical(other.accessCode, accessCode) ||
                const DeepCollectionEquality()
                    .equals(other.accessCode, accessCode)) &&
            (identical(other.courseImageId, courseImageId) ||
                const DeepCollectionEquality()
                    .equals(other.courseImageId, courseImageId)) &&
            (identical(other.discussionRoomID, discussionRoomID) ||
                const DeepCollectionEquality()
                    .equals(other.discussionRoomID, discussionRoomID)) &&
            (identical(other.classRoomId, classRoomId) ||
                const DeepCollectionEquality()
                    .equals(other.classRoomId, classRoomId)) &&
            (identical(other.rating, rating) ||
                const DeepCollectionEquality().equals(other.rating, rating)) &&
            (identical(other.courseImage, courseImage) ||
                const DeepCollectionEquality()
                    .equals(other.courseImage, courseImage)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)) &&
            (identical(other.lessons, lessons) ||
                const DeepCollectionEquality()
                    .equals(other.lessons, lessons)) &&
            (identical(other.instructors, instructors) ||
                const DeepCollectionEquality()
                    .equals(other.instructors, instructors)) &&
            (identical(other.studentsCount, studentsCount) ||
                const DeepCollectionEquality()
                    .equals(other.studentsCount, studentsCount)) &&
            (identical(other.lessonsCount, lessonsCount) ||
                const DeepCollectionEquality()
                    .equals(other.lessonsCount, lessonsCount)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(categoryId) ^
      const DeepCollectionEquality().hash(price) ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(createdBy) ^
      const DeepCollectionEquality().hash(visibility) ^
      const DeepCollectionEquality().hash(instructorIds) ^
      const DeepCollectionEquality().hash(accessCode) ^
      const DeepCollectionEquality().hash(courseImageId) ^
      const DeepCollectionEquality().hash(discussionRoomID) ^
      const DeepCollectionEquality().hash(classRoomId) ^
      const DeepCollectionEquality().hash(rating) ^
      const DeepCollectionEquality().hash(courseImage) ^
      const DeepCollectionEquality().hash(category) ^
      const DeepCollectionEquality().hash(lessons) ^
      const DeepCollectionEquality().hash(instructors) ^
      const DeepCollectionEquality().hash(studentsCount) ^
      const DeepCollectionEquality().hash(lessonsCount) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$CourseCopyWith<_Course> get copyWith =>
      __$CourseCopyWithImpl<_Course>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CourseToJson(this);
  }
}

abstract class _Course implements Course {
  factory _Course(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String categoryId,
      double price,
      String status,
      String createdBy,
      String visibility,
      List<String> instructorIds,
      String accessCode,
      String courseImageId,
      String discussionRoomID,
      String classRoomId,
      RatingResponse rating,
      Files courseImage,
      CourseCategory category,
      List<Lesson> lessons,
      List<User> instructors,
      int studentsCount,
      int lessonsCount,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Course;

  factory _Course.fromJson(Map<String, dynamic> json) = _$_Course.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get title;
  @override
  String get description;
  @override
  String get categoryId;
  @override
  double get price;
  @override
  String get status;
  @override
  String get createdBy;
  @override
  String get visibility;
  @override
  List<String> get instructorIds;
  @override
  String get accessCode;
  @override
  String get courseImageId;
  @override
  String get discussionRoomID;
  @override
  String get classRoomId;
  @override
  RatingResponse get rating;
  @override
  Files get courseImage;
  @override
  CourseCategory get category;
  @override
  List<Lesson> get lessons;
  @override
  List<User> get instructors;
  @override
  int get studentsCount;
  @override
  int get lessonsCount;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$CourseCopyWith<_Course> get copyWith;
}

CourseCategory _$CourseCategoryFromJson(Map<String, dynamic> json) {
  return _CourseCategory.fromJson(json);
}

/// @nodoc
class _$CourseCategoryTearOff {
  const _$CourseCategoryTearOff();

// ignore: unused_element
  _CourseCategory call(
      {@JsonKey(name: '_id') String id,
      String name,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _CourseCategory(
      id: id,
      name: name,
      type: type,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  CourseCategory fromJson(Map<String, Object> json) {
    return CourseCategory.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $CourseCategory = _$CourseCategoryTearOff();

/// @nodoc
mixin _$CourseCategory {
  @JsonKey(name: '_id')
  String get id;
  String get name;
  String get type;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $CourseCategoryCopyWith<CourseCategory> get copyWith;
}

/// @nodoc
abstract class $CourseCategoryCopyWith<$Res> {
  factory $CourseCategoryCopyWith(
          CourseCategory value, $Res Function(CourseCategory) then) =
      _$CourseCategoryCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String type,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$CourseCategoryCopyWithImpl<$Res>
    implements $CourseCategoryCopyWith<$Res> {
  _$CourseCategoryCopyWithImpl(this._value, this._then);

  final CourseCategory _value;
  // ignore: unused_field
  final $Res Function(CourseCategory) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object type = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      type: type == freezed ? _value.type : type as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$CourseCategoryCopyWith<$Res>
    implements $CourseCategoryCopyWith<$Res> {
  factory _$CourseCategoryCopyWith(
          _CourseCategory value, $Res Function(_CourseCategory) then) =
      __$CourseCategoryCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String type,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$CourseCategoryCopyWithImpl<$Res>
    extends _$CourseCategoryCopyWithImpl<$Res>
    implements _$CourseCategoryCopyWith<$Res> {
  __$CourseCategoryCopyWithImpl(
      _CourseCategory _value, $Res Function(_CourseCategory) _then)
      : super(_value, (v) => _then(v as _CourseCategory));

  @override
  _CourseCategory get _value => super._value as _CourseCategory;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object type = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_CourseCategory(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      type: type == freezed ? _value.type : type as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_CourseCategory implements _CourseCategory {
  _$_CourseCategory(
      {@JsonKey(name: '_id') this.id,
      this.name,
      this.type,
      this.createdAt,
      this.updatedAt});

  factory _$_CourseCategory.fromJson(Map<String, dynamic> json) =>
      _$_$_CourseCategoryFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String name;
  @override
  final String type;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'CourseCategory(id: $id, name: $name, type: $type, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CourseCategory &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$CourseCategoryCopyWith<_CourseCategory> get copyWith =>
      __$CourseCategoryCopyWithImpl<_CourseCategory>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CourseCategoryToJson(this);
  }
}

abstract class _CourseCategory implements CourseCategory {
  factory _CourseCategory(
      {@JsonKey(name: '_id') String id,
      String name,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) = _$_CourseCategory;

  factory _CourseCategory.fromJson(Map<String, dynamic> json) =
      _$_CourseCategory.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get name;
  @override
  String get type;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$CourseCategoryCopyWith<_CourseCategory> get copyWith;
}

Lesson _$LessonFromJson(Map<String, dynamic> json) {
  return _Lesson.fromJson(json);
}

/// @nodoc
class _$LessonTearOff {
  const _$LessonTearOff();

// ignore: unused_element
  _Lesson call(
      {@JsonKey(name: '_id') String id,
      String courseId,
      String title,
      String description,
      List<LessonContent> lessonContents,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Lesson(
      id: id,
      courseId: courseId,
      title: title,
      description: description,
      lessonContents: lessonContents,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Lesson fromJson(Map<String, Object> json) {
    return Lesson.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Lesson = _$LessonTearOff();

/// @nodoc
mixin _$Lesson {
  @JsonKey(name: '_id')
  String get id;
  String get courseId;
  String get title;
  String get description;
  List<LessonContent> get lessonContents;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $LessonCopyWith<Lesson> get copyWith;
}

/// @nodoc
abstract class $LessonCopyWith<$Res> {
  factory $LessonCopyWith(Lesson value, $Res Function(Lesson) then) =
      _$LessonCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String courseId,
      String title,
      String description,
      List<LessonContent> lessonContents,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$LessonCopyWithImpl<$Res> implements $LessonCopyWith<$Res> {
  _$LessonCopyWithImpl(this._value, this._then);

  final Lesson _value;
  // ignore: unused_field
  final $Res Function(Lesson) _then;

  @override
  $Res call({
    Object id = freezed,
    Object courseId = freezed,
    Object title = freezed,
    Object description = freezed,
    Object lessonContents = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      lessonContents: lessonContents == freezed
          ? _value.lessonContents
          : lessonContents as List<LessonContent>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$LessonCopyWith<$Res> implements $LessonCopyWith<$Res> {
  factory _$LessonCopyWith(_Lesson value, $Res Function(_Lesson) then) =
      __$LessonCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String courseId,
      String title,
      String description,
      List<LessonContent> lessonContents,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$LessonCopyWithImpl<$Res> extends _$LessonCopyWithImpl<$Res>
    implements _$LessonCopyWith<$Res> {
  __$LessonCopyWithImpl(_Lesson _value, $Res Function(_Lesson) _then)
      : super(_value, (v) => _then(v as _Lesson));

  @override
  _Lesson get _value => super._value as _Lesson;

  @override
  $Res call({
    Object id = freezed,
    Object courseId = freezed,
    Object title = freezed,
    Object description = freezed,
    Object lessonContents = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Lesson(
      id: id == freezed ? _value.id : id as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      lessonContents: lessonContents == freezed
          ? _value.lessonContents
          : lessonContents as List<LessonContent>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Lesson implements _Lesson {
  _$_Lesson(
      {@JsonKey(name: '_id') this.id,
      this.courseId,
      this.title,
      this.description,
      this.lessonContents,
      this.createdAt,
      this.updatedAt});

  factory _$_Lesson.fromJson(Map<String, dynamic> json) =>
      _$_$_LessonFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String courseId;
  @override
  final String title;
  @override
  final String description;
  @override
  final List<LessonContent> lessonContents;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Lesson(id: $id, courseId: $courseId, title: $title, description: $description, lessonContents: $lessonContents, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Lesson &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.courseId, courseId) ||
                const DeepCollectionEquality()
                    .equals(other.courseId, courseId)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.lessonContents, lessonContents) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContents, lessonContents)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(courseId) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(lessonContents) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$LessonCopyWith<_Lesson> get copyWith =>
      __$LessonCopyWithImpl<_Lesson>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LessonToJson(this);
  }
}

abstract class _Lesson implements Lesson {
  factory _Lesson(
      {@JsonKey(name: '_id') String id,
      String courseId,
      String title,
      String description,
      List<LessonContent> lessonContents,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Lesson;

  factory _Lesson.fromJson(Map<String, dynamic> json) = _$_Lesson.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get courseId;
  @override
  String get title;
  @override
  String get description;
  @override
  List<LessonContent> get lessonContents;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$LessonCopyWith<_Lesson> get copyWith;
}

LessonContent _$LessonContentFromJson(Map<String, dynamic> json) {
  return _LessonContent.fromJson(json);
}

/// @nodoc
class _$LessonContentTearOff {
  const _$LessonContentTearOff();

// ignore: unused_element
  _LessonContent call(
      {@JsonKey(name: '_id') String id,
      String lessonId,
      String title,
      String description,
      String type,
      int order,
      String fileId,
      String videoUrl,
      List<Article> articles,
      List<Question> questions,
      Files video,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _LessonContent(
      id: id,
      lessonId: lessonId,
      title: title,
      description: description,
      type: type,
      order: order,
      fileId: fileId,
      videoUrl: videoUrl,
      articles: articles,
      questions: questions,
      video: video,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  LessonContent fromJson(Map<String, Object> json) {
    return LessonContent.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $LessonContent = _$LessonContentTearOff();

/// @nodoc
mixin _$LessonContent {
  @JsonKey(name: '_id')
  String get id;
  String get lessonId;
  String get title;
  String get description;
  String get type;
  int get order;
  String get fileId;
  String get videoUrl;
  List<Article> get articles;
  List<Question> get questions;
  Files get video;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $LessonContentCopyWith<LessonContent> get copyWith;
}

/// @nodoc
abstract class $LessonContentCopyWith<$Res> {
  factory $LessonContentCopyWith(
          LessonContent value, $Res Function(LessonContent) then) =
      _$LessonContentCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String lessonId,
      String title,
      String description,
      String type,
      int order,
      String fileId,
      String videoUrl,
      List<Article> articles,
      List<Question> questions,
      Files video,
      DateTime createdAt,
      DateTime updatedAt});

  $FilesCopyWith<$Res> get video;
}

/// @nodoc
class _$LessonContentCopyWithImpl<$Res>
    implements $LessonContentCopyWith<$Res> {
  _$LessonContentCopyWithImpl(this._value, this._then);

  final LessonContent _value;
  // ignore: unused_field
  final $Res Function(LessonContent) _then;

  @override
  $Res call({
    Object id = freezed,
    Object lessonId = freezed,
    Object title = freezed,
    Object description = freezed,
    Object type = freezed,
    Object order = freezed,
    Object fileId = freezed,
    Object videoUrl = freezed,
    Object articles = freezed,
    Object questions = freezed,
    Object video = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      type: type == freezed ? _value.type : type as String,
      order: order == freezed ? _value.order : order as int,
      fileId: fileId == freezed ? _value.fileId : fileId as String,
      videoUrl: videoUrl == freezed ? _value.videoUrl : videoUrl as String,
      articles:
          articles == freezed ? _value.articles : articles as List<Article>,
      questions:
          questions == freezed ? _value.questions : questions as List<Question>,
      video: video == freezed ? _value.video : video as Files,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }

  @override
  $FilesCopyWith<$Res> get video {
    if (_value.video == null) {
      return null;
    }
    return $FilesCopyWith<$Res>(_value.video, (value) {
      return _then(_value.copyWith(video: value));
    });
  }
}

/// @nodoc
abstract class _$LessonContentCopyWith<$Res>
    implements $LessonContentCopyWith<$Res> {
  factory _$LessonContentCopyWith(
          _LessonContent value, $Res Function(_LessonContent) then) =
      __$LessonContentCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String lessonId,
      String title,
      String description,
      String type,
      int order,
      String fileId,
      String videoUrl,
      List<Article> articles,
      List<Question> questions,
      Files video,
      DateTime createdAt,
      DateTime updatedAt});

  @override
  $FilesCopyWith<$Res> get video;
}

/// @nodoc
class __$LessonContentCopyWithImpl<$Res>
    extends _$LessonContentCopyWithImpl<$Res>
    implements _$LessonContentCopyWith<$Res> {
  __$LessonContentCopyWithImpl(
      _LessonContent _value, $Res Function(_LessonContent) _then)
      : super(_value, (v) => _then(v as _LessonContent));

  @override
  _LessonContent get _value => super._value as _LessonContent;

  @override
  $Res call({
    Object id = freezed,
    Object lessonId = freezed,
    Object title = freezed,
    Object description = freezed,
    Object type = freezed,
    Object order = freezed,
    Object fileId = freezed,
    Object videoUrl = freezed,
    Object articles = freezed,
    Object questions = freezed,
    Object video = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_LessonContent(
      id: id == freezed ? _value.id : id as String,
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      type: type == freezed ? _value.type : type as String,
      order: order == freezed ? _value.order : order as int,
      fileId: fileId == freezed ? _value.fileId : fileId as String,
      videoUrl: videoUrl == freezed ? _value.videoUrl : videoUrl as String,
      articles:
          articles == freezed ? _value.articles : articles as List<Article>,
      questions:
          questions == freezed ? _value.questions : questions as List<Question>,
      video: video == freezed ? _value.video : video as Files,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_LessonContent implements _LessonContent {
  _$_LessonContent(
      {@JsonKey(name: '_id') this.id,
      this.lessonId,
      this.title,
      this.description,
      this.type,
      this.order,
      this.fileId,
      this.videoUrl,
      this.articles,
      this.questions,
      this.video,
      this.createdAt,
      this.updatedAt});

  factory _$_LessonContent.fromJson(Map<String, dynamic> json) =>
      _$_$_LessonContentFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String lessonId;
  @override
  final String title;
  @override
  final String description;
  @override
  final String type;
  @override
  final int order;
  @override
  final String fileId;
  @override
  final String videoUrl;
  @override
  final List<Article> articles;
  @override
  final List<Question> questions;
  @override
  final Files video;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'LessonContent(id: $id, lessonId: $lessonId, title: $title, description: $description, type: $type, order: $order, fileId: $fileId, videoUrl: $videoUrl, articles: $articles, questions: $questions, video: $video, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LessonContent &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.lessonId, lessonId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonId, lessonId)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.fileId, fileId) ||
                const DeepCollectionEquality().equals(other.fileId, fileId)) &&
            (identical(other.videoUrl, videoUrl) ||
                const DeepCollectionEquality()
                    .equals(other.videoUrl, videoUrl)) &&
            (identical(other.articles, articles) ||
                const DeepCollectionEquality()
                    .equals(other.articles, articles)) &&
            (identical(other.questions, questions) ||
                const DeepCollectionEquality()
                    .equals(other.questions, questions)) &&
            (identical(other.video, video) ||
                const DeepCollectionEquality().equals(other.video, video)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(lessonId) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(fileId) ^
      const DeepCollectionEquality().hash(videoUrl) ^
      const DeepCollectionEquality().hash(articles) ^
      const DeepCollectionEquality().hash(questions) ^
      const DeepCollectionEquality().hash(video) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$LessonContentCopyWith<_LessonContent> get copyWith =>
      __$LessonContentCopyWithImpl<_LessonContent>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LessonContentToJson(this);
  }
}

abstract class _LessonContent implements LessonContent {
  factory _LessonContent(
      {@JsonKey(name: '_id') String id,
      String lessonId,
      String title,
      String description,
      String type,
      int order,
      String fileId,
      String videoUrl,
      List<Article> articles,
      List<Question> questions,
      Files video,
      DateTime createdAt,
      DateTime updatedAt}) = _$_LessonContent;

  factory _LessonContent.fromJson(Map<String, dynamic> json) =
      _$_LessonContent.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get lessonId;
  @override
  String get title;
  @override
  String get description;
  @override
  String get type;
  @override
  int get order;
  @override
  String get fileId;
  @override
  String get videoUrl;
  @override
  List<Article> get articles;
  @override
  List<Question> get questions;
  @override
  Files get video;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$LessonContentCopyWith<_LessonContent> get copyWith;
}

ClassRoom _$ClassRoomFromJson(Map<String, dynamic> json) {
  return _ClassRoom.fromJson(json);
}

/// @nodoc
class _$ClassRoomTearOff {
  const _$ClassRoomTearOff();

// ignore: unused_element
  _ClassRoom call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String schoolId,
      List<String> courseIds,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _ClassRoom(
      id: id,
      title: title,
      description: description,
      schoolId: schoolId,
      courseIds: courseIds,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  ClassRoom fromJson(Map<String, Object> json) {
    return ClassRoom.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $ClassRoom = _$ClassRoomTearOff();

/// @nodoc
mixin _$ClassRoom {
  @JsonKey(name: '_id')
  String get id;
  String get title;
  String get description;
  String get schoolId;
  List<String> get courseIds;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $ClassRoomCopyWith<ClassRoom> get copyWith;
}

/// @nodoc
abstract class $ClassRoomCopyWith<$Res> {
  factory $ClassRoomCopyWith(ClassRoom value, $Res Function(ClassRoom) then) =
      _$ClassRoomCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String schoolId,
      List<String> courseIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$ClassRoomCopyWithImpl<$Res> implements $ClassRoomCopyWith<$Res> {
  _$ClassRoomCopyWithImpl(this._value, this._then);

  final ClassRoom _value;
  // ignore: unused_field
  final $Res Function(ClassRoom) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object schoolId = freezed,
    Object courseIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      schoolId: schoolId == freezed ? _value.schoolId : schoolId as String,
      courseIds:
          courseIds == freezed ? _value.courseIds : courseIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$ClassRoomCopyWith<$Res> implements $ClassRoomCopyWith<$Res> {
  factory _$ClassRoomCopyWith(
          _ClassRoom value, $Res Function(_ClassRoom) then) =
      __$ClassRoomCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String schoolId,
      List<String> courseIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$ClassRoomCopyWithImpl<$Res> extends _$ClassRoomCopyWithImpl<$Res>
    implements _$ClassRoomCopyWith<$Res> {
  __$ClassRoomCopyWithImpl(_ClassRoom _value, $Res Function(_ClassRoom) _then)
      : super(_value, (v) => _then(v as _ClassRoom));

  @override
  _ClassRoom get _value => super._value as _ClassRoom;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object description = freezed,
    Object schoolId = freezed,
    Object courseIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_ClassRoom(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      description:
          description == freezed ? _value.description : description as String,
      schoolId: schoolId == freezed ? _value.schoolId : schoolId as String,
      courseIds:
          courseIds == freezed ? _value.courseIds : courseIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_ClassRoom implements _ClassRoom {
  _$_ClassRoom(
      {@JsonKey(name: '_id') this.id,
      this.title,
      this.description,
      this.schoolId,
      this.courseIds,
      this.createdAt,
      this.updatedAt});

  factory _$_ClassRoom.fromJson(Map<String, dynamic> json) =>
      _$_$_ClassRoomFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String title;
  @override
  final String description;
  @override
  final String schoolId;
  @override
  final List<String> courseIds;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'ClassRoom(id: $id, title: $title, description: $description, schoolId: $schoolId, courseIds: $courseIds, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ClassRoom &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.schoolId, schoolId) ||
                const DeepCollectionEquality()
                    .equals(other.schoolId, schoolId)) &&
            (identical(other.courseIds, courseIds) ||
                const DeepCollectionEquality()
                    .equals(other.courseIds, courseIds)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(schoolId) ^
      const DeepCollectionEquality().hash(courseIds) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$ClassRoomCopyWith<_ClassRoom> get copyWith =>
      __$ClassRoomCopyWithImpl<_ClassRoom>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ClassRoomToJson(this);
  }
}

abstract class _ClassRoom implements ClassRoom {
  factory _ClassRoom(
      {@JsonKey(name: '_id') String id,
      String title,
      String description,
      String schoolId,
      List<String> courseIds,
      DateTime createdAt,
      DateTime updatedAt}) = _$_ClassRoom;

  factory _ClassRoom.fromJson(Map<String, dynamic> json) =
      _$_ClassRoom.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get title;
  @override
  String get description;
  @override
  String get schoolId;
  @override
  List<String> get courseIds;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$ClassRoomCopyWith<_ClassRoom> get copyWith;
}

School _$SchoolFromJson(Map<String, dynamic> json) {
  return _School.fromJson(json);
}

/// @nodoc
class _$SchoolTearOff {
  const _$SchoolTearOff();

// ignore: unused_element
  _School call(
      {@JsonKey(name: '_id') String id,
      String name,
      String about,
      List<String> classroomIds,
      List<String> instructorIds,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _School(
      id: id,
      name: name,
      about: about,
      classroomIds: classroomIds,
      instructorIds: instructorIds,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  School fromJson(Map<String, Object> json) {
    return School.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $School = _$SchoolTearOff();

/// @nodoc
mixin _$School {
  @JsonKey(name: '_id')
  String get id;
  String get name;
  String get about;
  List<String> get classroomIds;
  List<String> get instructorIds;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $SchoolCopyWith<School> get copyWith;
}

/// @nodoc
abstract class $SchoolCopyWith<$Res> {
  factory $SchoolCopyWith(School value, $Res Function(School) then) =
      _$SchoolCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String about,
      List<String> classroomIds,
      List<String> instructorIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$SchoolCopyWithImpl<$Res> implements $SchoolCopyWith<$Res> {
  _$SchoolCopyWithImpl(this._value, this._then);

  final School _value;
  // ignore: unused_field
  final $Res Function(School) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object about = freezed,
    Object classroomIds = freezed,
    Object instructorIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      about: about == freezed ? _value.about : about as String,
      classroomIds: classroomIds == freezed
          ? _value.classroomIds
          : classroomIds as List<String>,
      instructorIds: instructorIds == freezed
          ? _value.instructorIds
          : instructorIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$SchoolCopyWith<$Res> implements $SchoolCopyWith<$Res> {
  factory _$SchoolCopyWith(_School value, $Res Function(_School) then) =
      __$SchoolCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String about,
      List<String> classroomIds,
      List<String> instructorIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$SchoolCopyWithImpl<$Res> extends _$SchoolCopyWithImpl<$Res>
    implements _$SchoolCopyWith<$Res> {
  __$SchoolCopyWithImpl(_School _value, $Res Function(_School) _then)
      : super(_value, (v) => _then(v as _School));

  @override
  _School get _value => super._value as _School;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object about = freezed,
    Object classroomIds = freezed,
    Object instructorIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_School(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      about: about == freezed ? _value.about : about as String,
      classroomIds: classroomIds == freezed
          ? _value.classroomIds
          : classroomIds as List<String>,
      instructorIds: instructorIds == freezed
          ? _value.instructorIds
          : instructorIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_School implements _School {
  _$_School(
      {@JsonKey(name: '_id') this.id,
      this.name,
      this.about,
      this.classroomIds,
      this.instructorIds,
      this.createdAt,
      this.updatedAt});

  factory _$_School.fromJson(Map<String, dynamic> json) =>
      _$_$_SchoolFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String name;
  @override
  final String about;
  @override
  final List<String> classroomIds;
  @override
  final List<String> instructorIds;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'School(id: $id, name: $name, about: $about, classroomIds: $classroomIds, instructorIds: $instructorIds, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _School &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.about, about) ||
                const DeepCollectionEquality().equals(other.about, about)) &&
            (identical(other.classroomIds, classroomIds) ||
                const DeepCollectionEquality()
                    .equals(other.classroomIds, classroomIds)) &&
            (identical(other.instructorIds, instructorIds) ||
                const DeepCollectionEquality()
                    .equals(other.instructorIds, instructorIds)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(about) ^
      const DeepCollectionEquality().hash(classroomIds) ^
      const DeepCollectionEquality().hash(instructorIds) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$SchoolCopyWith<_School> get copyWith =>
      __$SchoolCopyWithImpl<_School>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SchoolToJson(this);
  }
}

abstract class _School implements School {
  factory _School(
      {@JsonKey(name: '_id') String id,
      String name,
      String about,
      List<String> classroomIds,
      List<String> instructorIds,
      DateTime createdAt,
      DateTime updatedAt}) = _$_School;

  factory _School.fromJson(Map<String, dynamic> json) = _$_School.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get name;
  @override
  String get about;
  @override
  List<String> get classroomIds;
  @override
  List<String> get instructorIds;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$SchoolCopyWith<_School> get copyWith;
}

ChatRoom _$ChatRoomFromJson(Map<String, dynamic> json) {
  return _ChatRoom.fromJson(json);
}

/// @nodoc
class _$ChatRoomTearOff {
  const _$ChatRoomTearOff();

// ignore: unused_element
  _ChatRoom call(
      {@JsonKey(name: '_id') String id,
      String name,
      String status,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _ChatRoom(
      id: id,
      name: name,
      status: status,
      type: type,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  ChatRoom fromJson(Map<String, Object> json) {
    return ChatRoom.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $ChatRoom = _$ChatRoomTearOff();

/// @nodoc
mixin _$ChatRoom {
  @JsonKey(name: '_id')
  String get id;
  String get name;
  String get status;
  String get type;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $ChatRoomCopyWith<ChatRoom> get copyWith;
}

/// @nodoc
abstract class $ChatRoomCopyWith<$Res> {
  factory $ChatRoomCopyWith(ChatRoom value, $Res Function(ChatRoom) then) =
      _$ChatRoomCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String status,
      String type,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$ChatRoomCopyWithImpl<$Res> implements $ChatRoomCopyWith<$Res> {
  _$ChatRoomCopyWithImpl(this._value, this._then);

  final ChatRoom _value;
  // ignore: unused_field
  final $Res Function(ChatRoom) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object status = freezed,
    Object type = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      status: status == freezed ? _value.status : status as String,
      type: type == freezed ? _value.type : type as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$ChatRoomCopyWith<$Res> implements $ChatRoomCopyWith<$Res> {
  factory _$ChatRoomCopyWith(_ChatRoom value, $Res Function(_ChatRoom) then) =
      __$ChatRoomCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String status,
      String type,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$ChatRoomCopyWithImpl<$Res> extends _$ChatRoomCopyWithImpl<$Res>
    implements _$ChatRoomCopyWith<$Res> {
  __$ChatRoomCopyWithImpl(_ChatRoom _value, $Res Function(_ChatRoom) _then)
      : super(_value, (v) => _then(v as _ChatRoom));

  @override
  _ChatRoom get _value => super._value as _ChatRoom;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object status = freezed,
    Object type = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_ChatRoom(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      status: status == freezed ? _value.status : status as String,
      type: type == freezed ? _value.type : type as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_ChatRoom implements _ChatRoom {
  _$_ChatRoom(
      {@JsonKey(name: '_id') this.id,
      this.name,
      this.status,
      this.type,
      this.createdAt,
      this.updatedAt});

  factory _$_ChatRoom.fromJson(Map<String, dynamic> json) =>
      _$_$_ChatRoomFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String name;
  @override
  final String status;
  @override
  final String type;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'ChatRoom(id: $id, name: $name, status: $status, type: $type, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChatRoom &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$ChatRoomCopyWith<_ChatRoom> get copyWith =>
      __$ChatRoomCopyWithImpl<_ChatRoom>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ChatRoomToJson(this);
  }
}

abstract class _ChatRoom implements ChatRoom {
  factory _ChatRoom(
      {@JsonKey(name: '_id') String id,
      String name,
      String status,
      String type,
      DateTime createdAt,
      DateTime updatedAt}) = _$_ChatRoom;

  factory _ChatRoom.fromJson(Map<String, dynamic> json) = _$_ChatRoom.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get name;
  @override
  String get status;
  @override
  String get type;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$ChatRoomCopyWith<_ChatRoom> get copyWith;
}

Files _$FilesFromJson(Map<String, dynamic> json) {
  return _Files.fromJson(json);
}

/// @nodoc
class _$FilesTearOff {
  const _$FilesTearOff();

// ignore: unused_element
  _Files call(
      {@JsonKey(name: '_id') String id,
      String name,
      String originalName,
      String contentType,
      String url,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Files(
      id: id,
      name: name,
      originalName: originalName,
      contentType: contentType,
      url: url,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Files fromJson(Map<String, Object> json) {
    return Files.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Files = _$FilesTearOff();

/// @nodoc
mixin _$Files {
  @JsonKey(name: '_id')
  String get id;
  String get name;
  String get originalName;
  String get contentType;
  String get url;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $FilesCopyWith<Files> get copyWith;
}

/// @nodoc
abstract class $FilesCopyWith<$Res> {
  factory $FilesCopyWith(Files value, $Res Function(Files) then) =
      _$FilesCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String originalName,
      String contentType,
      String url,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$FilesCopyWithImpl<$Res> implements $FilesCopyWith<$Res> {
  _$FilesCopyWithImpl(this._value, this._then);

  final Files _value;
  // ignore: unused_field
  final $Res Function(Files) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object originalName = freezed,
    Object contentType = freezed,
    Object url = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      originalName: originalName == freezed
          ? _value.originalName
          : originalName as String,
      contentType:
          contentType == freezed ? _value.contentType : contentType as String,
      url: url == freezed ? _value.url : url as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$FilesCopyWith<$Res> implements $FilesCopyWith<$Res> {
  factory _$FilesCopyWith(_Files value, $Res Function(_Files) then) =
      __$FilesCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String name,
      String originalName,
      String contentType,
      String url,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$FilesCopyWithImpl<$Res> extends _$FilesCopyWithImpl<$Res>
    implements _$FilesCopyWith<$Res> {
  __$FilesCopyWithImpl(_Files _value, $Res Function(_Files) _then)
      : super(_value, (v) => _then(v as _Files));

  @override
  _Files get _value => super._value as _Files;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object originalName = freezed,
    Object contentType = freezed,
    Object url = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Files(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      originalName: originalName == freezed
          ? _value.originalName
          : originalName as String,
      contentType:
          contentType == freezed ? _value.contentType : contentType as String,
      url: url == freezed ? _value.url : url as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Files implements _Files {
  _$_Files(
      {@JsonKey(name: '_id') this.id,
      this.name,
      this.originalName,
      this.contentType,
      this.url,
      this.createdAt,
      this.updatedAt});

  factory _$_Files.fromJson(Map<String, dynamic> json) =>
      _$_$_FilesFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String name;
  @override
  final String originalName;
  @override
  final String contentType;
  @override
  final String url;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Files(id: $id, name: $name, originalName: $originalName, contentType: $contentType, url: $url, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Files &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.originalName, originalName) ||
                const DeepCollectionEquality()
                    .equals(other.originalName, originalName)) &&
            (identical(other.contentType, contentType) ||
                const DeepCollectionEquality()
                    .equals(other.contentType, contentType)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(originalName) ^
      const DeepCollectionEquality().hash(contentType) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$FilesCopyWith<_Files> get copyWith =>
      __$FilesCopyWithImpl<_Files>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FilesToJson(this);
  }
}

abstract class _Files implements Files {
  factory _Files(
      {@JsonKey(name: '_id') String id,
      String name,
      String originalName,
      String contentType,
      String url,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Files;

  factory _Files.fromJson(Map<String, dynamic> json) = _$_Files.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get name;
  @override
  String get originalName;
  @override
  String get contentType;
  @override
  String get url;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$FilesCopyWith<_Files> get copyWith;
}

Question _$QuestionFromJson(Map<String, dynamic> json) {
  return _Question.fromJson(json);
}

/// @nodoc
class _$QuestionTearOff {
  const _$QuestionTearOff();

// ignore: unused_element
  _Question call(
      {@JsonKey(name: '_id') String id,
      String lessonContentId,
      String textQuestion,
      List<String> fileQuestionIds,
      int order,
      double point,
      String questionType,
      List<String> multipleChoices,
      String textAnswer,
      List<String> fileAnswerIds,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Question(
      id: id,
      lessonContentId: lessonContentId,
      textQuestion: textQuestion,
      fileQuestionIds: fileQuestionIds,
      order: order,
      point: point,
      questionType: questionType,
      multipleChoices: multipleChoices,
      textAnswer: textAnswer,
      fileAnswerIds: fileAnswerIds,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Question fromJson(Map<String, Object> json) {
    return Question.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Question = _$QuestionTearOff();

/// @nodoc
mixin _$Question {
  @JsonKey(name: '_id')
  String get id;
  String get lessonContentId;
  String get textQuestion;
  List<String> get fileQuestionIds;
  int get order;
  double get point;
  String get questionType;
  List<String> get multipleChoices;
  String get textAnswer;
  List<String> get fileAnswerIds;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $QuestionCopyWith<Question> get copyWith;
}

/// @nodoc
abstract class $QuestionCopyWith<$Res> {
  factory $QuestionCopyWith(Question value, $Res Function(Question) then) =
      _$QuestionCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String lessonContentId,
      String textQuestion,
      List<String> fileQuestionIds,
      int order,
      double point,
      String questionType,
      List<String> multipleChoices,
      String textAnswer,
      List<String> fileAnswerIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$QuestionCopyWithImpl<$Res> implements $QuestionCopyWith<$Res> {
  _$QuestionCopyWithImpl(this._value, this._then);

  final Question _value;
  // ignore: unused_field
  final $Res Function(Question) _then;

  @override
  $Res call({
    Object id = freezed,
    Object lessonContentId = freezed,
    Object textQuestion = freezed,
    Object fileQuestionIds = freezed,
    Object order = freezed,
    Object point = freezed,
    Object questionType = freezed,
    Object multipleChoices = freezed,
    Object textAnswer = freezed,
    Object fileAnswerIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      textQuestion: textQuestion == freezed
          ? _value.textQuestion
          : textQuestion as String,
      fileQuestionIds: fileQuestionIds == freezed
          ? _value.fileQuestionIds
          : fileQuestionIds as List<String>,
      order: order == freezed ? _value.order : order as int,
      point: point == freezed ? _value.point : point as double,
      questionType: questionType == freezed
          ? _value.questionType
          : questionType as String,
      multipleChoices: multipleChoices == freezed
          ? _value.multipleChoices
          : multipleChoices as List<String>,
      textAnswer:
          textAnswer == freezed ? _value.textAnswer : textAnswer as String,
      fileAnswerIds: fileAnswerIds == freezed
          ? _value.fileAnswerIds
          : fileAnswerIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$QuestionCopyWith<$Res> implements $QuestionCopyWith<$Res> {
  factory _$QuestionCopyWith(_Question value, $Res Function(_Question) then) =
      __$QuestionCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String lessonContentId,
      String textQuestion,
      List<String> fileQuestionIds,
      int order,
      double point,
      String questionType,
      List<String> multipleChoices,
      String textAnswer,
      List<String> fileAnswerIds,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$QuestionCopyWithImpl<$Res> extends _$QuestionCopyWithImpl<$Res>
    implements _$QuestionCopyWith<$Res> {
  __$QuestionCopyWithImpl(_Question _value, $Res Function(_Question) _then)
      : super(_value, (v) => _then(v as _Question));

  @override
  _Question get _value => super._value as _Question;

  @override
  $Res call({
    Object id = freezed,
    Object lessonContentId = freezed,
    Object textQuestion = freezed,
    Object fileQuestionIds = freezed,
    Object order = freezed,
    Object point = freezed,
    Object questionType = freezed,
    Object multipleChoices = freezed,
    Object textAnswer = freezed,
    Object fileAnswerIds = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Question(
      id: id == freezed ? _value.id : id as String,
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      textQuestion: textQuestion == freezed
          ? _value.textQuestion
          : textQuestion as String,
      fileQuestionIds: fileQuestionIds == freezed
          ? _value.fileQuestionIds
          : fileQuestionIds as List<String>,
      order: order == freezed ? _value.order : order as int,
      point: point == freezed ? _value.point : point as double,
      questionType: questionType == freezed
          ? _value.questionType
          : questionType as String,
      multipleChoices: multipleChoices == freezed
          ? _value.multipleChoices
          : multipleChoices as List<String>,
      textAnswer:
          textAnswer == freezed ? _value.textAnswer : textAnswer as String,
      fileAnswerIds: fileAnswerIds == freezed
          ? _value.fileAnswerIds
          : fileAnswerIds as List<String>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Question implements _Question {
  _$_Question(
      {@JsonKey(name: '_id') this.id,
      this.lessonContentId,
      this.textQuestion,
      this.fileQuestionIds,
      this.order,
      this.point,
      this.questionType,
      this.multipleChoices,
      this.textAnswer,
      this.fileAnswerIds,
      this.createdAt,
      this.updatedAt});

  factory _$_Question.fromJson(Map<String, dynamic> json) =>
      _$_$_QuestionFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String lessonContentId;
  @override
  final String textQuestion;
  @override
  final List<String> fileQuestionIds;
  @override
  final int order;
  @override
  final double point;
  @override
  final String questionType;
  @override
  final List<String> multipleChoices;
  @override
  final String textAnswer;
  @override
  final List<String> fileAnswerIds;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Question(id: $id, lessonContentId: $lessonContentId, textQuestion: $textQuestion, fileQuestionIds: $fileQuestionIds, order: $order, point: $point, questionType: $questionType, multipleChoices: $multipleChoices, textAnswer: $textAnswer, fileAnswerIds: $fileAnswerIds, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Question &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.lessonContentId, lessonContentId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContentId, lessonContentId)) &&
            (identical(other.textQuestion, textQuestion) ||
                const DeepCollectionEquality()
                    .equals(other.textQuestion, textQuestion)) &&
            (identical(other.fileQuestionIds, fileQuestionIds) ||
                const DeepCollectionEquality()
                    .equals(other.fileQuestionIds, fileQuestionIds)) &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.point, point) ||
                const DeepCollectionEquality().equals(other.point, point)) &&
            (identical(other.questionType, questionType) ||
                const DeepCollectionEquality()
                    .equals(other.questionType, questionType)) &&
            (identical(other.multipleChoices, multipleChoices) ||
                const DeepCollectionEquality()
                    .equals(other.multipleChoices, multipleChoices)) &&
            (identical(other.textAnswer, textAnswer) ||
                const DeepCollectionEquality()
                    .equals(other.textAnswer, textAnswer)) &&
            (identical(other.fileAnswerIds, fileAnswerIds) ||
                const DeepCollectionEquality()
                    .equals(other.fileAnswerIds, fileAnswerIds)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(lessonContentId) ^
      const DeepCollectionEquality().hash(textQuestion) ^
      const DeepCollectionEquality().hash(fileQuestionIds) ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(point) ^
      const DeepCollectionEquality().hash(questionType) ^
      const DeepCollectionEquality().hash(multipleChoices) ^
      const DeepCollectionEquality().hash(textAnswer) ^
      const DeepCollectionEquality().hash(fileAnswerIds) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$QuestionCopyWith<_Question> get copyWith =>
      __$QuestionCopyWithImpl<_Question>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_QuestionToJson(this);
  }
}

abstract class _Question implements Question {
  factory _Question(
      {@JsonKey(name: '_id') String id,
      String lessonContentId,
      String textQuestion,
      List<String> fileQuestionIds,
      int order,
      double point,
      String questionType,
      List<String> multipleChoices,
      String textAnswer,
      List<String> fileAnswerIds,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Question;

  factory _Question.fromJson(Map<String, dynamic> json) = _$_Question.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get lessonContentId;
  @override
  String get textQuestion;
  @override
  List<String> get fileQuestionIds;
  @override
  int get order;
  @override
  double get point;
  @override
  String get questionType;
  @override
  List<String> get multipleChoices;
  @override
  String get textAnswer;
  @override
  List<String> get fileAnswerIds;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$QuestionCopyWith<_Question> get copyWith;
}

Article _$ArticleFromJson(Map<String, dynamic> json) {
  return _Article.fromJson(json);
}

/// @nodoc
class _$ArticleTearOff {
  const _$ArticleTearOff();

// ignore: unused_element
  _Article call(
      {@JsonKey(name: '_id') String id,
      String title,
      String paragraph,
      int order,
      List<String> fileIds,
      List<Files> file,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Article(
      id: id,
      title: title,
      paragraph: paragraph,
      order: order,
      fileIds: fileIds,
      file: file,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Article fromJson(Map<String, Object> json) {
    return Article.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Article = _$ArticleTearOff();

/// @nodoc
mixin _$Article {
  @JsonKey(name: '_id')
  String get id;
  String get title;
  String get paragraph;
  int get order;
  List<String> get fileIds;
  List<Files> get file;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $ArticleCopyWith<Article> get copyWith;
}

/// @nodoc
abstract class $ArticleCopyWith<$Res> {
  factory $ArticleCopyWith(Article value, $Res Function(Article) then) =
      _$ArticleCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String paragraph,
      int order,
      List<String> fileIds,
      List<Files> file,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$ArticleCopyWithImpl<$Res> implements $ArticleCopyWith<$Res> {
  _$ArticleCopyWithImpl(this._value, this._then);

  final Article _value;
  // ignore: unused_field
  final $Res Function(Article) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object paragraph = freezed,
    Object order = freezed,
    Object fileIds = freezed,
    Object file = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      paragraph: paragraph == freezed ? _value.paragraph : paragraph as String,
      order: order == freezed ? _value.order : order as int,
      fileIds: fileIds == freezed ? _value.fileIds : fileIds as List<String>,
      file: file == freezed ? _value.file : file as List<Files>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$ArticleCopyWith<$Res> implements $ArticleCopyWith<$Res> {
  factory _$ArticleCopyWith(_Article value, $Res Function(_Article) then) =
      __$ArticleCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String title,
      String paragraph,
      int order,
      List<String> fileIds,
      List<Files> file,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$ArticleCopyWithImpl<$Res> extends _$ArticleCopyWithImpl<$Res>
    implements _$ArticleCopyWith<$Res> {
  __$ArticleCopyWithImpl(_Article _value, $Res Function(_Article) _then)
      : super(_value, (v) => _then(v as _Article));

  @override
  _Article get _value => super._value as _Article;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object paragraph = freezed,
    Object order = freezed,
    Object fileIds = freezed,
    Object file = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Article(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      paragraph: paragraph == freezed ? _value.paragraph : paragraph as String,
      order: order == freezed ? _value.order : order as int,
      fileIds: fileIds == freezed ? _value.fileIds : fileIds as List<String>,
      file: file == freezed ? _value.file : file as List<Files>,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Article implements _Article {
  _$_Article(
      {@JsonKey(name: '_id') this.id,
      this.title,
      this.paragraph,
      this.order,
      this.fileIds,
      this.file,
      this.createdAt,
      this.updatedAt});

  factory _$_Article.fromJson(Map<String, dynamic> json) =>
      _$_$_ArticleFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String title;
  @override
  final String paragraph;
  @override
  final int order;
  @override
  final List<String> fileIds;
  @override
  final List<Files> file;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Article(id: $id, title: $title, paragraph: $paragraph, order: $order, fileIds: $fileIds, file: $file, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Article &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.paragraph, paragraph) ||
                const DeepCollectionEquality()
                    .equals(other.paragraph, paragraph)) &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.fileIds, fileIds) ||
                const DeepCollectionEquality()
                    .equals(other.fileIds, fileIds)) &&
            (identical(other.file, file) ||
                const DeepCollectionEquality().equals(other.file, file)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(paragraph) ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(fileIds) ^
      const DeepCollectionEquality().hash(file) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$ArticleCopyWith<_Article> get copyWith =>
      __$ArticleCopyWithImpl<_Article>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ArticleToJson(this);
  }
}

abstract class _Article implements Article {
  factory _Article(
      {@JsonKey(name: '_id') String id,
      String title,
      String paragraph,
      int order,
      List<String> fileIds,
      List<Files> file,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Article;

  factory _Article.fromJson(Map<String, dynamic> json) = _$_Article.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get title;
  @override
  String get paragraph;
  @override
  int get order;
  @override
  List<String> get fileIds;
  @override
  List<Files> get file;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$ArticleCopyWith<_Article> get copyWith;
}

Rating _$RatingFromJson(Map<String, dynamic> json) {
  return _Rating.fromJson(json);
}

/// @nodoc
class _$RatingTearOff {
  const _$RatingTearOff();

// ignore: unused_element
  _Rating call(
      {@JsonKey(name: '_id') String id,
      double value,
      String ratedBy,
      String ratingFor,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Rating(
      id: id,
      value: value,
      ratedBy: ratedBy,
      ratingFor: ratingFor,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Rating fromJson(Map<String, Object> json) {
    return Rating.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Rating = _$RatingTearOff();

/// @nodoc
mixin _$Rating {
  @JsonKey(name: '_id')
  String get id;
  double get value;
  String get ratedBy;
  String get ratingFor;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $RatingCopyWith<Rating> get copyWith;
}

/// @nodoc
abstract class $RatingCopyWith<$Res> {
  factory $RatingCopyWith(Rating value, $Res Function(Rating) then) =
      _$RatingCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      double value,
      String ratedBy,
      String ratingFor,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$RatingCopyWithImpl<$Res> implements $RatingCopyWith<$Res> {
  _$RatingCopyWithImpl(this._value, this._then);

  final Rating _value;
  // ignore: unused_field
  final $Res Function(Rating) _then;

  @override
  $Res call({
    Object id = freezed,
    Object value = freezed,
    Object ratedBy = freezed,
    Object ratingFor = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      value: value == freezed ? _value.value : value as double,
      ratedBy: ratedBy == freezed ? _value.ratedBy : ratedBy as String,
      ratingFor: ratingFor == freezed ? _value.ratingFor : ratingFor as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$RatingCopyWith<$Res> implements $RatingCopyWith<$Res> {
  factory _$RatingCopyWith(_Rating value, $Res Function(_Rating) then) =
      __$RatingCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      double value,
      String ratedBy,
      String ratingFor,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$RatingCopyWithImpl<$Res> extends _$RatingCopyWithImpl<$Res>
    implements _$RatingCopyWith<$Res> {
  __$RatingCopyWithImpl(_Rating _value, $Res Function(_Rating) _then)
      : super(_value, (v) => _then(v as _Rating));

  @override
  _Rating get _value => super._value as _Rating;

  @override
  $Res call({
    Object id = freezed,
    Object value = freezed,
    Object ratedBy = freezed,
    Object ratingFor = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Rating(
      id: id == freezed ? _value.id : id as String,
      value: value == freezed ? _value.value : value as double,
      ratedBy: ratedBy == freezed ? _value.ratedBy : ratedBy as String,
      ratingFor: ratingFor == freezed ? _value.ratingFor : ratingFor as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Rating implements _Rating {
  _$_Rating(
      {@JsonKey(name: '_id') this.id,
      this.value,
      this.ratedBy,
      this.ratingFor,
      this.createdAt,
      this.updatedAt});

  factory _$_Rating.fromJson(Map<String, dynamic> json) =>
      _$_$_RatingFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final double value;
  @override
  final String ratedBy;
  @override
  final String ratingFor;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Rating(id: $id, value: $value, ratedBy: $ratedBy, ratingFor: $ratingFor, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Rating &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.ratedBy, ratedBy) ||
                const DeepCollectionEquality()
                    .equals(other.ratedBy, ratedBy)) &&
            (identical(other.ratingFor, ratingFor) ||
                const DeepCollectionEquality()
                    .equals(other.ratingFor, ratingFor)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(ratedBy) ^
      const DeepCollectionEquality().hash(ratingFor) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$RatingCopyWith<_Rating> get copyWith =>
      __$RatingCopyWithImpl<_Rating>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RatingToJson(this);
  }
}

abstract class _Rating implements Rating {
  factory _Rating(
      {@JsonKey(name: '_id') String id,
      double value,
      String ratedBy,
      String ratingFor,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Rating;

  factory _Rating.fromJson(Map<String, dynamic> json) = _$_Rating.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  double get value;
  @override
  String get ratedBy;
  @override
  String get ratingFor;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$RatingCopyWith<_Rating> get copyWith;
}

Message _$MessageFromJson(Map<String, dynamic> json) {
  return _Message.fromJson(json);
}

/// @nodoc
class _$MessageTearOff {
  const _$MessageTearOff();

// ignore: unused_element
  _Message call(
      {@JsonKey(name: '_id') String id,
      String sender,
      String text,
      String chatroomId,
      String fileId,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _Message(
      id: id,
      sender: sender,
      text: text,
      chatroomId: chatroomId,
      fileId: fileId,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  Message fromJson(Map<String, Object> json) {
    return Message.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Message = _$MessageTearOff();

/// @nodoc
mixin _$Message {
  @JsonKey(name: '_id')
  String get id;
  String get sender;
  String get text;
  String get chatroomId;
  String get fileId;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $MessageCopyWith<Message> get copyWith;
}

/// @nodoc
abstract class $MessageCopyWith<$Res> {
  factory $MessageCopyWith(Message value, $Res Function(Message) then) =
      _$MessageCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String sender,
      String text,
      String chatroomId,
      String fileId,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class _$MessageCopyWithImpl<$Res> implements $MessageCopyWith<$Res> {
  _$MessageCopyWithImpl(this._value, this._then);

  final Message _value;
  // ignore: unused_field
  final $Res Function(Message) _then;

  @override
  $Res call({
    Object id = freezed,
    Object sender = freezed,
    Object text = freezed,
    Object chatroomId = freezed,
    Object fileId = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      sender: sender == freezed ? _value.sender : sender as String,
      text: text == freezed ? _value.text : text as String,
      chatroomId:
          chatroomId == freezed ? _value.chatroomId : chatroomId as String,
      fileId: fileId == freezed ? _value.fileId : fileId as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$MessageCopyWith<$Res> implements $MessageCopyWith<$Res> {
  factory _$MessageCopyWith(_Message value, $Res Function(_Message) then) =
      __$MessageCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String sender,
      String text,
      String chatroomId,
      String fileId,
      DateTime createdAt,
      DateTime updatedAt});
}

/// @nodoc
class __$MessageCopyWithImpl<$Res> extends _$MessageCopyWithImpl<$Res>
    implements _$MessageCopyWith<$Res> {
  __$MessageCopyWithImpl(_Message _value, $Res Function(_Message) _then)
      : super(_value, (v) => _then(v as _Message));

  @override
  _Message get _value => super._value as _Message;

  @override
  $Res call({
    Object id = freezed,
    Object sender = freezed,
    Object text = freezed,
    Object chatroomId = freezed,
    Object fileId = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_Message(
      id: id == freezed ? _value.id : id as String,
      sender: sender == freezed ? _value.sender : sender as String,
      text: text == freezed ? _value.text : text as String,
      chatroomId:
          chatroomId == freezed ? _value.chatroomId : chatroomId as String,
      fileId: fileId == freezed ? _value.fileId : fileId as String,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Message implements _Message {
  _$_Message(
      {@JsonKey(name: '_id') this.id,
      this.sender,
      this.text,
      this.chatroomId,
      this.fileId,
      this.createdAt,
      this.updatedAt});

  factory _$_Message.fromJson(Map<String, dynamic> json) =>
      _$_$_MessageFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String sender;
  @override
  final String text;
  @override
  final String chatroomId;
  @override
  final String fileId;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'Message(id: $id, sender: $sender, text: $text, chatroomId: $chatroomId, fileId: $fileId, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Message &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.sender, sender) ||
                const DeepCollectionEquality().equals(other.sender, sender)) &&
            (identical(other.text, text) ||
                const DeepCollectionEquality().equals(other.text, text)) &&
            (identical(other.chatroomId, chatroomId) ||
                const DeepCollectionEquality()
                    .equals(other.chatroomId, chatroomId)) &&
            (identical(other.fileId, fileId) ||
                const DeepCollectionEquality().equals(other.fileId, fileId)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(sender) ^
      const DeepCollectionEquality().hash(text) ^
      const DeepCollectionEquality().hash(chatroomId) ^
      const DeepCollectionEquality().hash(fileId) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$MessageCopyWith<_Message> get copyWith =>
      __$MessageCopyWithImpl<_Message>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MessageToJson(this);
  }
}

abstract class _Message implements Message {
  factory _Message(
      {@JsonKey(name: '_id') String id,
      String sender,
      String text,
      String chatroomId,
      String fileId,
      DateTime createdAt,
      DateTime updatedAt}) = _$_Message;

  factory _Message.fromJson(Map<String, dynamic> json) = _$_Message.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get sender;
  @override
  String get text;
  @override
  String get chatroomId;
  @override
  String get fileId;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$MessageCopyWith<_Message> get copyWith;
}

QuestionSubmission _$QuestionSubmissionFromJson(Map<String, dynamic> json) {
  return _QuestionSubmission.fromJson(json);
}

/// @nodoc
class _$QuestionSubmissionTearOff {
  const _$QuestionSubmissionTearOff();

// ignore: unused_element
  _QuestionSubmission call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String lessonId,
      String lessonContentId,
      String questionId,
      int choiceAnswer,
      String textAnswer,
      List<String> fileAnswerIds,
      double resultPoint,
      Question question,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _QuestionSubmission(
      id: id,
      userId: userId,
      courseId: courseId,
      lessonId: lessonId,
      lessonContentId: lessonContentId,
      questionId: questionId,
      choiceAnswer: choiceAnswer,
      textAnswer: textAnswer,
      fileAnswerIds: fileAnswerIds,
      resultPoint: resultPoint,
      question: question,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  QuestionSubmission fromJson(Map<String, Object> json) {
    return QuestionSubmission.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $QuestionSubmission = _$QuestionSubmissionTearOff();

/// @nodoc
mixin _$QuestionSubmission {
  @JsonKey(name: '_id')
  String get id;
  String get userId;
  String get courseId;
  String get lessonId;
  String get lessonContentId;
  String get questionId;
  int get choiceAnswer;
  String get textAnswer;
  List<String> get fileAnswerIds;
  double get resultPoint;
  Question get question;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $QuestionSubmissionCopyWith<QuestionSubmission> get copyWith;
}

/// @nodoc
abstract class $QuestionSubmissionCopyWith<$Res> {
  factory $QuestionSubmissionCopyWith(
          QuestionSubmission value, $Res Function(QuestionSubmission) then) =
      _$QuestionSubmissionCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String lessonId,
      String lessonContentId,
      String questionId,
      int choiceAnswer,
      String textAnswer,
      List<String> fileAnswerIds,
      double resultPoint,
      Question question,
      DateTime createdAt,
      DateTime updatedAt});

  $QuestionCopyWith<$Res> get question;
}

/// @nodoc
class _$QuestionSubmissionCopyWithImpl<$Res>
    implements $QuestionSubmissionCopyWith<$Res> {
  _$QuestionSubmissionCopyWithImpl(this._value, this._then);

  final QuestionSubmission _value;
  // ignore: unused_field
  final $Res Function(QuestionSubmission) _then;

  @override
  $Res call({
    Object id = freezed,
    Object userId = freezed,
    Object courseId = freezed,
    Object lessonId = freezed,
    Object lessonContentId = freezed,
    Object questionId = freezed,
    Object choiceAnswer = freezed,
    Object textAnswer = freezed,
    Object fileAnswerIds = freezed,
    Object resultPoint = freezed,
    Object question = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      userId: userId == freezed ? _value.userId : userId as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      questionId:
          questionId == freezed ? _value.questionId : questionId as String,
      choiceAnswer:
          choiceAnswer == freezed ? _value.choiceAnswer : choiceAnswer as int,
      textAnswer:
          textAnswer == freezed ? _value.textAnswer : textAnswer as String,
      fileAnswerIds: fileAnswerIds == freezed
          ? _value.fileAnswerIds
          : fileAnswerIds as List<String>,
      resultPoint:
          resultPoint == freezed ? _value.resultPoint : resultPoint as double,
      question: question == freezed ? _value.question : question as Question,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }

  @override
  $QuestionCopyWith<$Res> get question {
    if (_value.question == null) {
      return null;
    }
    return $QuestionCopyWith<$Res>(_value.question, (value) {
      return _then(_value.copyWith(question: value));
    });
  }
}

/// @nodoc
abstract class _$QuestionSubmissionCopyWith<$Res>
    implements $QuestionSubmissionCopyWith<$Res> {
  factory _$QuestionSubmissionCopyWith(
          _QuestionSubmission value, $Res Function(_QuestionSubmission) then) =
      __$QuestionSubmissionCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String lessonId,
      String lessonContentId,
      String questionId,
      int choiceAnswer,
      String textAnswer,
      List<String> fileAnswerIds,
      double resultPoint,
      Question question,
      DateTime createdAt,
      DateTime updatedAt});

  @override
  $QuestionCopyWith<$Res> get question;
}

/// @nodoc
class __$QuestionSubmissionCopyWithImpl<$Res>
    extends _$QuestionSubmissionCopyWithImpl<$Res>
    implements _$QuestionSubmissionCopyWith<$Res> {
  __$QuestionSubmissionCopyWithImpl(
      _QuestionSubmission _value, $Res Function(_QuestionSubmission) _then)
      : super(_value, (v) => _then(v as _QuestionSubmission));

  @override
  _QuestionSubmission get _value => super._value as _QuestionSubmission;

  @override
  $Res call({
    Object id = freezed,
    Object userId = freezed,
    Object courseId = freezed,
    Object lessonId = freezed,
    Object lessonContentId = freezed,
    Object questionId = freezed,
    Object choiceAnswer = freezed,
    Object textAnswer = freezed,
    Object fileAnswerIds = freezed,
    Object resultPoint = freezed,
    Object question = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_QuestionSubmission(
      id: id == freezed ? _value.id : id as String,
      userId: userId == freezed ? _value.userId : userId as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      questionId:
          questionId == freezed ? _value.questionId : questionId as String,
      choiceAnswer:
          choiceAnswer == freezed ? _value.choiceAnswer : choiceAnswer as int,
      textAnswer:
          textAnswer == freezed ? _value.textAnswer : textAnswer as String,
      fileAnswerIds: fileAnswerIds == freezed
          ? _value.fileAnswerIds
          : fileAnswerIds as List<String>,
      resultPoint:
          resultPoint == freezed ? _value.resultPoint : resultPoint as double,
      question: question == freezed ? _value.question : question as Question,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_QuestionSubmission implements _QuestionSubmission {
  _$_QuestionSubmission(
      {@JsonKey(name: '_id') this.id,
      this.userId,
      this.courseId,
      this.lessonId,
      this.lessonContentId,
      this.questionId,
      this.choiceAnswer,
      this.textAnswer,
      this.fileAnswerIds,
      this.resultPoint,
      this.question,
      this.createdAt,
      this.updatedAt});

  factory _$_QuestionSubmission.fromJson(Map<String, dynamic> json) =>
      _$_$_QuestionSubmissionFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String userId;
  @override
  final String courseId;
  @override
  final String lessonId;
  @override
  final String lessonContentId;
  @override
  final String questionId;
  @override
  final int choiceAnswer;
  @override
  final String textAnswer;
  @override
  final List<String> fileAnswerIds;
  @override
  final double resultPoint;
  @override
  final Question question;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'QuestionSubmission(id: $id, userId: $userId, courseId: $courseId, lessonId: $lessonId, lessonContentId: $lessonContentId, questionId: $questionId, choiceAnswer: $choiceAnswer, textAnswer: $textAnswer, fileAnswerIds: $fileAnswerIds, resultPoint: $resultPoint, question: $question, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _QuestionSubmission &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.courseId, courseId) ||
                const DeepCollectionEquality()
                    .equals(other.courseId, courseId)) &&
            (identical(other.lessonId, lessonId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonId, lessonId)) &&
            (identical(other.lessonContentId, lessonContentId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContentId, lessonContentId)) &&
            (identical(other.questionId, questionId) ||
                const DeepCollectionEquality()
                    .equals(other.questionId, questionId)) &&
            (identical(other.choiceAnswer, choiceAnswer) ||
                const DeepCollectionEquality()
                    .equals(other.choiceAnswer, choiceAnswer)) &&
            (identical(other.textAnswer, textAnswer) ||
                const DeepCollectionEquality()
                    .equals(other.textAnswer, textAnswer)) &&
            (identical(other.fileAnswerIds, fileAnswerIds) ||
                const DeepCollectionEquality()
                    .equals(other.fileAnswerIds, fileAnswerIds)) &&
            (identical(other.resultPoint, resultPoint) ||
                const DeepCollectionEquality()
                    .equals(other.resultPoint, resultPoint)) &&
            (identical(other.question, question) ||
                const DeepCollectionEquality()
                    .equals(other.question, question)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(courseId) ^
      const DeepCollectionEquality().hash(lessonId) ^
      const DeepCollectionEquality().hash(lessonContentId) ^
      const DeepCollectionEquality().hash(questionId) ^
      const DeepCollectionEquality().hash(choiceAnswer) ^
      const DeepCollectionEquality().hash(textAnswer) ^
      const DeepCollectionEquality().hash(fileAnswerIds) ^
      const DeepCollectionEquality().hash(resultPoint) ^
      const DeepCollectionEquality().hash(question) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$QuestionSubmissionCopyWith<_QuestionSubmission> get copyWith =>
      __$QuestionSubmissionCopyWithImpl<_QuestionSubmission>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_QuestionSubmissionToJson(this);
  }
}

abstract class _QuestionSubmission implements QuestionSubmission {
  factory _QuestionSubmission(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String lessonId,
      String lessonContentId,
      String questionId,
      int choiceAnswer,
      String textAnswer,
      List<String> fileAnswerIds,
      double resultPoint,
      Question question,
      DateTime createdAt,
      DateTime updatedAt}) = _$_QuestionSubmission;

  factory _QuestionSubmission.fromJson(Map<String, dynamic> json) =
      _$_QuestionSubmission.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get userId;
  @override
  String get courseId;
  @override
  String get lessonId;
  @override
  String get lessonContentId;
  @override
  String get questionId;
  @override
  int get choiceAnswer;
  @override
  String get textAnswer;
  @override
  List<String> get fileAnswerIds;
  @override
  double get resultPoint;
  @override
  Question get question;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$QuestionSubmissionCopyWith<_QuestionSubmission> get copyWith;
}

LessonContentSubmission _$LessonContentSubmissionFromJson(
    Map<String, dynamic> json) {
  return _LessonContentSubmission.fromJson(json);
}

/// @nodoc
class _$LessonContentSubmissionTearOff {
  const _$LessonContentSubmissionTearOff();

// ignore: unused_element
  _LessonContentSubmission call(
      {String lessonContentId,
      LessonContent lessonContent,
      List<QuestionSubmission> submissions}) {
    return _LessonContentSubmission(
      lessonContentId: lessonContentId,
      lessonContent: lessonContent,
      submissions: submissions,
    );
  }

// ignore: unused_element
  LessonContentSubmission fromJson(Map<String, Object> json) {
    return LessonContentSubmission.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $LessonContentSubmission = _$LessonContentSubmissionTearOff();

/// @nodoc
mixin _$LessonContentSubmission {
  String get lessonContentId;
  LessonContent get lessonContent;
  List<QuestionSubmission> get submissions;

  Map<String, dynamic> toJson();
  $LessonContentSubmissionCopyWith<LessonContentSubmission> get copyWith;
}

/// @nodoc
abstract class $LessonContentSubmissionCopyWith<$Res> {
  factory $LessonContentSubmissionCopyWith(LessonContentSubmission value,
          $Res Function(LessonContentSubmission) then) =
      _$LessonContentSubmissionCopyWithImpl<$Res>;
  $Res call(
      {String lessonContentId,
      LessonContent lessonContent,
      List<QuestionSubmission> submissions});

  $LessonContentCopyWith<$Res> get lessonContent;
}

/// @nodoc
class _$LessonContentSubmissionCopyWithImpl<$Res>
    implements $LessonContentSubmissionCopyWith<$Res> {
  _$LessonContentSubmissionCopyWithImpl(this._value, this._then);

  final LessonContentSubmission _value;
  // ignore: unused_field
  final $Res Function(LessonContentSubmission) _then;

  @override
  $Res call({
    Object lessonContentId = freezed,
    Object lessonContent = freezed,
    Object submissions = freezed,
  }) {
    return _then(_value.copyWith(
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      lessonContent: lessonContent == freezed
          ? _value.lessonContent
          : lessonContent as LessonContent,
      submissions: submissions == freezed
          ? _value.submissions
          : submissions as List<QuestionSubmission>,
    ));
  }

  @override
  $LessonContentCopyWith<$Res> get lessonContent {
    if (_value.lessonContent == null) {
      return null;
    }
    return $LessonContentCopyWith<$Res>(_value.lessonContent, (value) {
      return _then(_value.copyWith(lessonContent: value));
    });
  }
}

/// @nodoc
abstract class _$LessonContentSubmissionCopyWith<$Res>
    implements $LessonContentSubmissionCopyWith<$Res> {
  factory _$LessonContentSubmissionCopyWith(_LessonContentSubmission value,
          $Res Function(_LessonContentSubmission) then) =
      __$LessonContentSubmissionCopyWithImpl<$Res>;
  @override
  $Res call(
      {String lessonContentId,
      LessonContent lessonContent,
      List<QuestionSubmission> submissions});

  @override
  $LessonContentCopyWith<$Res> get lessonContent;
}

/// @nodoc
class __$LessonContentSubmissionCopyWithImpl<$Res>
    extends _$LessonContentSubmissionCopyWithImpl<$Res>
    implements _$LessonContentSubmissionCopyWith<$Res> {
  __$LessonContentSubmissionCopyWithImpl(_LessonContentSubmission _value,
      $Res Function(_LessonContentSubmission) _then)
      : super(_value, (v) => _then(v as _LessonContentSubmission));

  @override
  _LessonContentSubmission get _value =>
      super._value as _LessonContentSubmission;

  @override
  $Res call({
    Object lessonContentId = freezed,
    Object lessonContent = freezed,
    Object submissions = freezed,
  }) {
    return _then(_LessonContentSubmission(
      lessonContentId: lessonContentId == freezed
          ? _value.lessonContentId
          : lessonContentId as String,
      lessonContent: lessonContent == freezed
          ? _value.lessonContent
          : lessonContent as LessonContent,
      submissions: submissions == freezed
          ? _value.submissions
          : submissions as List<QuestionSubmission>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_LessonContentSubmission implements _LessonContentSubmission {
  _$_LessonContentSubmission(
      {this.lessonContentId, this.lessonContent, this.submissions});

  factory _$_LessonContentSubmission.fromJson(Map<String, dynamic> json) =>
      _$_$_LessonContentSubmissionFromJson(json);

  @override
  final String lessonContentId;
  @override
  final LessonContent lessonContent;
  @override
  final List<QuestionSubmission> submissions;

  @override
  String toString() {
    return 'LessonContentSubmission(lessonContentId: $lessonContentId, lessonContent: $lessonContent, submissions: $submissions)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LessonContentSubmission &&
            (identical(other.lessonContentId, lessonContentId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContentId, lessonContentId)) &&
            (identical(other.lessonContent, lessonContent) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContent, lessonContent)) &&
            (identical(other.submissions, submissions) ||
                const DeepCollectionEquality()
                    .equals(other.submissions, submissions)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(lessonContentId) ^
      const DeepCollectionEquality().hash(lessonContent) ^
      const DeepCollectionEquality().hash(submissions);

  @override
  _$LessonContentSubmissionCopyWith<_LessonContentSubmission> get copyWith =>
      __$LessonContentSubmissionCopyWithImpl<_LessonContentSubmission>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LessonContentSubmissionToJson(this);
  }
}

abstract class _LessonContentSubmission implements LessonContentSubmission {
  factory _LessonContentSubmission(
      {String lessonContentId,
      LessonContent lessonContent,
      List<QuestionSubmission> submissions}) = _$_LessonContentSubmission;

  factory _LessonContentSubmission.fromJson(Map<String, dynamic> json) =
      _$_LessonContentSubmission.fromJson;

  @override
  String get lessonContentId;
  @override
  LessonContent get lessonContent;
  @override
  List<QuestionSubmission> get submissions;
  @override
  _$LessonContentSubmissionCopyWith<_LessonContentSubmission> get copyWith;
}

SubmissionResponse _$SubmissionResponseFromJson(Map<String, dynamic> json) {
  return _SubmissionResponse.fromJson(json);
}

/// @nodoc
class _$SubmissionResponseTearOff {
  const _$SubmissionResponseTearOff();

// ignore: unused_element
  _SubmissionResponse call(
      {String lessonId,
      Lesson lesson,
      List<LessonContentSubmission> lessonContents}) {
    return _SubmissionResponse(
      lessonId: lessonId,
      lesson: lesson,
      lessonContents: lessonContents,
    );
  }

// ignore: unused_element
  SubmissionResponse fromJson(Map<String, Object> json) {
    return SubmissionResponse.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $SubmissionResponse = _$SubmissionResponseTearOff();

/// @nodoc
mixin _$SubmissionResponse {
  String get lessonId;
  Lesson get lesson;
  List<LessonContentSubmission> get lessonContents;

  Map<String, dynamic> toJson();
  $SubmissionResponseCopyWith<SubmissionResponse> get copyWith;
}

/// @nodoc
abstract class $SubmissionResponseCopyWith<$Res> {
  factory $SubmissionResponseCopyWith(
          SubmissionResponse value, $Res Function(SubmissionResponse) then) =
      _$SubmissionResponseCopyWithImpl<$Res>;
  $Res call(
      {String lessonId,
      Lesson lesson,
      List<LessonContentSubmission> lessonContents});

  $LessonCopyWith<$Res> get lesson;
}

/// @nodoc
class _$SubmissionResponseCopyWithImpl<$Res>
    implements $SubmissionResponseCopyWith<$Res> {
  _$SubmissionResponseCopyWithImpl(this._value, this._then);

  final SubmissionResponse _value;
  // ignore: unused_field
  final $Res Function(SubmissionResponse) _then;

  @override
  $Res call({
    Object lessonId = freezed,
    Object lesson = freezed,
    Object lessonContents = freezed,
  }) {
    return _then(_value.copyWith(
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      lesson: lesson == freezed ? _value.lesson : lesson as Lesson,
      lessonContents: lessonContents == freezed
          ? _value.lessonContents
          : lessonContents as List<LessonContentSubmission>,
    ));
  }

  @override
  $LessonCopyWith<$Res> get lesson {
    if (_value.lesson == null) {
      return null;
    }
    return $LessonCopyWith<$Res>(_value.lesson, (value) {
      return _then(_value.copyWith(lesson: value));
    });
  }
}

/// @nodoc
abstract class _$SubmissionResponseCopyWith<$Res>
    implements $SubmissionResponseCopyWith<$Res> {
  factory _$SubmissionResponseCopyWith(
          _SubmissionResponse value, $Res Function(_SubmissionResponse) then) =
      __$SubmissionResponseCopyWithImpl<$Res>;
  @override
  $Res call(
      {String lessonId,
      Lesson lesson,
      List<LessonContentSubmission> lessonContents});

  @override
  $LessonCopyWith<$Res> get lesson;
}

/// @nodoc
class __$SubmissionResponseCopyWithImpl<$Res>
    extends _$SubmissionResponseCopyWithImpl<$Res>
    implements _$SubmissionResponseCopyWith<$Res> {
  __$SubmissionResponseCopyWithImpl(
      _SubmissionResponse _value, $Res Function(_SubmissionResponse) _then)
      : super(_value, (v) => _then(v as _SubmissionResponse));

  @override
  _SubmissionResponse get _value => super._value as _SubmissionResponse;

  @override
  $Res call({
    Object lessonId = freezed,
    Object lesson = freezed,
    Object lessonContents = freezed,
  }) {
    return _then(_SubmissionResponse(
      lessonId: lessonId == freezed ? _value.lessonId : lessonId as String,
      lesson: lesson == freezed ? _value.lesson : lesson as Lesson,
      lessonContents: lessonContents == freezed
          ? _value.lessonContents
          : lessonContents as List<LessonContentSubmission>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_SubmissionResponse implements _SubmissionResponse {
  _$_SubmissionResponse({this.lessonId, this.lesson, this.lessonContents});

  factory _$_SubmissionResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_SubmissionResponseFromJson(json);

  @override
  final String lessonId;
  @override
  final Lesson lesson;
  @override
  final List<LessonContentSubmission> lessonContents;

  @override
  String toString() {
    return 'SubmissionResponse(lessonId: $lessonId, lesson: $lesson, lessonContents: $lessonContents)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubmissionResponse &&
            (identical(other.lessonId, lessonId) ||
                const DeepCollectionEquality()
                    .equals(other.lessonId, lessonId)) &&
            (identical(other.lesson, lesson) ||
                const DeepCollectionEquality().equals(other.lesson, lesson)) &&
            (identical(other.lessonContents, lessonContents) ||
                const DeepCollectionEquality()
                    .equals(other.lessonContents, lessonContents)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(lessonId) ^
      const DeepCollectionEquality().hash(lesson) ^
      const DeepCollectionEquality().hash(lessonContents);

  @override
  _$SubmissionResponseCopyWith<_SubmissionResponse> get copyWith =>
      __$SubmissionResponseCopyWithImpl<_SubmissionResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SubmissionResponseToJson(this);
  }
}

abstract class _SubmissionResponse implements SubmissionResponse {
  factory _SubmissionResponse(
      {String lessonId,
      Lesson lesson,
      List<LessonContentSubmission> lessonContents}) = _$_SubmissionResponse;

  factory _SubmissionResponse.fromJson(Map<String, dynamic> json) =
      _$_SubmissionResponse.fromJson;

  @override
  String get lessonId;
  @override
  Lesson get lesson;
  @override
  List<LessonContentSubmission> get lessonContents;
  @override
  _$SubmissionResponseCopyWith<_SubmissionResponse> get copyWith;
}

Student _$StudentFromJson(Map<String, dynamic> json) {
  return _Student.fromJson(json);
}

/// @nodoc
class _$StudentTearOff {
  const _$StudentTearOff();

// ignore: unused_element
  _Student call({User profile, List<SubmissionResponse> submissionResponses}) {
    return _Student(
      profile: profile,
      submissionResponses: submissionResponses,
    );
  }

// ignore: unused_element
  Student fromJson(Map<String, Object> json) {
    return Student.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Student = _$StudentTearOff();

/// @nodoc
mixin _$Student {
  User get profile;
  List<SubmissionResponse> get submissionResponses;

  Map<String, dynamic> toJson();
  $StudentCopyWith<Student> get copyWith;
}

/// @nodoc
abstract class $StudentCopyWith<$Res> {
  factory $StudentCopyWith(Student value, $Res Function(Student) then) =
      _$StudentCopyWithImpl<$Res>;
  $Res call({User profile, List<SubmissionResponse> submissionResponses});

  $UserCopyWith<$Res> get profile;
}

/// @nodoc
class _$StudentCopyWithImpl<$Res> implements $StudentCopyWith<$Res> {
  _$StudentCopyWithImpl(this._value, this._then);

  final Student _value;
  // ignore: unused_field
  final $Res Function(Student) _then;

  @override
  $Res call({
    Object profile = freezed,
    Object submissionResponses = freezed,
  }) {
    return _then(_value.copyWith(
      profile: profile == freezed ? _value.profile : profile as User,
      submissionResponses: submissionResponses == freezed
          ? _value.submissionResponses
          : submissionResponses as List<SubmissionResponse>,
    ));
  }

  @override
  $UserCopyWith<$Res> get profile {
    if (_value.profile == null) {
      return null;
    }
    return $UserCopyWith<$Res>(_value.profile, (value) {
      return _then(_value.copyWith(profile: value));
    });
  }
}

/// @nodoc
abstract class _$StudentCopyWith<$Res> implements $StudentCopyWith<$Res> {
  factory _$StudentCopyWith(_Student value, $Res Function(_Student) then) =
      __$StudentCopyWithImpl<$Res>;
  @override
  $Res call({User profile, List<SubmissionResponse> submissionResponses});

  @override
  $UserCopyWith<$Res> get profile;
}

/// @nodoc
class __$StudentCopyWithImpl<$Res> extends _$StudentCopyWithImpl<$Res>
    implements _$StudentCopyWith<$Res> {
  __$StudentCopyWithImpl(_Student _value, $Res Function(_Student) _then)
      : super(_value, (v) => _then(v as _Student));

  @override
  _Student get _value => super._value as _Student;

  @override
  $Res call({
    Object profile = freezed,
    Object submissionResponses = freezed,
  }) {
    return _then(_Student(
      profile: profile == freezed ? _value.profile : profile as User,
      submissionResponses: submissionResponses == freezed
          ? _value.submissionResponses
          : submissionResponses as List<SubmissionResponse>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Student implements _Student {
  _$_Student({this.profile, this.submissionResponses});

  factory _$_Student.fromJson(Map<String, dynamic> json) =>
      _$_$_StudentFromJson(json);

  @override
  final User profile;
  @override
  final List<SubmissionResponse> submissionResponses;

  @override
  String toString() {
    return 'Student(profile: $profile, submissionResponses: $submissionResponses)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Student &&
            (identical(other.profile, profile) ||
                const DeepCollectionEquality()
                    .equals(other.profile, profile)) &&
            (identical(other.submissionResponses, submissionResponses) ||
                const DeepCollectionEquality()
                    .equals(other.submissionResponses, submissionResponses)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(profile) ^
      const DeepCollectionEquality().hash(submissionResponses);

  @override
  _$StudentCopyWith<_Student> get copyWith =>
      __$StudentCopyWithImpl<_Student>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_StudentToJson(this);
  }
}

abstract class _Student implements Student {
  factory _Student(
      {User profile,
      List<SubmissionResponse> submissionResponses}) = _$_Student;

  factory _Student.fromJson(Map<String, dynamic> json) = _$_Student.fromJson;

  @override
  User get profile;
  @override
  List<SubmissionResponse> get submissionResponses;
  @override
  _$StudentCopyWith<_Student> get copyWith;
}

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return _LoginResponse.fromJson(json);
}

/// @nodoc
class _$LoginResponseTearOff {
  const _$LoginResponseTearOff();

// ignore: unused_element
  _LoginResponse call({String accessToken, User user}) {
    return _LoginResponse(
      accessToken: accessToken,
      user: user,
    );
  }

// ignore: unused_element
  LoginResponse fromJson(Map<String, Object> json) {
    return LoginResponse.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $LoginResponse = _$LoginResponseTearOff();

/// @nodoc
mixin _$LoginResponse {
  String get accessToken;
  User get user;

  Map<String, dynamic> toJson();
  $LoginResponseCopyWith<LoginResponse> get copyWith;
}

/// @nodoc
abstract class $LoginResponseCopyWith<$Res> {
  factory $LoginResponseCopyWith(
          LoginResponse value, $Res Function(LoginResponse) then) =
      _$LoginResponseCopyWithImpl<$Res>;
  $Res call({String accessToken, User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class _$LoginResponseCopyWithImpl<$Res>
    implements $LoginResponseCopyWith<$Res> {
  _$LoginResponseCopyWithImpl(this._value, this._then);

  final LoginResponse _value;
  // ignore: unused_field
  final $Res Function(LoginResponse) _then;

  @override
  $Res call({
    Object accessToken = freezed,
    Object user = freezed,
  }) {
    return _then(_value.copyWith(
      accessToken:
          accessToken == freezed ? _value.accessToken : accessToken as String,
      user: user == freezed ? _value.user : user as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
abstract class _$LoginResponseCopyWith<$Res>
    implements $LoginResponseCopyWith<$Res> {
  factory _$LoginResponseCopyWith(
          _LoginResponse value, $Res Function(_LoginResponse) then) =
      __$LoginResponseCopyWithImpl<$Res>;
  @override
  $Res call({String accessToken, User user});

  @override
  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$LoginResponseCopyWithImpl<$Res>
    extends _$LoginResponseCopyWithImpl<$Res>
    implements _$LoginResponseCopyWith<$Res> {
  __$LoginResponseCopyWithImpl(
      _LoginResponse _value, $Res Function(_LoginResponse) _then)
      : super(_value, (v) => _then(v as _LoginResponse));

  @override
  _LoginResponse get _value => super._value as _LoginResponse;

  @override
  $Res call({
    Object accessToken = freezed,
    Object user = freezed,
  }) {
    return _then(_LoginResponse(
      accessToken:
          accessToken == freezed ? _value.accessToken : accessToken as String,
      user: user == freezed ? _value.user : user as User,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_LoginResponse implements _LoginResponse {
  _$_LoginResponse({this.accessToken, this.user});

  factory _$_LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_LoginResponseFromJson(json);

  @override
  final String accessToken;
  @override
  final User user;

  @override
  String toString() {
    return 'LoginResponse(accessToken: $accessToken, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoginResponse &&
            (identical(other.accessToken, accessToken) ||
                const DeepCollectionEquality()
                    .equals(other.accessToken, accessToken)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(accessToken) ^
      const DeepCollectionEquality().hash(user);

  @override
  _$LoginResponseCopyWith<_LoginResponse> get copyWith =>
      __$LoginResponseCopyWithImpl<_LoginResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LoginResponseToJson(this);
  }
}

abstract class _LoginResponse implements LoginResponse {
  factory _LoginResponse({String accessToken, User user}) = _$_LoginResponse;

  factory _LoginResponse.fromJson(Map<String, dynamic> json) =
      _$_LoginResponse.fromJson;

  @override
  String get accessToken;
  @override
  User get user;
  @override
  _$LoginResponseCopyWith<_LoginResponse> get copyWith;
}

UserCourses _$UserCoursesFromJson(Map<String, dynamic> json) {
  return _UserCourses.fromJson(json);
}

/// @nodoc
class _$UserCoursesTearOff {
  const _$UserCoursesTearOff();

// ignore: unused_element
  _UserCourses call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String relation,
      Course course,
      double courseRating,
      DateTime createdAt,
      DateTime updatedAt}) {
    return _UserCourses(
      id: id,
      userId: userId,
      courseId: courseId,
      relation: relation,
      course: course,
      courseRating: courseRating,
      createdAt: createdAt,
      updatedAt: updatedAt,
    );
  }

// ignore: unused_element
  UserCourses fromJson(Map<String, Object> json) {
    return UserCourses.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $UserCourses = _$UserCoursesTearOff();

/// @nodoc
mixin _$UserCourses {
  @JsonKey(name: '_id')
  String get id;
  String get userId;
  String get courseId;
  String get relation;
  Course get course;
  double get courseRating;
  DateTime get createdAt;
  DateTime get updatedAt;

  Map<String, dynamic> toJson();
  $UserCoursesCopyWith<UserCourses> get copyWith;
}

/// @nodoc
abstract class $UserCoursesCopyWith<$Res> {
  factory $UserCoursesCopyWith(
          UserCourses value, $Res Function(UserCourses) then) =
      _$UserCoursesCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String relation,
      Course course,
      double courseRating,
      DateTime createdAt,
      DateTime updatedAt});

  $CourseCopyWith<$Res> get course;
}

/// @nodoc
class _$UserCoursesCopyWithImpl<$Res> implements $UserCoursesCopyWith<$Res> {
  _$UserCoursesCopyWithImpl(this._value, this._then);

  final UserCourses _value;
  // ignore: unused_field
  final $Res Function(UserCourses) _then;

  @override
  $Res call({
    Object id = freezed,
    Object userId = freezed,
    Object courseId = freezed,
    Object relation = freezed,
    Object course = freezed,
    Object courseRating = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      userId: userId == freezed ? _value.userId : userId as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      relation: relation == freezed ? _value.relation : relation as String,
      course: course == freezed ? _value.course : course as Course,
      courseRating: courseRating == freezed
          ? _value.courseRating
          : courseRating as double,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }

  @override
  $CourseCopyWith<$Res> get course {
    if (_value.course == null) {
      return null;
    }
    return $CourseCopyWith<$Res>(_value.course, (value) {
      return _then(_value.copyWith(course: value));
    });
  }
}

/// @nodoc
abstract class _$UserCoursesCopyWith<$Res>
    implements $UserCoursesCopyWith<$Res> {
  factory _$UserCoursesCopyWith(
          _UserCourses value, $Res Function(_UserCourses) then) =
      __$UserCoursesCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String relation,
      Course course,
      double courseRating,
      DateTime createdAt,
      DateTime updatedAt});

  @override
  $CourseCopyWith<$Res> get course;
}

/// @nodoc
class __$UserCoursesCopyWithImpl<$Res> extends _$UserCoursesCopyWithImpl<$Res>
    implements _$UserCoursesCopyWith<$Res> {
  __$UserCoursesCopyWithImpl(
      _UserCourses _value, $Res Function(_UserCourses) _then)
      : super(_value, (v) => _then(v as _UserCourses));

  @override
  _UserCourses get _value => super._value as _UserCourses;

  @override
  $Res call({
    Object id = freezed,
    Object userId = freezed,
    Object courseId = freezed,
    Object relation = freezed,
    Object course = freezed,
    Object courseRating = freezed,
    Object createdAt = freezed,
    Object updatedAt = freezed,
  }) {
    return _then(_UserCourses(
      id: id == freezed ? _value.id : id as String,
      userId: userId == freezed ? _value.userId : userId as String,
      courseId: courseId == freezed ? _value.courseId : courseId as String,
      relation: relation == freezed ? _value.relation : relation as String,
      course: course == freezed ? _value.course : course as Course,
      courseRating: courseRating == freezed
          ? _value.courseRating
          : courseRating as double,
      createdAt:
          createdAt == freezed ? _value.createdAt : createdAt as DateTime,
      updatedAt:
          updatedAt == freezed ? _value.updatedAt : updatedAt as DateTime,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_UserCourses implements _UserCourses {
  _$_UserCourses(
      {@JsonKey(name: '_id') this.id,
      this.userId,
      this.courseId,
      this.relation,
      this.course,
      this.courseRating,
      this.createdAt,
      this.updatedAt});

  factory _$_UserCourses.fromJson(Map<String, dynamic> json) =>
      _$_$_UserCoursesFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String id;
  @override
  final String userId;
  @override
  final String courseId;
  @override
  final String relation;
  @override
  final Course course;
  @override
  final double courseRating;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  @override
  String toString() {
    return 'UserCourses(id: $id, userId: $userId, courseId: $courseId, relation: $relation, course: $course, courseRating: $courseRating, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _UserCourses &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.userId, userId) ||
                const DeepCollectionEquality().equals(other.userId, userId)) &&
            (identical(other.courseId, courseId) ||
                const DeepCollectionEquality()
                    .equals(other.courseId, courseId)) &&
            (identical(other.relation, relation) ||
                const DeepCollectionEquality()
                    .equals(other.relation, relation)) &&
            (identical(other.course, course) ||
                const DeepCollectionEquality().equals(other.course, course)) &&
            (identical(other.courseRating, courseRating) ||
                const DeepCollectionEquality()
                    .equals(other.courseRating, courseRating)) &&
            (identical(other.createdAt, createdAt) ||
                const DeepCollectionEquality()
                    .equals(other.createdAt, createdAt)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(userId) ^
      const DeepCollectionEquality().hash(courseId) ^
      const DeepCollectionEquality().hash(relation) ^
      const DeepCollectionEquality().hash(course) ^
      const DeepCollectionEquality().hash(courseRating) ^
      const DeepCollectionEquality().hash(createdAt) ^
      const DeepCollectionEquality().hash(updatedAt);

  @override
  _$UserCoursesCopyWith<_UserCourses> get copyWith =>
      __$UserCoursesCopyWithImpl<_UserCourses>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserCoursesToJson(this);
  }
}

abstract class _UserCourses implements UserCourses {
  factory _UserCourses(
      {@JsonKey(name: '_id') String id,
      String userId,
      String courseId,
      String relation,
      Course course,
      double courseRating,
      DateTime createdAt,
      DateTime updatedAt}) = _$_UserCourses;

  factory _UserCourses.fromJson(Map<String, dynamic> json) =
      _$_UserCourses.fromJson;

  @override
  @JsonKey(name: '_id')
  String get id;
  @override
  String get userId;
  @override
  String get courseId;
  @override
  String get relation;
  @override
  Course get course;
  @override
  double get courseRating;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  _$UserCoursesCopyWith<_UserCourses> get copyWith;
}

RatingResponse _$RatingResponseFromJson(Map<String, dynamic> json) {
  return _RatingResponse.fromJson(json);
}

/// @nodoc
class _$RatingResponseTearOff {
  const _$RatingResponseTearOff();

// ignore: unused_element
  _RatingResponse call({double value, int totalRates}) {
    return _RatingResponse(
      value: value,
      totalRates: totalRates,
    );
  }

// ignore: unused_element
  RatingResponse fromJson(Map<String, Object> json) {
    return RatingResponse.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $RatingResponse = _$RatingResponseTearOff();

/// @nodoc
mixin _$RatingResponse {
  double get value;
  int get totalRates;

  Map<String, dynamic> toJson();
  $RatingResponseCopyWith<RatingResponse> get copyWith;
}

/// @nodoc
abstract class $RatingResponseCopyWith<$Res> {
  factory $RatingResponseCopyWith(
          RatingResponse value, $Res Function(RatingResponse) then) =
      _$RatingResponseCopyWithImpl<$Res>;
  $Res call({double value, int totalRates});
}

/// @nodoc
class _$RatingResponseCopyWithImpl<$Res>
    implements $RatingResponseCopyWith<$Res> {
  _$RatingResponseCopyWithImpl(this._value, this._then);

  final RatingResponse _value;
  // ignore: unused_field
  final $Res Function(RatingResponse) _then;

  @override
  $Res call({
    Object value = freezed,
    Object totalRates = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed ? _value.value : value as double,
      totalRates: totalRates == freezed ? _value.totalRates : totalRates as int,
    ));
  }
}

/// @nodoc
abstract class _$RatingResponseCopyWith<$Res>
    implements $RatingResponseCopyWith<$Res> {
  factory _$RatingResponseCopyWith(
          _RatingResponse value, $Res Function(_RatingResponse) then) =
      __$RatingResponseCopyWithImpl<$Res>;
  @override
  $Res call({double value, int totalRates});
}

/// @nodoc
class __$RatingResponseCopyWithImpl<$Res>
    extends _$RatingResponseCopyWithImpl<$Res>
    implements _$RatingResponseCopyWith<$Res> {
  __$RatingResponseCopyWithImpl(
      _RatingResponse _value, $Res Function(_RatingResponse) _then)
      : super(_value, (v) => _then(v as _RatingResponse));

  @override
  _RatingResponse get _value => super._value as _RatingResponse;

  @override
  $Res call({
    Object value = freezed,
    Object totalRates = freezed,
  }) {
    return _then(_RatingResponse(
      value: value == freezed ? _value.value : value as double,
      totalRates: totalRates == freezed ? _value.totalRates : totalRates as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_RatingResponse implements _RatingResponse {
  _$_RatingResponse({this.value, this.totalRates});

  factory _$_RatingResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_RatingResponseFromJson(json);

  @override
  final double value;
  @override
  final int totalRates;

  @override
  String toString() {
    return 'RatingResponse(value: $value, totalRates: $totalRates)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RatingResponse &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)) &&
            (identical(other.totalRates, totalRates) ||
                const DeepCollectionEquality()
                    .equals(other.totalRates, totalRates)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(value) ^
      const DeepCollectionEquality().hash(totalRates);

  @override
  _$RatingResponseCopyWith<_RatingResponse> get copyWith =>
      __$RatingResponseCopyWithImpl<_RatingResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RatingResponseToJson(this);
  }
}

abstract class _RatingResponse implements RatingResponse {
  factory _RatingResponse({double value, int totalRates}) = _$_RatingResponse;

  factory _RatingResponse.fromJson(Map<String, dynamic> json) =
      _$_RatingResponse.fromJson;

  @override
  double get value;
  @override
  int get totalRates;
  @override
  _$RatingResponseCopyWith<_RatingResponse> get copyWith;
}

ListResponse<T> _$ListResponseFromJson<T>(Map<String, dynamic> json) {
  return _ListResponse<T>.fromJson(json);
}

/// @nodoc
class _$ListResponseTearOff {
  const _$ListResponseTearOff();

// ignore: unused_element
  _ListResponse<T> call<T>(
      {int total, int limit, int skip, @DataConverter() List<T> data}) {
    return _ListResponse<T>(
      total: total,
      limit: limit,
      skip: skip,
      data: data,
    );
  }

// ignore: unused_element
  ListResponse<T> fromJson<T>(Map<String, Object> json) {
    return ListResponse<T>.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $ListResponse = _$ListResponseTearOff();

/// @nodoc
mixin _$ListResponse<T> {
  int get total;
  int get limit;
  int get skip;
  @DataConverter()
  List<T> get data;

  Map<String, dynamic> toJson();
  $ListResponseCopyWith<T, ListResponse<T>> get copyWith;
}

/// @nodoc
abstract class $ListResponseCopyWith<T, $Res> {
  factory $ListResponseCopyWith(
          ListResponse<T> value, $Res Function(ListResponse<T>) then) =
      _$ListResponseCopyWithImpl<T, $Res>;
  $Res call({int total, int limit, int skip, @DataConverter() List<T> data});
}

/// @nodoc
class _$ListResponseCopyWithImpl<T, $Res>
    implements $ListResponseCopyWith<T, $Res> {
  _$ListResponseCopyWithImpl(this._value, this._then);

  final ListResponse<T> _value;
  // ignore: unused_field
  final $Res Function(ListResponse<T>) _then;

  @override
  $Res call({
    Object total = freezed,
    Object limit = freezed,
    Object skip = freezed,
    Object data = freezed,
  }) {
    return _then(_value.copyWith(
      total: total == freezed ? _value.total : total as int,
      limit: limit == freezed ? _value.limit : limit as int,
      skip: skip == freezed ? _value.skip : skip as int,
      data: data == freezed ? _value.data : data as List<T>,
    ));
  }
}

/// @nodoc
abstract class _$ListResponseCopyWith<T, $Res>
    implements $ListResponseCopyWith<T, $Res> {
  factory _$ListResponseCopyWith(
          _ListResponse<T> value, $Res Function(_ListResponse<T>) then) =
      __$ListResponseCopyWithImpl<T, $Res>;
  @override
  $Res call({int total, int limit, int skip, @DataConverter() List<T> data});
}

/// @nodoc
class __$ListResponseCopyWithImpl<T, $Res>
    extends _$ListResponseCopyWithImpl<T, $Res>
    implements _$ListResponseCopyWith<T, $Res> {
  __$ListResponseCopyWithImpl(
      _ListResponse<T> _value, $Res Function(_ListResponse<T>) _then)
      : super(_value, (v) => _then(v as _ListResponse<T>));

  @override
  _ListResponse<T> get _value => super._value as _ListResponse<T>;

  @override
  $Res call({
    Object total = freezed,
    Object limit = freezed,
    Object skip = freezed,
    Object data = freezed,
  }) {
    return _then(_ListResponse<T>(
      total: total == freezed ? _value.total : total as int,
      limit: limit == freezed ? _value.limit : limit as int,
      skip: skip == freezed ? _value.skip : skip as int,
      data: data == freezed ? _value.data : data as List<T>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_ListResponse<T> implements _ListResponse<T> {
  _$_ListResponse(
      {this.total, this.limit, this.skip, @DataConverter() this.data});

  factory _$_ListResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_ListResponseFromJson(json);

  @override
  final int total;
  @override
  final int limit;
  @override
  final int skip;
  @override
  @DataConverter()
  final List<T> data;

  @override
  String toString() {
    return 'ListResponse<$T>(total: $total, limit: $limit, skip: $skip, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ListResponse<T> &&
            (identical(other.total, total) ||
                const DeepCollectionEquality().equals(other.total, total)) &&
            (identical(other.limit, limit) ||
                const DeepCollectionEquality().equals(other.limit, limit)) &&
            (identical(other.skip, skip) ||
                const DeepCollectionEquality().equals(other.skip, skip)) &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(total) ^
      const DeepCollectionEquality().hash(limit) ^
      const DeepCollectionEquality().hash(skip) ^
      const DeepCollectionEquality().hash(data);

  @override
  _$ListResponseCopyWith<T, _ListResponse<T>> get copyWith =>
      __$ListResponseCopyWithImpl<T, _ListResponse<T>>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ListResponseToJson(this);
  }
}

abstract class _ListResponse<T> implements ListResponse<T> {
  factory _ListResponse(
      {int total,
      int limit,
      int skip,
      @DataConverter() List<T> data}) = _$_ListResponse<T>;

  factory _ListResponse.fromJson(Map<String, dynamic> json) =
      _$_ListResponse<T>.fromJson;

  @override
  int get total;
  @override
  int get limit;
  @override
  int get skip;
  @override
  @DataConverter()
  List<T> get data;
  @override
  _$ListResponseCopyWith<T, _ListResponse<T>> get copyWith;
}

BatchResponse<T> _$BatchResponseFromJson<T>(Map<String, dynamic> json) {
  return _BatchResponse<T>.fromJson(json);
}

/// @nodoc
class _$BatchResponseTearOff {
  const _$BatchResponseTearOff();

// ignore: unused_element
  _BatchResponse<T> call<T>({String status, @DataConverter() T value}) {
    return _BatchResponse<T>(
      status: status,
      value: value,
    );
  }

// ignore: unused_element
  BatchResponse<T> fromJson<T>(Map<String, Object> json) {
    return BatchResponse<T>.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $BatchResponse = _$BatchResponseTearOff();

/// @nodoc
mixin _$BatchResponse<T> {
  String get status;
  @DataConverter()
  T get value;

  Map<String, dynamic> toJson();
  $BatchResponseCopyWith<T, BatchResponse<T>> get copyWith;
}

/// @nodoc
abstract class $BatchResponseCopyWith<T, $Res> {
  factory $BatchResponseCopyWith(
          BatchResponse<T> value, $Res Function(BatchResponse<T>) then) =
      _$BatchResponseCopyWithImpl<T, $Res>;
  $Res call({String status, @DataConverter() T value});
}

/// @nodoc
class _$BatchResponseCopyWithImpl<T, $Res>
    implements $BatchResponseCopyWith<T, $Res> {
  _$BatchResponseCopyWithImpl(this._value, this._then);

  final BatchResponse<T> _value;
  // ignore: unused_field
  final $Res Function(BatchResponse<T>) _then;

  @override
  $Res call({
    Object status = freezed,
    Object value = freezed,
  }) {
    return _then(_value.copyWith(
      status: status == freezed ? _value.status : status as String,
      value: value == freezed ? _value.value : value as T,
    ));
  }
}

/// @nodoc
abstract class _$BatchResponseCopyWith<T, $Res>
    implements $BatchResponseCopyWith<T, $Res> {
  factory _$BatchResponseCopyWith(
          _BatchResponse<T> value, $Res Function(_BatchResponse<T>) then) =
      __$BatchResponseCopyWithImpl<T, $Res>;
  @override
  $Res call({String status, @DataConverter() T value});
}

/// @nodoc
class __$BatchResponseCopyWithImpl<T, $Res>
    extends _$BatchResponseCopyWithImpl<T, $Res>
    implements _$BatchResponseCopyWith<T, $Res> {
  __$BatchResponseCopyWithImpl(
      _BatchResponse<T> _value, $Res Function(_BatchResponse<T>) _then)
      : super(_value, (v) => _then(v as _BatchResponse<T>));

  @override
  _BatchResponse<T> get _value => super._value as _BatchResponse<T>;

  @override
  $Res call({
    Object status = freezed,
    Object value = freezed,
  }) {
    return _then(_BatchResponse<T>(
      status: status == freezed ? _value.status : status as String,
      value: value == freezed ? _value.value : value as T,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_BatchResponse<T> implements _BatchResponse<T> {
  _$_BatchResponse({this.status, @DataConverter() this.value});

  factory _$_BatchResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_BatchResponseFromJson(json);

  @override
  final String status;
  @override
  @DataConverter()
  final T value;

  @override
  String toString() {
    return 'BatchResponse<$T>(status: $status, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _BatchResponse<T> &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(value);

  @override
  _$BatchResponseCopyWith<T, _BatchResponse<T>> get copyWith =>
      __$BatchResponseCopyWithImpl<T, _BatchResponse<T>>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_BatchResponseToJson(this);
  }
}

abstract class _BatchResponse<T> implements BatchResponse<T> {
  factory _BatchResponse({String status, @DataConverter() T value}) =
      _$_BatchResponse<T>;

  factory _BatchResponse.fromJson(Map<String, dynamic> json) =
      _$_BatchResponse<T>.fromJson;

  @override
  String get status;
  @override
  @DataConverter()
  T get value;
  @override
  _$BatchResponseCopyWith<T, _BatchResponse<T>> get copyWith;
}

Item _$ItemFromJson(Map<String, dynamic> json) {
  return _Item.fromJson(json);
}

/// @nodoc
class _$ItemTearOff {
  const _$ItemTearOff();

// ignore: unused_element
  _Item call(String name) {
    return _Item(
      name,
    );
  }

// ignore: unused_element
  Item fromJson(Map<String, Object> json) {
    return Item.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Item = _$ItemTearOff();

/// @nodoc
mixin _$Item {
  String get name;

  Map<String, dynamic> toJson();
  $ItemCopyWith<Item> get copyWith;
}

/// @nodoc
abstract class $ItemCopyWith<$Res> {
  factory $ItemCopyWith(Item value, $Res Function(Item) then) =
      _$ItemCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class _$ItemCopyWithImpl<$Res> implements $ItemCopyWith<$Res> {
  _$ItemCopyWithImpl(this._value, this._then);

  final Item _value;
  // ignore: unused_field
  final $Res Function(Item) _then;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
abstract class _$ItemCopyWith<$Res> implements $ItemCopyWith<$Res> {
  factory _$ItemCopyWith(_Item value, $Res Function(_Item) then) =
      __$ItemCopyWithImpl<$Res>;
  @override
  $Res call({String name});
}

/// @nodoc
class __$ItemCopyWithImpl<$Res> extends _$ItemCopyWithImpl<$Res>
    implements _$ItemCopyWith<$Res> {
  __$ItemCopyWithImpl(_Item _value, $Res Function(_Item) _then)
      : super(_value, (v) => _then(v as _Item));

  @override
  _Item get _value => super._value as _Item;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_Item(
      name == freezed ? _value.name : name as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Item implements _Item {
  _$_Item(this.name) : assert(name != null);

  factory _$_Item.fromJson(Map<String, dynamic> json) =>
      _$_$_ItemFromJson(json);

  @override
  final String name;

  @override
  String toString() {
    return 'Item(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Item &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @override
  _$ItemCopyWith<_Item> get copyWith =>
      __$ItemCopyWithImpl<_Item>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ItemToJson(this);
  }
}

abstract class _Item implements Item {
  factory _Item(String name) = _$_Item;

  factory _Item.fromJson(Map<String, dynamic> json) = _$_Item.fromJson;

  @override
  String get name;
  @override
  _$ItemCopyWith<_Item> get copyWith;
}
