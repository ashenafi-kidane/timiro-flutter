// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'freezed_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$_$_UserFromJson(Map<String, dynamic> json) {
  return _$_User(
    id: json['_id'] as String,
    email: json['email'] as String,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    gender: json['gender'] as String,
    phone: json['phone'] as String,
    address: json['address'] as String,
    profileImageId: json['profileImageId'] as String,
    language: json['language'] as String,
    accessToken: json['accessToken'] as String,
    isVerified: json['isVerified'] as bool,
    profileImage: json['profileImage'] == null
        ? null
        : Files.fromJson(json['profileImage'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_UserToJson(_$_User instance) => <String, dynamic>{
      '_id': instance.id,
      'email': instance.email,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'gender': instance.gender,
      'phone': instance.phone,
      'address': instance.address,
      'profileImageId': instance.profileImageId,
      'language': instance.language,
      'accessToken': instance.accessToken,
      'isVerified': instance.isVerified,
      'profileImage': instance.profileImage,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Course _$_$_CourseFromJson(Map<String, dynamic> json) {
  return _$_Course(
    id: json['_id'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    categoryId: json['categoryId'] as String,
    price: (json['price'] as num)?.toDouble(),
    status: json['status'] as String,
    createdBy: json['createdBy'] as String,
    visibility: json['visibility'] as String,
    instructorIds:
        (json['instructorIds'] as List)?.map((e) => e as String)?.toList(),
    accessCode: json['accessCode'] as String,
    courseImageId: json['courseImageId'] as String,
    discussionRoomID: json['discussionRoomID'] as String,
    classRoomId: json['classRoomId'] as String,
    rating: json['rating'] == null
        ? null
        : RatingResponse.fromJson(json['rating'] as Map<String, dynamic>),
    courseImage: json['courseImage'] == null
        ? null
        : Files.fromJson(json['courseImage'] as Map<String, dynamic>),
    category: json['category'] == null
        ? null
        : CourseCategory.fromJson(json['category'] as Map<String, dynamic>),
    lessons: (json['lessons'] as List)
        ?.map((e) =>
            e == null ? null : Lesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    instructors: (json['instructors'] as List)
        ?.map(
            (e) => e == null ? null : User.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    studentsCount: json['studentsCount'] as int,
    lessonsCount: json['lessonsCount'] as int,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_CourseToJson(_$_Course instance) => <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'categoryId': instance.categoryId,
      'price': instance.price,
      'status': instance.status,
      'createdBy': instance.createdBy,
      'visibility': instance.visibility,
      'instructorIds': instance.instructorIds,
      'accessCode': instance.accessCode,
      'courseImageId': instance.courseImageId,
      'discussionRoomID': instance.discussionRoomID,
      'classRoomId': instance.classRoomId,
      'rating': instance.rating,
      'courseImage': instance.courseImage,
      'category': instance.category,
      'lessons': instance.lessons,
      'instructors': instance.instructors,
      'studentsCount': instance.studentsCount,
      'lessonsCount': instance.lessonsCount,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_CourseCategory _$_$_CourseCategoryFromJson(Map<String, dynamic> json) {
  return _$_CourseCategory(
    id: json['_id'] as String,
    name: json['name'] as String,
    type: json['type'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_CourseCategoryToJson(_$_CourseCategory instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Lesson _$_$_LessonFromJson(Map<String, dynamic> json) {
  return _$_Lesson(
    id: json['_id'] as String,
    courseId: json['courseId'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    lessonContents: (json['lessonContents'] as List)
        ?.map((e) => e == null
            ? null
            : LessonContent.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_LessonToJson(_$_Lesson instance) => <String, dynamic>{
      '_id': instance.id,
      'courseId': instance.courseId,
      'title': instance.title,
      'description': instance.description,
      'lessonContents': instance.lessonContents,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_LessonContent _$_$_LessonContentFromJson(Map<String, dynamic> json) {
  return _$_LessonContent(
    id: json['_id'] as String,
    lessonId: json['lessonId'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    type: json['type'] as String,
    order: json['order'] as int,
    fileId: json['fileId'] as String,
    videoUrl: json['videoUrl'] as String,
    articles: (json['articles'] as List)
        ?.map((e) =>
            e == null ? null : Article.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    questions: (json['questions'] as List)
        ?.map((e) =>
            e == null ? null : Question.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    video: json['video'] == null
        ? null
        : Files.fromJson(json['video'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_LessonContentToJson(_$_LessonContent instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'lessonId': instance.lessonId,
      'title': instance.title,
      'description': instance.description,
      'type': instance.type,
      'order': instance.order,
      'fileId': instance.fileId,
      'videoUrl': instance.videoUrl,
      'articles': instance.articles,
      'questions': instance.questions,
      'video': instance.video,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_ClassRoom _$_$_ClassRoomFromJson(Map<String, dynamic> json) {
  return _$_ClassRoom(
    id: json['_id'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    schoolId: json['schoolId'] as String,
    courseIds: (json['courseIds'] as List)?.map((e) => e as String)?.toList(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_ClassRoomToJson(_$_ClassRoom instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'schoolId': instance.schoolId,
      'courseIds': instance.courseIds,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_School _$_$_SchoolFromJson(Map<String, dynamic> json) {
  return _$_School(
    id: json['_id'] as String,
    name: json['name'] as String,
    about: json['about'] as String,
    classroomIds:
        (json['classroomIds'] as List)?.map((e) => e as String)?.toList(),
    instructorIds:
        (json['instructorIds'] as List)?.map((e) => e as String)?.toList(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_SchoolToJson(_$_School instance) => <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'about': instance.about,
      'classroomIds': instance.classroomIds,
      'instructorIds': instance.instructorIds,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_ChatRoom _$_$_ChatRoomFromJson(Map<String, dynamic> json) {
  return _$_ChatRoom(
    id: json['_id'] as String,
    name: json['name'] as String,
    status: json['status'] as String,
    type: json['type'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_ChatRoomToJson(_$_ChatRoom instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'status': instance.status,
      'type': instance.type,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Files _$_$_FilesFromJson(Map<String, dynamic> json) {
  return _$_Files(
    id: json['_id'] as String,
    name: json['name'] as String,
    originalName: json['originalName'] as String,
    contentType: json['contentType'] as String,
    url: json['url'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_FilesToJson(_$_Files instance) => <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'originalName': instance.originalName,
      'contentType': instance.contentType,
      'url': instance.url,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Question _$_$_QuestionFromJson(Map<String, dynamic> json) {
  return _$_Question(
    id: json['_id'] as String,
    lessonContentId: json['lessonContentId'] as String,
    textQuestion: json['textQuestion'] as String,
    fileQuestionIds:
        (json['fileQuestionIds'] as List)?.map((e) => e as String)?.toList(),
    order: json['order'] as int,
    point: (json['point'] as num)?.toDouble(),
    questionType: json['questionType'] as String,
    multipleChoices:
        (json['multipleChoices'] as List)?.map((e) => e as String)?.toList(),
    textAnswer: json['textAnswer'] as String,
    fileAnswerIds:
        (json['fileAnswerIds'] as List)?.map((e) => e as String)?.toList(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_QuestionToJson(_$_Question instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'lessonContentId': instance.lessonContentId,
      'textQuestion': instance.textQuestion,
      'fileQuestionIds': instance.fileQuestionIds,
      'order': instance.order,
      'point': instance.point,
      'questionType': instance.questionType,
      'multipleChoices': instance.multipleChoices,
      'textAnswer': instance.textAnswer,
      'fileAnswerIds': instance.fileAnswerIds,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Article _$_$_ArticleFromJson(Map<String, dynamic> json) {
  return _$_Article(
    id: json['_id'] as String,
    title: json['title'] as String,
    paragraph: json['paragraph'] as String,
    order: json['order'] as int,
    fileIds: (json['fileIds'] as List)?.map((e) => e as String)?.toList(),
    file: (json['file'] as List)
        ?.map(
            (e) => e == null ? null : Files.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_ArticleToJson(_$_Article instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'paragraph': instance.paragraph,
      'order': instance.order,
      'fileIds': instance.fileIds,
      'file': instance.file,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Rating _$_$_RatingFromJson(Map<String, dynamic> json) {
  return _$_Rating(
    id: json['_id'] as String,
    value: (json['value'] as num)?.toDouble(),
    ratedBy: json['ratedBy'] as String,
    ratingFor: json['ratingFor'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_RatingToJson(_$_Rating instance) => <String, dynamic>{
      '_id': instance.id,
      'value': instance.value,
      'ratedBy': instance.ratedBy,
      'ratingFor': instance.ratingFor,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_Message _$_$_MessageFromJson(Map<String, dynamic> json) {
  return _$_Message(
    id: json['_id'] as String,
    sender: json['sender'] as String,
    text: json['text'] as String,
    chatroomId: json['chatroomId'] as String,
    fileId: json['fileId'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_MessageToJson(_$_Message instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'sender': instance.sender,
      'text': instance.text,
      'chatroomId': instance.chatroomId,
      'fileId': instance.fileId,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_QuestionSubmission _$_$_QuestionSubmissionFromJson(
    Map<String, dynamic> json) {
  return _$_QuestionSubmission(
    id: json['_id'] as String,
    userId: json['userId'] as String,
    courseId: json['courseId'] as String,
    lessonId: json['lessonId'] as String,
    lessonContentId: json['lessonContentId'] as String,
    questionId: json['questionId'] as String,
    choiceAnswer: json['choiceAnswer'] as int,
    textAnswer: json['textAnswer'] as String,
    fileAnswerIds:
        (json['fileAnswerIds'] as List)?.map((e) => e as String)?.toList(),
    resultPoint: (json['resultPoint'] as num)?.toDouble(),
    question: json['question'] == null
        ? null
        : Question.fromJson(json['question'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_QuestionSubmissionToJson(
        _$_QuestionSubmission instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'userId': instance.userId,
      'courseId': instance.courseId,
      'lessonId': instance.lessonId,
      'lessonContentId': instance.lessonContentId,
      'questionId': instance.questionId,
      'choiceAnswer': instance.choiceAnswer,
      'textAnswer': instance.textAnswer,
      'fileAnswerIds': instance.fileAnswerIds,
      'resultPoint': instance.resultPoint,
      'question': instance.question,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_LessonContentSubmission _$_$_LessonContentSubmissionFromJson(
    Map<String, dynamic> json) {
  return _$_LessonContentSubmission(
    lessonContentId: json['lessonContentId'] as String,
    lessonContent: json['lessonContent'] == null
        ? null
        : LessonContent.fromJson(json['lessonContent'] as Map<String, dynamic>),
    submissions: (json['submissions'] as List)
        ?.map((e) => e == null
            ? null
            : QuestionSubmission.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_LessonContentSubmissionToJson(
        _$_LessonContentSubmission instance) =>
    <String, dynamic>{
      'lessonContentId': instance.lessonContentId,
      'lessonContent': instance.lessonContent,
      'submissions': instance.submissions,
    };

_$_SubmissionResponse _$_$_SubmissionResponseFromJson(
    Map<String, dynamic> json) {
  return _$_SubmissionResponse(
    lessonId: json['lessonId'] as String,
    lesson: json['lesson'] == null
        ? null
        : Lesson.fromJson(json['lesson'] as Map<String, dynamic>),
    lessonContents: (json['lessonContents'] as List)
        ?.map((e) => e == null
            ? null
            : LessonContentSubmission.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_SubmissionResponseToJson(
        _$_SubmissionResponse instance) =>
    <String, dynamic>{
      'lessonId': instance.lessonId,
      'lesson': instance.lesson,
      'lessonContents': instance.lessonContents,
    };

_$_Student _$_$_StudentFromJson(Map<String, dynamic> json) {
  return _$_Student(
    profile: json['profile'] == null
        ? null
        : User.fromJson(json['profile'] as Map<String, dynamic>),
    submissionResponses: (json['submissionResponses'] as List)
        ?.map((e) => e == null
            ? null
            : SubmissionResponse.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_StudentToJson(_$_Student instance) =>
    <String, dynamic>{
      'profile': instance.profile,
      'submissionResponses': instance.submissionResponses,
    };

_$_LoginResponse _$_$_LoginResponseFromJson(Map<String, dynamic> json) {
  return _$_LoginResponse(
    accessToken: json['accessToken'] as String,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_LoginResponseToJson(_$_LoginResponse instance) =>
    <String, dynamic>{
      'accessToken': instance.accessToken,
      'user': instance.user,
    };

_$_UserCourses _$_$_UserCoursesFromJson(Map<String, dynamic> json) {
  return _$_UserCourses(
    id: json['_id'] as String,
    userId: json['userId'] as String,
    courseId: json['courseId'] as String,
    relation: json['relation'] as String,
    course: json['course'] == null
        ? null
        : Course.fromJson(json['course'] as Map<String, dynamic>),
    courseRating: (json['courseRating'] as num)?.toDouble(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
  );
}

Map<String, dynamic> _$_$_UserCoursesToJson(_$_UserCourses instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'userId': instance.userId,
      'courseId': instance.courseId,
      'relation': instance.relation,
      'course': instance.course,
      'courseRating': instance.courseRating,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };

_$_RatingResponse _$_$_RatingResponseFromJson(Map<String, dynamic> json) {
  return _$_RatingResponse(
    value: (json['value'] as num)?.toDouble(),
    totalRates: json['totalRates'] as int,
  );
}

Map<String, dynamic> _$_$_RatingResponseToJson(_$_RatingResponse instance) =>
    <String, dynamic>{
      'value': instance.value,
      'totalRates': instance.totalRates,
    };

_$_ListResponse<T> _$_$_ListResponseFromJson<T>(Map<String, dynamic> json) {
  return _$_ListResponse<T>(
    total: json['total'] as int,
    limit: json['limit'] as int,
    skip: json['skip'] as int,
    data: (json['data'] as List)?.map(DataConverter<T>().fromJson)?.toList(),
  );
}

Map<String, dynamic> _$_$_ListResponseToJson<T>(_$_ListResponse<T> instance) =>
    <String, dynamic>{
      'total': instance.total,
      'limit': instance.limit,
      'skip': instance.skip,
      'data': instance.data?.map(DataConverter<T>().toJson)?.toList(),
    };

_$_BatchResponse<T> _$_$_BatchResponseFromJson<T>(Map<String, dynamic> json) {
  return _$_BatchResponse<T>(
    status: json['status'] as String,
    value: DataConverter<T>().fromJson(json['value']),
  );
}

Map<String, dynamic> _$_$_BatchResponseToJson<T>(
        _$_BatchResponse<T> instance) =>
    <String, dynamic>{
      'status': instance.status,
      'value': DataConverter<T>().toJson(instance.value),
    };

_$_Item _$_$_ItemFromJson(Map<String, dynamic> json) {
  return _$_Item(
    json['name'] as String,
  );
}

Map<String, dynamic> _$_$_ItemToJson(_$_Item instance) => <String, dynamic>{
      'name': instance.name,
    };
