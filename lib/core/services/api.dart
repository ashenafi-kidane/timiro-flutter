import 'package:app/core/enums/common_enums.dart';
import 'package:app/utils/constants.dart';
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:meta/meta.dart';

BaseOptions baseOptions = new BaseOptions(
  baseUrl: kBaseUrl,
  connectTimeout: 5000,
  receiveTimeout: 3000,
  // followRedirects: false,
  // validateStatus: (status) {
  //   return status < 500;
  // },
);

class ApiClient {
  final Dio dio;

  ApiClient({@required this.dio}) {
    dio.options = baseOptions;
  }

  Future setInterceptors() async {
    // final _prefs = await SharedPreferences.getInstance();
    // final accessToken = _prefs.getString('accessToken');
    final accessToken = GetStorage().read('accessToken');
    // print('accessToken: $accessToken');
    if (accessToken != null) {
      dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers["Authorization"] = accessToken;
          return options;
        },
      ));
    }
  }

  Future<Map<String, dynamic>> request(
      {@required RequestType requestType,
      bool requiresAuth = true,
      @required String path,
      Map<String, dynamic> queryParameters,
      Map<String, dynamic> data}) async {
    try {
      if (requiresAuth) await setInterceptors();
      switch (requestType) {
        case RequestType.GET:
          return (await dio.get(path, queryParameters: queryParameters)).data
              as Map<String, dynamic>;
          break;
        case RequestType.POST:
          return (await dio.post(path, data: data)).data
              as Map<String, dynamic>;
          break;
        case RequestType.PATCH:
          return (await dio.patch(path, data: data)).data
              as Map<String, dynamic>;
          break;
        case RequestType.DELETE:
          return (await dio.delete(path)).data as Map<String, dynamic>;
          break;
        case RequestType.PUT:
          return (await dio.delete(path, data: data)).data
              as Map<String, dynamic>;
          break;
        default:
      }
    } on Exception catch (e) {
      print(e.toString());
      return Future.error(e.toString());
    }
  }

  Future batchRequest({Map<String, dynamic> data}) async {
    try {
      await setInterceptors();
      return (await dio.post('/batch', data: data)).data;
    } on Exception catch (e) {
      print(e.toString());
      return Future.error(e.toString());
    }
  }

  getAll() async {
    try {
      // var response = await httpClient.get(baseUrl);
      // if(response.statusCode == 200){
      //   Map<String, dynamic> jsonResponse = json.decode(response.data);
      //     List<MyModel> listMyModel = jsonResponse['data'].map<MyModel>((map) {
      //       return MyModel.fromJson(map);
      //     }).toList();
      //   return listMyModel;
      // }else print ('erro -get');
    } catch (_) {}
  }
}
