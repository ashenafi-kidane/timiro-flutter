import 'package:app/core/models/freezed_models.dart';
import 'dart:io';

import 'package:image_picker/image_picker.dart';

abstract class IMainRepository {
  Future<File> getMedia(ImageSource imageSource, String mediaType);
  Future<File> getCroppedImage(File imageFile);
  Future<User> editUser(Map<String, dynamic> user);

  Future<Course> createCourse(Map<String, dynamic> course);
  Future<ListResponse<CourseCategory>> getCategories();
  Future<ListResponse<Course>> getCourses(String userId);

  Future<ListResponse<UserCourses>> getUserCourses(String userId);
  Future<Lesson> addLesson(Map<String, dynamic> lesson);
  Future<LessonContent> addLessonContent(Map<String, dynamic> lessonContent);

  Future<Course> editCourse(Map<String, dynamic> course);
  Future<Article> addArticle(Map<String, dynamic> article);
  Future<Question> addQuestion(Map<String, dynamic> question);

  Future<Rating> rate(Map<String, dynamic> rating);
  Future<UserCourses> enrollCourse(Map<String, dynamic> courseEnrollPyaload);
  Future<ListResponse<Student>> getStudents(String courseId);

  Future<ListResponse<SubmissionResponse>> getUserCourseSubmissions(
      String courseId, String userId);
  Future<List<BatchResponse<QuestionSubmission>>> sendSubmissions(
      Map<String, dynamic> submissions);
}

abstract class IAuthRepository {
  Future<User> login(String email, String password);
  Future<User> signup(Map<String, dynamic> signupPayload);
  Future<User> getUser(String id);

  Future<User> verifyEmail(String email, String verificationCode);
}
