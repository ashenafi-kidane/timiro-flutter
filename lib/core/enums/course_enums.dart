enum Categories {
  Programming,
  Science,
  Design,
}

enum ContentOptions {
  FILE,
  ARTICLE,
  QUESTION,
}

enum ArticleOptions {
  title,
  paragraph,
  image,
}

enum ImageFor {
  courseProfile,
  article,
}

enum QuestionMode {
  question,
  submission,
}

enum courseRelation {
  INSTRUCTOR,
  ENROLLED,
  BANNED,
}

enum QuestionType {
  TRUE_FALSE,
  MULTIPLE_CHOICE,
  SHORT_ANSWER,
  FILE_SUBMISSION,
}
