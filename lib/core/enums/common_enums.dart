enum Status {
  loading,
  success,
  error,
}

enum RequestType {
  GET,
  POST,
  PATCH,
  PUT,
  DELETE,
}

enum InputType {
  text,
  point,
}
