import 'dart:convert';

import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/enums/common_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/core/repository/auth_repository.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends GetxController {
  final IAuthRepository repository;
  AuthController({@required this.repository}) : assert(repository != null);

  final status = Status.success.obs;
  final _email = ''.obs;
  get email => this._email.value;
  set email(value) => this._email.value = value;

  final _password = ''.obs;
  get password => this._password.value;
  set password(value) => this._password.value = value;

  final _confirmPassword = ''.obs;
  get confirmPassword => this._confirmPassword.value;
  set confirmPassword(value) => this._confirmPassword.value = value;

  final _fullname = ''.obs;
  get fullname => this._fullname.value;
  set fullname(value) => this._fullname.value = value;

  final _passwordVisiblity = false.obs;
  get passwordVisiblity => this._passwordVisiblity.value;
  set passwordVisiblity(value) => this._passwordVisiblity.value = value;

  final _verificationCode = ''.obs;
  get verificationCode => this._verificationCode.value;
  set verificationCode(value) => this._verificationCode.value = value;

  User _user;

  // final box = GetStorage();
  // SharedPreferences _prefs;
  GetStorage _storage = GetStorage();

  @override
  void onInit() async {
    // _prefs = await SharedPreferences.getInstance();
    super.onInit();
  }

  Future<void> login() async {
    status(Status.loading);
    await repository.login(email, password).then(
      (data) {
        // Get.snackbar('Success', 'Login Success!');
        status(Status.success);
        print(data);
        _persistUser(data);
        _user = data;
        _navigateUser();
      },
      onError: (err) {
        print("$err");
        status(Status.error);
      },
    );
  }

  Future<void> signup() async {
    status(Status.loading);
    final splitedName = fullname.trim().split(' ');
    final signupPayload = {
      "email": email,
      "password": password,
      "firstName": splitedName.length > 0 ? splitedName[0] : '',
      "lastName": splitedName.length > 1 ? splitedName[1] : '',
    };
    await repository.signup(signupPayload).then(
      (data) {
        // Get.snackbar('Success', 'Signup Success!');
        status(Status.success);
        _persistUser(data);
        _user = data;
        Get.toNamed(Routes.VERIFY);
      },
      onError: (err) {
        print("$err");
        status(Status.error);
      },
    );
  }

  Future<User> getUser(String id) async {
    try {
      return await repository.getUser(id);
    } on Exception catch (e) {
      return Future.error(e.toString());
    }
  }

  Future<void> verifyEmail() async {
    //show main page
    Get.toNamed(Routes.MAIN);
    //Todo: uncomment the following
    // status(Status.loading);
    // await repository.verifyEmail(_user.email, verificationCode).then(
    //   (data) {
    //     print(data);
    //     status(Status.success);
    //     _user = data;
    //     //Todo: show next route
    //   },
    //   onError: (err) {
    //     print("$err");
    //     status(Status.error);
    //   },
    // );
  }

  void determineNextRoute() async {
    if (_user == null) {
      //Get current user if the user already loged in and route accordingly
      //else show welcome screen
      // _prefs = await SharedPreferences.getInstance();
      // final userString = _prefs.getString('user');
      final userString = _storage.read('user');
      if (userString != null) {
        print(userString);
        _user = User.fromJson(jsonDecode(userString));
        Future.delayed(Duration.zero, () {
          Get.toNamed(Routes.MAIN);
        });
        await getUser(_user.id).then(
          (user) {
            this._user = user;
            _persistUser(user);
          },
        );
      } else
        Future.delayed(Duration.zero, () {
          Get.toNamed(Routes.WELCOME);
        });
    } else
      _navigateUser();
  }

  void _navigateUser() {
    // route to email verivication if user is not verified
    if (!_user.isVerified)
      Get.toNamed(Routes.VERIFY);
    else
      Get.offAllNamed(Routes.MAIN);
  }

  void _persistUser(User user) {
    // _prefs.setString('user', json.encode(user));
    _storage.write('user', json.encode(user));
    if (user.accessToken != null)
      _storage.write('accessToken', user.accessToken);
    // _prefs.setString('accessToken', user.accessToken);
  }

  void logout() {
    // _prefs.clear();
    _storage.erase();
    Get.offAllNamed(Routes.LOGIN);
  }
}
