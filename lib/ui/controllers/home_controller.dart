import 'dart:convert';

import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController
    with StateMixin<ListResponse<Course>> {
  final IMainRepository repository;
  HomeController({@required this.repository}) : assert(repository != null);

  CoursesController coursesController;
  // final status = Status.success.obs;
  final _user = User().obs..nil();
  get user => this._user.value;
  set user(value) => this._user.value = value;

  final _selectedCategory = 0.obs;
  get selectedCategory => this._selectedCategory.value;
  set selectedCategory(value) => this._selectedCategory.value = value;

  final _courses = List<Course>().obs;
  get courses => this._courses;
  set courses(value) => this._courses.assignAll(value);

  final _categories = List<CourseCategory>().obs;
  get categories => this._categories;
  set categories(value) => this._categories.assignAll(value);

  GetStorage _storage = GetStorage();

  @override
  void onInit() async {
    getUser();
    if (user != null) coursesController = Get.find();
    getCategories();
    getCourses(user != null ? user.id : '');
    super.onInit();
  }

  Future getCategories() async {
    // status(Status.loading);
    await repository.getCategories().then(
      (data) {
        // status(Status.success);
        categories = data.data;
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  Future getCourses(String userId) async {
    repository.getCourses(userId).then((data) {
      change(data, status: RxStatus.success());
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  void getUser() {
    final userString = _storage.read('user');
    if (userString != null) user = User.fromJson(jsonDecode(userString));
  }

  void joinCourse(String courseId) async {
    if (user != null) {
      final enrollCoursePayload = {
        'courseId': courseId,
      };

      await repository.enrollCourse(enrollCoursePayload).then(
        (userCourse) {
          coursesController.onCourseJoined(userCourse, 'enroll');
        },
        onError: (err) {
          print("$err");
          // status(Status.error);
        },
      );
    } else
      Get.snackbar('join_course'.tr, 'error_unauthorized'.tr);
  }
}
