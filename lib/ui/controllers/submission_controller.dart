import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class SubmissionController extends GetxController
    with StateMixin<List<Student>> {
  final IMainRepository repository;
  SubmissionController({@required this.repository})
      : assert(repository != null);

  CoursesController coursesController;

  final _selectedCourse = Course().obs..nil();
  get selectedCourse => this._selectedCourse.value;
  set selectedCourse(value) => this._selectedCourse.value = value;

  final _students = List<Student>().obs;
  get students => this._students;
  set students(value) => this._students.assignAll(value);

  final _selectedStudent = Student().obs..nil();
  get selectedStudent => this._selectedStudent.value;
  set selectedStudent(value) => this._selectedStudent.value = value;

  final _selectedSubmissionResponse = SubmissionResponse().obs..nil();
  get selectedSubmissionResponse => this._selectedSubmissionResponse.value;
  set selectedSubmissionResponse(value) =>
      this._selectedSubmissionResponse.value = value;

  final _selectedSubmissionContent = LessonContentSubmission().obs..nil();
  get selectedSubmissionContent => this._selectedSubmissionContent.value;
  set selectedSubmissionContent(value) =>
      this._selectedSubmissionContent.value = value;

  AnimationController animationController;

  @override
  void onInit() async {
    coursesController = Get.find();
    selectedCourse = coursesController.selectedCourse.value;
    animationController = coursesController.animationController;
    getStudents();
    super.onInit();
  }

  void getStudents() async {
    if (selectedCourse != null) {
      repository.getStudents(selectedCourse.id).then((data) {
        students = (data.data);
        attachLessonOnSubmissonResponse();
        change(students, status: RxStatus.success());
      }, onError: (err) {
        change(null, status: RxStatus.error(err.toString()));
      });
    }
  }

  void updatePoint(String inputPoint, int submissionIndex) {
    final point = double.tryParse(inputPoint);
    if (point != null) {
      selectedSubmissionResponse.update((value) {
        value.submissions[submissionIndex] =
            value.submissions[submissionIndex].copyWith(resultPoint: point);
      });
    }
  }

  void submitEvaluation() {
    //Todo: submit evaluation
  }

  void attachLessonOnSubmissonResponse() {
    for (Student student in students) {
      for (int i = 0; i < student.submissionResponses.length; i++) {
        SubmissionResponse submissionResponse = student.submissionResponses[i];
        final iterableLesson = selectedCourse.lessons
            .where((lesson) => lesson.id == submissionResponse.lessonId);
        final lesson = iterableLesson.first.toJson();
        final lessonContents = lesson.remove('lessonContents');
        student.submissionResponses[i] =
            submissionResponse.copyWith(lesson: Lesson.fromJson(lesson));
        _attachContentOnSubmission(
            lessonContents, submissionResponse.lessonContents);
      }
    }
  }

  List<LessonContentSubmission> _attachContentOnSubmission(
      List<LessonContent> lessonContents,
      List<LessonContentSubmission> submissionsContents) {
    for (int i = 0; i < submissionsContents.length; i++) {
      LessonContentSubmission submissionsContent = submissionsContents[i];
      final iterableContent = lessonContents
          .where((content) => content.id == submissionsContent.lessonContentId);
      final lessonContent = iterableContent.first.toJson();
      lessonContent.remove('questions');
      submissionsContents[i] = submissionsContent.copyWith(
          lessonContent: LessonContent.fromJson(lessonContent));
    }
    return submissionsContents;
  }
}
