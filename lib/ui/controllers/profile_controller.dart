import 'dart:convert';
import 'dart:io';

import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/core/repository/main_repository.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:app/core/enums/common_enums.dart';

class ProfileController extends GetxController {
  final IMainRepository repository;
  ProfileController({@required this.repository}) : assert(repository != null);

  final status = Status.success.obs;

  final Rx<User> _user = User().obs..nil();
  get user => this._user.value;
  set user(value) => this._user.value = value;

  //for showing other users profile

  final Rx<User> _profile = User().obs..nil();
  get profile => this._profile.value;
  set profile(value) => this._profile.value = value;

  //for editing profile

  final _phone = ''.obs;
  get phone =>
      this._phone.value != null ? this._phone.value.trim() : this._phone.value;
  set phone(value) => this._phone.value = value;

  final _address = ''.obs;
  get address => this._address.value != null
      ? this._address.value.trim()
      : this._address.value;
  set address(value) => this._address.value = value;

  final _pickedFilePath = ''.obs;
  get pickedFilePath => this._pickedFilePath.value;
  set pickedFilePath(value) => this._pickedFilePath.value = value;

  final _isProfileEdited = false.obs;
  get isProfileEdited => this._isProfileEdited.value;
  set isProfileEdited(value) => this._isProfileEdited.value = value;

  File image;
  GetStorage _storage = GetStorage();

  @override
  void onInit() async {
    getUser();
    super.onInit();
  }

  void getUser() {
    final userString = _storage.read('user');
    user = User.fromJson(jsonDecode(userString));
  }

  void setProfileValues(User studentProfile) {
    if (studentProfile != null)
      profile = studentProfile;
    else
      profile = user;

    //set values
    address = profile.address;
    phone = profile.phone;
  }

  void getImage(ImageSource imageSource) async {
    try {
      final pickedFile = await repository.getMedia(imageSource, 'image');
      if (pickedFile != null) {
        // pickedFilePath = pickedFile.path;
        // image = File(pickedFilePath);
        getCroppedImage(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void getCroppedImage(File imageFile) async {
    try {
      final croppedImage = await repository.getCroppedImage(imageFile);
      if (croppedImage != null) {
        pickedFilePath = croppedImage.path;
        image = File(pickedFilePath);
        checkProfileChanged();
      } else {
        print('crop error.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  final _count = 0.obs;
  get count => this._count.value;
  set count(value) => this._count.value = value;

  void editProfile() async {
    //Todo: edit profile logic
    if (pickedFilePath.isNotEmpty) {
      //Todo: upload profile image first
    }
    final userPayload = {
      'id': user.id,
    };

    if (address != null) userPayload['address'] = address;
    if (phone != null) userPayload['phone'] = phone;

    status(Status.loading);

    await repository.editUser(userPayload).then(
      (data) {
        status(Status.success);

        //update user
        user = data;
        profile = user;
        _persistUser(user);

        //clear picked file
        pickedFilePath = '';

        checkProfileChanged();
      },
      onError: (err) {
        print("$err");
        status(Status.error);
      },
    );
  }

  void checkProfileChanged() {
    if (pickedFilePath.isNotEmpty)
      isProfileEdited = true;
    else if (phone != profile.phone ||
        (phone.isNotEmpty && profile.phone == null))
      isProfileEdited = true;
    else if (address != profile.address ||
        (address.isNotEmpty && profile.address == null))
      isProfileEdited = true;
    else
      isProfileEdited = false;
  }

  void _persistUser(User user) {
    // _prefs.setString('user', json.encode(user));
    _storage.write('user', json.encode(user));
    // _prefs.setString('accessToken', user.accessToken);
  }
}
