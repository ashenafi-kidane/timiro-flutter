import 'dart:convert';

import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/enums/course_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:app/ui/pages/common/confirm_dialog.dart';
import 'package:app/ui/pages/common/success_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'dart:io';

class CoursesController extends GetxController with StateMixin<List<Course>> {
  final IMainRepository repository;
  CoursesController({@required this.repository}) : assert(repository != null);

  // final status = Status.success.obs;
  Rx<Course> selectedCourse = Course().obs..nil();
  Rx<Lesson> selectedlesson = Lesson().obs..nil();
  Rx<LessonContent> selectedLessonContent = LessonContent().obs..nil();

  final _categories = List<CourseCategory>().obs;
  get categories => this._categories;
  set categories(value) => this._categories.assignAll(value);

  final _user = User().obs..nil();
  get user => this._user.value;
  set user(value) => this._user.value = value;

  final _isInstructor = false.obs;
  get isInstructor => this._isInstructor.value;
  set isInstructor(value) => this._isInstructor.value = value;

  // For creating/editing course
  final _title = ''.obs;
  get title => this._title.value.trim();
  set title(value) => this._title.value = value;

  final _description = ''.obs;
  get description => this._description.value.trim();
  set description(value) => this._description.value = value;

  final _category = CourseCategory().obs..nil();
  get category => this._category.value;
  set category(value) => this._category.value = value;

  final _price = ''.obs;
  get price => this._price.value.trim();
  set price(value) => this._price.value = value;

  final _instructors = [].obs;
  get instructors => this._instructors;

  final _isCourseEdited = false.obs;
  get isCourseEdited => this._isCourseEdited.value;
  set isCourseEdited(value) => this._isCourseEdited.value = value;

  var pickedVideoPath = ''.obs..nil();

  final _courseImagePath = ''.obs;
  get courseImagePath => this._courseImagePath.value;
  set courseImagePath(value) => this._courseImagePath.value = value;

  List<UserCourses> _userCourses;

  final _courses = List<Course>().obs;
  get courses => this._courses;
  set courses(value) => this._courses.assignAll(value);

  final _userCourseSubmissions = List<SubmissionResponse>().obs;
  get userCourseSubmissions => this._userCourseSubmissions;
  set userCourseSubmissions(value) =>
      this._userCourseSubmissions.assignAll(value);

  final _selectedSubmissionContent = LessonContentSubmission().obs..nil();
  get selectedSubmissionContent => this._selectedSubmissionContent.value;
  set selectedSubmissionContent(value) =>
      this._selectedSubmissionContent.value = value;

  List<String> answers;

  GetStorage _storage = GetStorage();
  int selectedCourseIndex = 0;
  int selectedLessonIndex = 0;
  int selectedContentIndex = 0;

  //your rating for the current course
  double userCourseRating;

  AnimationController animationController;
  Animation<double> animation;
  RxDouble opacity1 = 0.0.obs;
  RxDouble opacity2 = 0.0.obs;
  RxDouble opacity3 = 0.0.obs;
  RxDouble opacity4 = 0.0.obs;

  @override
  void onInit() async {
    getUser();
    //Todo: Get courses will be callaed using the current user
    getCategories();
    getUserCourses();
    super.onInit();
  }

  void initAnimation() {
    animationController = AnimationController(
        vsync: NavigatorState(), duration: Duration(milliseconds: 1000));

    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    _setData();
  }

  Future<void> _setData() async {
    opacity1.value = 0.0;
    opacity2.value = 0.0;
    opacity3.value = 0.0;
    opacity4.value = 0.0;

    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    opacity1.value = 1.0;
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    opacity2.value = 1.0;
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    opacity3.value = 1.0;
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    opacity4.value = 1.0;
  }

  void getUser() {
    final userString = _storage.read('user');
    user = User.fromJson(jsonDecode(userString));
  }

  Future getUserCourses() async {
    repository.getUserCourses(user.id).then((data) {
      _userCourses = (data.data);
      courses = _userCourses.map((m) => m.course).toList();
      change(courses, status: RxStatus.success());
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  Future getCategories() async {
    await repository.getCategories().then(
      (data) {
        categories = data.data;
      },
      onError: (err) {
        print("$err");
      },
    );
  }

  void onCourseSelected(Course course, int index) {
    selectedCourse.value = course;
    selectedCourseIndex = index;
    //show lessons page.
    Get.toNamed(Routes.COURSELESSON);

    // set course values
    title = selectedCourse.value.title;
    description = selectedCourse.value.description;
    category = selectedCourse.value.category;
    price = selectedCourse.value.price.toString();

    //check and set if the user is an instructor
    isInstructor =
        _userCourses[index].relation == describeEnum(courseRelation.INSTRUCTOR);

    // set current user's rating for this course
    userCourseRating = _userCourses[index].courseRating;
    //clear values
    courseImagePath = '';

    //get courseSubmissions for the course (if not instructor)
    if (!isInstructor) getUserCourseSubmissions();
  }

  void createCourse() async {
    final coursePayload = {
      'title': title,
      'description': description,
      'categoryId': category.id,
    };

    // status(Status.loading);
    await repository.createCourse(coursePayload).then(
      (newCourse) {
        // status(Status.success);

        final course = newCourse.copyWith(
          courseImageId: 'assets/images/interFace4.png',
        );

        //clear course values
        title = '';
        description = '';
        category = null;
        price = '';

        onCourseJoined(
            UserCourses(
              userId: user.id,
              courseId: course.id,
              relation: describeEnum(courseRelation.INSTRUCTOR),
              course: course,
            ),
            'new');
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void addLesson(title, description) async {
    final lessonPayload = {
      'courseId': selectedCourse.value.id,
      'title': title.trim(),
      'description': description.trim(),
    };

    await repository.addLesson(lessonPayload).then(
      (lesson) {
        // status(Status.success);
        selectedCourse.update((value) {
          value.lessons.add(lesson);
        });
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void addLessonContent(
    String title,
    String description,
    ContentOptions type,
    String videoUrl,
  ) async {
    //Todo: upload video using video picked path for the case of video
    //then remember to clear the states(pickedvideopath) after successful upload.
    // for videoUrl case send file with the url
    final lessonContentPayload = {
      'courseId': selectedCourse.value.id,
      'lessonId': selectedlesson.value.id,
      'title': title.trim(),
      'type': describeEnum(type),
    };

    if (description.trim().isNotEmpty)
      lessonContentPayload['description'] = description.trim();

    await repository.addLessonContent(lessonContentPayload).then(
      (lessonContent) {
        selectedlesson.update((value) {
          value.lessonContents.add(lessonContent);
        });
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void onSelectContent(LessonContent content, int index) {
    selectedLessonContent.value = content;
    selectedContentIndex = index;
    switch (content.type) {
      case 'FILE':
        Get.toNamed(Routes.COURSEVIDEO);
        break;
      case 'ARTICLE':
        Get.toNamed(Routes.ARTICLE);
        break;
      case 'QUESTION':
        if (!isInstructor) {
          if (_isCourseContentSubmitted()) {
            Get.toNamed(Routes.RESULTS);
          } else {
            answers = List(content.questions.length);
            Get.toNamed(Routes.QUESTIONS);
          }
        } else
          Get.toNamed(Routes.QUESTIONS);
        break;
      default:
    }
  }

  void addArticle({
    String title,
    String paragraph,
    String filePath,
  }) async {
    //Todo: upload file first if fileId is not null
    // also suport for multiple file
    String fileId;
    if (filePath != null) {
      //Upoad file first , then set file Id
    }

    final Map<String, dynamic> articlePayload = {
      'courseId': selectedCourse.value.id,
      'lessonContentId': selectedLessonContent.value.id,
    };

    if (title != null) articlePayload['title'] = title.trim();
    if (paragraph != null) articlePayload['paragraph'] = paragraph.trim();
    if (fileId != null) articlePayload['fileIds'] = [fileId];

    await repository.addArticle(articlePayload).then(
      (article) {
        // status(Status.success);
        //Update selectedLessonContent
        if (selectedLessonContent.value.articles == null) {
          selectedLessonContent.value = selectedLessonContent.value.copyWith(
            articles: [article],
          );
        } else
          selectedLessonContent.update((value) {
            value.articles.add(article);
          });

        //update LessonContents List
        // selectedlesson.update((value) {
        //   value.lessonContents[selectedContentIndex] =
        //       selectedLessonContent.value;
        // });
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void addQuestion(
    String textQuestion,
    QuestionType questionType,
    List<String> choices,
    String answer,
    double point,
  ) async {
    //Todo: upload file first filequestions

    final Map<String, dynamic> questionPayload = {
      'courseId': selectedCourse.value.id,
      'lessonContentId': selectedLessonContent.value.id,
      'textQuestion': textQuestion.trim(),
      'questionType': describeEnum(questionType == QuestionType.TRUE_FALSE
          ? QuestionType.MULTIPLE_CHOICE
          : questionType),
      'point': point,
      'textAnswer': answer.trim(),
    };

    if (choices.length > 1) questionPayload['multipleChoices'] = choices;

    await repository.addQuestion(questionPayload).then(
      (question) {
        // status(Status.success);
        //Update selectedLessonContent
        selectedLessonContent.update((value) {
          value.questions.add(question);
        });
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void getVideo(ImageSource imageSource) async {
    try {
      final pickedFile = await repository.getMedia(imageSource, 'video');
      if (pickedFile != null) {
        pickedVideoPath.value = pickedFile.path;
      } else {
        print('No video selected.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void getImage(ImageSource imageSource, ImageFor imageFor) async {
    try {
      final pickedFile = await repository.getMedia(imageSource, 'image');
      if (pickedFile != null) {
        // pickedFilePath = pickedFile.path;
        // image = File(pickedFilePath);
        getCroppedImage(File(pickedFile.path), imageFor);
      } else {
        print('No image selected.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void getCroppedImage(File imageFile, ImageFor imageFor) async {
    try {
      final croppedImage = await repository.getCroppedImage(imageFile);
      if (croppedImage != null) {
        switch (imageFor) {
          case ImageFor.courseProfile:
            //Todo: upload and set course image
            courseImagePath = croppedImage.path;
            checkCourseEdited();
            break;
          case ImageFor.article:
            //Todo: upload and set course image
            addArticle(filePath: croppedImage.path);
            break;
          default:
        }
      } else {
        print('crop error.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void setAnswer(String answer, int index) {
    answers[index] = answer;
    print(answers);
  }

  void submitAnswers() async {
    //check if all answers are answered and submit it.
    if (answers.contains(null)) {
      Get.snackbar('submit_answer'.tr, 'error_submit_answer'.tr);
    } else {
      // show confirm dialog
      Get.dialog(ConfirmDialog(
        title: 'are_you_sure',
        content: 'submission_info',
        actionText: 'submit',
        actionCallback: () async {
          //submit answers for the current user and questions
          //upload file first if question type is file submission,
          // and then add the fileAnswerIds.

          // construct list call requests for a single batch request
          final calls = [];
          for (int i = 0;
              i < selectedLessonContent.value.questions.length;
              i++) {
            calls.add([
              'create',
              'question-submissions',
              {
                'userId': user.id,
                'courseId': selectedCourse.value.id,
                'lessonId': selectedlesson.value.id,
                'lessonContentId': selectedLessonContent.value.id,
                'questionId': selectedLessonContent.value.questions[i].id,
                'textAnswer': answers[i],
              }
            ]);
          }

          // get current user's submissions
          await repository.sendSubmissions({'calls': calls}).then(
            (batchResponse) {
              //add the new submissions
              _updateSubmissions(batchResponse);
              Get.back();
              Get.dialog(SuccessDialog(
                description: 'submision_success',
              ));
            },
            onError: (err) {
              print("$err");
            },
          );
        },
      ));
    }
  }

  void editCourse() async {
    //check if all inputs are valid
    if (_isCourseInputValid()) {
      //upload file first if profile is edited.
      if (courseImagePath.isNotEmpty) {
        //Todo: upload profile image first
      }

      //remeber to trim values, check double.tryParse(price),  clear values after success,
      final coursePayload = {
        'id': selectedCourse.value.id,
        'title': title,
        'description': description,
        'categoryId': category.id,
        'price': double.tryParse(price) ?? selectedCourse.value.price,
      };

      await repository.editCourse(coursePayload).then(
        (data) {
          // status(Status.success);

          //update fields after saving (so that is courseEdited will be false)
          //update the current selected course with the new one
          selectedCourse.value = data;

          //update the course from the list
          courses[selectedCourseIndex] = data;
          change(courses, status: RxStatus.success());

          //clear values
          courseImagePath = '';
          checkCourseEdited();
        },
        onError: (err) {
          print("$err");
          // status(Status.error);
        },
      );
    }
  }

  void checkCourseEdited() {
    if (courseImagePath.isNotEmpty)
      isCourseEdited = true;
    else if (title != selectedCourse.value.title)
      isCourseEdited = true;
    else if (description != selectedCourse.value.description)
      isCourseEdited = true;
    else if (category != selectedCourse.value.category)
      isCourseEdited = true;
    else if (double.tryParse(price) != selectedCourse.value.price)
      isCourseEdited = true;
    else
      isCourseEdited = false;
  }

  bool _isCourseInputValid() {
    String errorText;
    if (title.isEmpty)
      errorText = 'error_course_title';
    else if (description.isEmpty)
      errorText = 'error_course_description';
    else if (price.isEmpty ||
        (double.tryParse(price) != null && double.tryParse(price) < 0))
      errorText = 'error_course_price';

    if (errorText != null) {
      Get.snackbar('invalid_input'.tr, errorText.tr);
      return false;
    } else
      return true;
  }

  void rateCourse(double rating) async {
    final ratePayload = {
      'value': rating,
      'ratedBy': user.id,
      'ratingFor': selectedCourse.value.id,
    };

    await repository.rate(ratePayload).then(
      (rating) {
        userCourseRating = rating.value;
        Get.snackbar('rating_success'.tr, 'course_rating_succes'.tr);
      },
      onError: (err) {
        print("$err");
        // status(Status.error);
      },
    );
  }

  void onCourseJoined(UserCourses userCourse, String action) {
    courses.add(userCourse.course);

    //add user courses too
    _userCourses.add(userCourse);
    change(courses, status: RxStatus.success());
    Get.back();

    onCourseSelected(userCourse.course, courses.length - 1);
    Get.dialog(SuccessDialog(
      description:
          action == 'new' ? 'course_success_info' : 'course_enroll_success',
    ));
  }

  void getUserCourseSubmissions() async {
    final courseId = selectedCourse.value.id;
    final userId = user.id;

    // get current user's submissions
    await repository.getUserCourseSubmissions(courseId, userId).then(
      (submissionResponse) {
        userCourseSubmissions = submissionResponse.data;
      },
      onError: (err) {
        print("$err");
      },
    );
  }

  // checks if the user submitted for the selected question Course Content
  bool _isCourseContentSubmitted() {
    bool isSubmitted = false;
    final submissionLessons = userCourseSubmissions
        .where((response) => response.lessonId == selectedlesson.value.id);

    if (submissionLessons.length > 0) {
      final submissionContents = submissionLessons.first.lessonContents.where(
          (content) =>
              content.lessonContentId == selectedLessonContent.value.id);
      isSubmitted = submissionContents.length > 0;
      if (isSubmitted) selectedSubmissionContent = submissionContents.first;
    }
    return isSubmitted;
  }

  void _updateSubmissions(
      List<BatchResponse<QuestionSubmission>> newSubmissions) {
    LessonContentSubmission contentSubmission = LessonContentSubmission(
      lessonContentId: selectedLessonContent.value.id,
      submissions: newSubmissions.map((m) => m.value).toList(),
    );

    // check if there is already a submission in the lesson
    if (userCourseSubmissions
            .where((response) => response.lessonId == selectedlesson.value.id)
            .length >
        0) {
      for (int i = 0; i < userCourseSubmissions.length; i++) {
        if (userCourseSubmissions[i].lessonId == selectedlesson.value.id) {
          userCourseSubmissions[i].lessonContents.add(contentSubmission);
          break;
        }
      }
    } else {
      // create new submission for the lesson
      userCourseSubmissions.add(SubmissionResponse(
        lessonId: selectedlesson.value.id,
        lessonContents: [contentSubmission],
      ));
    }
  }
}
