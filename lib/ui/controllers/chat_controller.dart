import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/core/repository/main_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'dart:io';

class ChatController extends GetxController {
  final IMainRepository repository;
  ChatController({@required this.repository}) : assert(repository != null);

  final messageTextController = TextEditingController();
  String messageText;

  final _messages = [
    Message(text: 'Hello there', sender: 'ash@test.com'),
    Message(text: 'How are you', sender: 'ash@test.com', fileId: 'sda'),
    Message(text: 'hi, I am good', sender: 'ash1@test.com'),
    Message(sender: 'ash1@test.com', fileId: 'dasd'),
    Message(text: 'doing good here.', sender: 'ash@test.com'),
    Message(
        text: 'I was wondering if you are interested in some idea',
        sender: 'ash@test.com'),
    Message(
        text: 'Are you available tomorrow morning?', sender: 'ash@test.com'),
    Message(text: 'oh raelly!', sender: 'ash1@test.com', fileId: 'fdadsf'),
    Message(text: 'let me check my schedule ..', sender: 'ash1@test.com'),
    Message(text: 'It is packed tomorrow', sender: 'ash1@test.com'),
    Message(text: 'lets do it the other day ..', sender: 'ash1@test.com'),
  ].obs;
  get messages => this._messages.value;
  set messages(value) => this._messages.value = value;

  final _imagePath = ''.obs;
  get imagePath => this._imagePath.value;
  set imagePath(value) => this._imagePath.value = value;

  final currentUser = 'ash@test.com';

  @override
  void onInit() async {
    //get Chats

    super.onInit();
  }

  void getImage(ImageSource imageSource) async {
    try {
      final pickedFile = await repository.getMedia(imageSource, 'image');
      if (pickedFile != null) {
        // pickedFilePath = pickedFile.path;
        // image = File(pickedFilePath);
        getCroppedImage(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void getCroppedImage(File imageFile) async {
    try {
      final croppedImage = await repository.getCroppedImage(imageFile);
      if (croppedImage != null) {
        imagePath = croppedImage.path;
        _uploadPhoto();
      } else {
        print('crop error.');
      }
    } on Exception catch (e) {
      print(Future.error(e.toString()));
    }
  }

  void _uploadPhoto() {
    //upload photo
    sendMessage();
  }

  void sendMessage() {
    //Todo: add message logic
    Message message;
    if (imagePath.isNotEmpty) {
      message = Message(
        fileId: imagePath,
        sender: currentUser,
      );
    } else if (messageTextController.text.isNotEmpty) {
      message = Message(
        text: messageTextController.text,
        sender: currentUser,
      );
    }

    _messages.add(message);
    messageTextController.clear();
    imagePath = '';
  }
}
