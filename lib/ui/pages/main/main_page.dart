import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/course/courses_page.dart';
import 'package:app/ui/pages/home/home_page.dart';
import 'package:app/ui/pages/profile/navigation_home_screen.dart';
import 'package:app/ui/pages/profile/profile_page.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  AnimationController animationController;
  int selectedPage = 0;
  List<Widget> _pageOptions;

  Widget tabBody = Container(
    color: AppTheme.background,
  );

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 400), vsync: this);

    tabBody = HomePage(animationController: animationController);

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        bottomNavigationBar: ConvexAppBar(
          items: [
            TabItem(icon: Icons.home, title: 'home'.tr),
            TabItem(icon: Icons.map, title: 'courses'.tr),
            // TabItem(icon: Icons.add, title: 'add'.tr),
            // TabItem(icon: Icons.settings, title: 'setting'.tr),
            TabItem(icon: Icons.people, title: 'profile'.tr),
          ],
          backgroundColor: Colors.white,
          activeColor: Colors.lightBlueAccent,
          color: Colors.lightBlueAccent,
          initialActiveIndex: 0, //optional, default as 0
          onTap: (int i) {
            // setState(() {
            //   selectedPage = i;
            // });

            if (selectedPage != i) {
              animationController.reverse().then<dynamic>((data) {
                if (!mounted) {
                  return;
                }
                setState(() {
                  switch (i) {
                    case 0:
                      tabBody =
                          HomePage(animationController: animationController);
                      break;
                    case 1:
                      tabBody =
                          CoursesPage(animationController: animationController);
                      break;
                    case 2:
                      tabBody = NavigationHomeScreen(
                          animationController: animationController);
                      break;
                    default:
                  }
                });
              });
              selectedPage = i;
            }
          },
        ),
        body: tabBody,
      ),
    );
  }
}
