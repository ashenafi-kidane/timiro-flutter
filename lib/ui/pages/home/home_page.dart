import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/home_controller.dart';
import 'package:app/ui/pages/course/components/category_list.dart';
import 'package:app/ui/pages/course/components/course_list.dart';
import 'package:app/ui/pages/course/course_info_page.dart';
import 'package:app/ui/pages/home/components/home_slider.dart';
import 'package:app/ui/widgets/home_animation.dart';
import 'package:app/ui/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  HomePage({Key key, this.animationController}) : super(key: key);

  final AnimationController animationController;

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () async {
      controller.getUser();
      animationController.forward();
    });
    return Container(
      color: AppTheme.nearlyWhite,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        floatingActionButton: Obx(() => controller.user == null
            ? FloatingActionButton(
                child: PopupMenuButton(
                  icon: Icon(Icons.more_vert),
                  elevation: 20,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                  offset: Offset(0, -130),
                  onSelected: (value) {
                    if (value == 0)
                      Get.toNamed(Routes.LOGIN);
                    else if (value == 1) Get.toNamed(Routes.SIGNUP);
                  },
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      height: 40.0,
                      child: Row(
                        children: [
                          Icon(Icons.login, color: AppTheme.darkText),
                          SizedBox(width: 16.0),
                          Text(
                            'login'.tr,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              letterSpacing: 1.2,
                              fontFamily: 'WorkSans',
                            ),
                          ),
                        ],
                      ),
                      value: 0,
                    ),
                    PopupMenuItem(
                      height: 40.0,
                      child: Row(
                        children: [
                          Icon(Icons.lock, color: AppTheme.dark_grey),
                          SizedBox(width: 16.0),
                          Text(
                            'signup'.tr,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              letterSpacing: 1.2,
                              fontFamily: 'WorkSans',
                            ),
                          ),
                        ],
                      ),
                      value: 1,
                    ),
                  ],
                ),
                onPressed: () {},
              )
            : SizedBox()),
        body: controller.obx(
          (state) => Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        HomeAnimation(
                          animationController: animationController,
                          viewCount: 2,
                          index: 0,
                          view: HomeSliderWithIndicator(),
                        ),
                        Expanded(
                          child: HomeAnimation(
                            animationController: animationController,
                            viewCount: 2,
                            index: 1,
                            view: Container(
                              decoration: BoxDecoration(
                                color: AppTheme.nearlyWhite,
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(32.0),
                                    topRight: Radius.circular(32.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: AppTheme.grey.withOpacity(0.5),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 10.0),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SearchBar(),
                                  getCategoryUI(),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 8.0, left: 18, right: 16),
                                      child: CourseList(
                                        courses: state.data,
                                        itemTapCallBack: (course, index) {
                                          Get.to(CourseInfoPage(
                                            course: course,
                                            onJoinCourseCallback: () =>
                                                controller
                                                    .joinCourse(course.id),
                                          ));
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getCategoryUI() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
          child: Text(
            'category'.tr,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: AppTheme.darkerText,
            ),
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: CategoriesList(
            categories: controller.categories,
            selectedIndex: controller.selectedCategory,
            onTapCallback: (selectedIndex) {
              controller.selectedCategory = selectedIndex;
            },
          ),
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }

  // Widget getPopularCourseUI() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Text(
  //           'Popular Course',
  //           textAlign: TextAlign.left,
  //           style: TextStyle(
  //             fontWeight: FontWeight.w600,
  //             fontSize: 22,
  //             letterSpacing: 0.27,
  //             color: AppTheme.darkerText,
  //           ),
  //         ),
  //         Flexible(
  //           child: CourseList(
  //             courses: controller.courses,
  //             callBack: (course) {
  //               Get.to(CourseInfoPage(course: course));
  //             },
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }
}
