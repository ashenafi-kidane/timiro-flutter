import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AspectRatio(
            aspectRatio: 1.2,
            child: Image.asset(
              'assets/images/about_us.jpeg',
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 24.0,
          ),
          Text(
            'about_us_info'.tr,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              letterSpacing: 1.2,
              fontWeight: FontWeight.w400,
              fontFamily: 'Pacifico',
            ),
          ),
        ],
      ),
    );
  }
}
