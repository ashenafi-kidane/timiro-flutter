import 'package:app/shared/theme/app_theme.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmDialog extends StatelessWidget {
  ConfirmDialog({
    @required this.title,
    @required this.content,
    this.actionText = 'done',
    this.actionCallback,
  });

  final String title;
  final String content;
  final String actionText;
  final VoidCallback actionCallback;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        title.tr,
        textAlign: TextAlign.center,
      ),
      content: Text(
        content.tr,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18.0,
          letterSpacing: 1.2,
        ),
      ),
      actions: [
        TextButton(
          child: Text(
            'cancel'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
            actionText.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            actionCallback();
          },
        ),
      ],
    );
  }
}
