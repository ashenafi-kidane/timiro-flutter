import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LanguageDialog extends StatefulWidget {
  @override
  _LanguageDialogState createState() => _LanguageDialogState();
}

class _LanguageDialogState extends State<LanguageDialog> {
  final List<String> languages = ['English', 'Amharic'];
  String selectedLanguage = 'English';

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'change_language'.tr,
        textAlign: TextAlign.center,
      ),
      content: DropdownButtonFormField<String>(
        // isExpanded: true,
        decoration: InputDecoration(
          hintText: 'select_language'.tr,
          fillColor: Colors.white,
          alignLabelWithHint: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
        ),
        items: languages.map((String category) {
          return DropdownMenuItem<String>(
            value: category,
            child: new Text(category),
          );
        }).toList(),
        value: selectedLanguage,
        onChanged: (value) {
          setState(() {
            selectedLanguage = value;
          });
        },
      ),
      actions: [
        TextButton(
          child: Text(
            'cancel'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
            'done'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            //Todo: change language here.
            var locale;
            switch (selectedLanguage) {
              case 'English':
                locale = Locale('en', 'US');
                break;
              case 'Amharic':
                locale = Locale('am');
                break;
              default:
            }
            Get.updateLocale(locale);
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
