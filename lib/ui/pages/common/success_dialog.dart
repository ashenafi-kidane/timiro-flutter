import 'package:app/ui/widgets/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SuccessDialog extends StatelessWidget {
  SuccessDialog({
    this.title = 'congratulations',
    this.imageUrl = 'assets/images/well_done2.png',
    this.description = '',
    this.actionText = 'continue',
    this.actionCallback,
  });

  final String title;
  final String imageUrl;
  final String description;
  final String actionText;
  final VoidCallback actionCallback;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      // insetPadding: EdgeInsets.all(20),
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
            ),
            padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title.tr,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Pacifico',
                  ),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    description.tr,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: RoundedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      // actionCallback();
                    },
                    text: 'continue'.tr,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
              top: -120,
              child: Image.asset(
                imageUrl,
                width: 200,
                height: 200,
              ))
        ],
      ),
    );
  }
}
