import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/auth_controller.dart';
import 'package:app/ui/pages/auth/components/auth_background.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:app/ui/widgets/rounded_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VerifyForm extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GlobalKey key = GlobalKey<FormState>();
    return AuthBackground(
      child: SingleChildScrollView(
        child: Form(
          key: key,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(height: Get.height * 0.2),
              Text(
                'verification_info'.tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18.0,
                  letterSpacing: 1.0,
                ),
              ),
              SizedBox(height: 8.0),
              RoundedInputField(
                keyboardType: TextInputType.number,
                hintText: 'hint_verification_code'.tr,
                onSaved: (value) => controller.verificationCode = value,
                validator: (value) =>
                    value.trim().isEmpty || value.trim().length < 6
                        ? 'invalid_code'.tr
                        : null,
              ),
              SizedBox(height: size.height * 0.01),
              GestureDetector(
                onTap: () {
                  //Todo: resend code(add counter too.)
                },
                child: Text(
                  'resend_verification'.tr,
                  style: TextStyle(
                    fontSize: 16.0,
                    letterSpacing: 1.0,
                    color: AppTheme.primaryColor,
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.03),
              RoundedButton(
                text: 'submit'.tr,
                onPressed: () {
                  final FormState form = key.currentState;
                  if (form.validate()) {
                    form.save();
                    controller.verifyEmail();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
