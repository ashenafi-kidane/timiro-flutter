import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/auth_controller.dart';
import 'package:app/ui/pages/auth/components/auth_background.dart';
import 'package:app/ui/pages/auth/components/logo.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:app/ui/widgets/rounded_input_field.dart';
import 'package:app/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignupForm extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GlobalKey key = GlobalKey<FormState>();
    return AuthBackground(
      child: SingleChildScrollView(
        child: Column(
          children: [
            // SvgPicture.asset(
            //   "assets/icons/signup.svg",
            //   height: size.height * 0.35,
            // ),
            Logo(),
            SizedBox(height: size.height * 0.08),
            Form(
              key: key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RoundedInputField(
                    keyboardType: TextInputType.name,
                    hintText: 'fullname'.tr,
                    icon: Icons.person,
                    onSaved: (value) => controller.fullname = value,
                    validator: (value) {
                      final fullname = value.trim().split(' ');
                      return fullname.length < 2 ? 'error_name'.tr : null;
                    },
                  ),
                  RoundedInputField(
                    keyboardType: TextInputType.emailAddress,
                    hintText: 'email'.tr,
                    icon: Icons.email,
                    onSaved: (value) => controller.email = value,
                    validator: (value) =>
                        GetUtils.isEmail(value) ? null : 'error_email'.tr,
                  ),
                  Obx(
                    () => RoundedInputField(
                      keyboardType: TextInputType.text,
                      onChanged: (value) => controller.password = value,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.lock,
                          color: AppTheme.primaryColor,
                        ),
                        hintText: 'password'.tr,
                        suffixIcon: IconButton(
                          icon: Icon(
                            controller.passwordVisiblity
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                          onPressed: () {
                            controller.passwordVisiblity =
                                !controller.passwordVisiblity;
                          },
                        ),
                        border: InputBorder.none,
                      ),
                      obscureText: !controller.passwordVisiblity,
                      validator: (value) =>
                          isPasswordValid(value) ? null : 'invalid_password'.tr,
                    ),
                  ),
                  Obx(
                    () => RoundedInputField(
                      keyboardType: TextInputType.text,
                      onSaved: (value) => controller.confirmPassword = value,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.lock,
                          color: AppTheme.primaryColor,
                        ),
                        hintText: 'confirm_password'.tr,
                        suffixIcon: IconButton(
                          icon: Icon(
                            controller.passwordVisiblity
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                          onPressed: () {
                            controller.passwordVisiblity =
                                !controller.passwordVisiblity;
                          },
                        ),
                        border: InputBorder.none,
                      ),
                      obscureText: !controller.passwordVisiblity,
                      validator: (value) => value == controller.password
                          ? null
                          : 'error_confirm_password'.tr,
                    ),
                  ),
                  SizedBox(height: size.height * 0.01),
                  GestureDetector(
                    onTap: () {
                      Get.offNamed(Routes.LOGIN);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8.0,
                      ),
                      child: Text(
                        'already_memeber'.tr,
                        style: TextStyle(
                          fontSize: 16.0,
                          letterSpacing: 1.0,
                          color: AppTheme.primaryColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: size.height * 0.02),
                  RoundedButton(
                    text: 'signup'.tr,
                    onPressed: () {
                      final FormState form = key.currentState;
                      if (form.validate()) {
                        form.save();
                        controller.signup();
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
