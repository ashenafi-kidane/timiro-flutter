import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/auth_controller.dart';
import 'package:app/ui/pages/auth/components/auth_background.dart';
import 'package:app/ui/pages/auth/components/logo.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:app/ui/widgets/rounded_input_field.dart';
import 'package:app/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginForm extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GlobalKey key = GlobalKey<FormState>();
    return AuthBackground(
      child: SingleChildScrollView(
        child: Column(
          children: [
            // SvgPicture.asset(
            //   "assets/icons/login.svg",
            //   height: size.height * 0.35,
            // ),
            Logo(),
            SizedBox(height: size.height * 0.08),
            Form(
              key: key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RoundedInputField(
                    keyboardType: TextInputType.emailAddress,
                    hintText: 'email'.tr,
                    icon: Icons.email,
                    onSaved: (value) => controller.email = value,
                    validator: (value) =>
                        GetUtils.isEmail(value) ? null : 'error_email'.tr,
                  ),
                  Obx(
                    () => RoundedInputField(
                      keyboardType: TextInputType.text,
                      onSaved: (value) => controller.password = value,
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.lock,
                          color: AppTheme.primaryColor,
                        ),
                        hintText: 'password'.tr,
                        border: InputBorder.none,
                        suffixIcon: IconButton(
                          icon: Icon(
                            controller.passwordVisiblity
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                          onPressed: () {
                            controller.passwordVisiblity =
                                !controller.passwordVisiblity;
                          },
                        ),
                      ),
                      obscureText: !controller.passwordVisiblity,
                      validator: (value) =>
                          isPasswordValid(value) ? null : 'invalid_password'.tr,
                    ),
                  ),
                  SizedBox(height: size.height * 0.01),
                  GestureDetector(
                    onTap: () {
                      //Todo: show password reset screen
                    },
                    child: Text(
                      'forgot_password'.tr,
                      style: TextStyle(
                        fontSize: 16.0,
                        letterSpacing: 1.0,
                        color: AppTheme.primaryColor,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.offNamed(Routes.SIGNUP);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8.0,
                      ),
                      child: Text(
                        'not_memeber'.tr,
                        style: TextStyle(
                          fontSize: 16.0,
                          letterSpacing: 1.0,
                          color: AppTheme.primaryColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  RoundedButton(
                    text: 'login'.tr,
                    onPressed: () {
                      final FormState form = key.currentState;
                      if (form.validate()) {
                        form.save();
                        controller.login();
                      }
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
