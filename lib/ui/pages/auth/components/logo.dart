import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.6,
      height: Get.height * 0.12,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(40)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'logo',
            child: Container(
              child: Image.asset('assets/images/logo.png'),
              height: 60.0,
            ),
          ),
          TypewriterAnimatedTextKit(
            text: ['app_name'.tr],
            textStyle: TextStyle(
              color: AppTheme.primaryColor,
              fontSize: 40.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'Pacifico',
            ),
          ),
        ],
      ),
    );
  }
}
