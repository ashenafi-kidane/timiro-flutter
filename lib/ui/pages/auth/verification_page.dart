import 'package:app/core/enums/common_enums.dart';
import 'package:app/ui/controllers/auth_controller.dart';
import 'package:app/ui/pages/auth/components/auth_background.dart';
import 'package:app/ui/pages/auth/components/verify_form.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class VerificationPage extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => ModalProgressHUD(
          inAsyncCall: controller.status.value == Status.loading,
          child: AuthBackground(
            child: VerifyForm(),
          ),
        ),
      ),
    );
  }
}
