import 'package:app/ui/controllers/auth_controller.dart';
import 'package:app/ui/widgets/splash.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InitialPage extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    controller.determineNextRoute();
    return Splash();
  }
}
