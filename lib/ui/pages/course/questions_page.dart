import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/common/confirm_dialog.dart';
import 'package:app/ui/pages/course/components/add_question_dialog.dart';
import 'package:app/ui/pages/course/components/question_list.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuestionsPage extends GetView<CoursesController> {
  // final LessonContent question = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: controller.selectedLessonContent.value.title,
        actions: [
          !controller.isInstructor
              ? CustomButton(
                  text: 'submit'.tr,
                  width: 80,
                  onPressed: () {
                    controller.submitAnswers();
                  },
                )
              : SizedBox(),
          GestureDetector(
            onTap: () => Get.toNamed(Routes.DISCUSSION),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Icon(
                Icons.chat,
                color: AppTheme.primaryColor,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => controller.selectedLessonContent.value != null
            ? controller.isInstructor &&
                    !controller
                        .selectedLessonContent.value.questions.isNullOrBlank
                ? FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () => showAddQuestionDialog(context),
                  )
                : Container()
            : null,
      ),
      body: Obx(
        () => QuestionList(
          questions: controller.selectedLessonContent.value.questions,
          isInstructor: controller.isInstructor,
          onAnswerCallback: (answer, index) =>
              controller.setAnswer(answer, index),
          emptyActionCallback: () => showAddQuestionDialog(context),
        ),
      ),
    );
  }

  void showAddQuestionDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AddQuestionDialog(
          callback: ({textQuestion, questionType, choices, answer, point}) =>
              controller.addQuestion(
                  textQuestion, questionType, choices, answer, point),
        );
      },
    );
  }
}
