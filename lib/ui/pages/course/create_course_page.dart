import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreateCoursePage extends GetView<CoursesController> {
  // final controller = Get.put(CoursesController(repository: null))
  @override
  Widget build(BuildContext context) {
    final GlobalKey key = GlobalKey<FormState>();
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Form(
              key: key,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'create_a_course'.tr,
                    textAlign: TextAlign.left,
                    style: AppTheme.title,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      onSaved: (value) => controller.title = value,
                      decoration: AppTheme.textFormFieldDecoration.copyWith(
                        labelText: 'course_title'.tr,
                      ),
                      maxLength: 50,
                      enableSuggestions: true,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) =>
                          value.trim().length < 1 ? 'error_input'.tr : null,
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButtonFormField<CourseCategory>(
                        isExpanded: true,
                        decoration: AppTheme.textFormFieldDecoration.copyWith(
                          labelText: 'select_category'.tr,
                        ),
                        items: controller.categories
                            .map<DropdownMenuItem<CourseCategory>>(
                                (CourseCategory category) {
                          return DropdownMenuItem<CourseCategory>(
                            value: category,
                            child: new Text(category.name),
                          );
                        }).toList(),
                        value: controller.category == null
                            ? null
                            : controller.category,
                        onChanged: (value) => controller.category = value,
                        validator: (value) => controller.category == null
                            ? 'error_input'.tr
                            : null,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      onSaved: (value) => controller.description = value,
                      decoration: AppTheme.textFormFieldDecoration.copyWith(
                        labelText: 'course_description'.tr,
                      ),
                      enableSuggestions: true,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      maxLines: 4,
                      validator: (value) =>
                          value.trim().length < 1 ? 'error_input'.tr : null,
                    ),
                  ),
                  SizedBox(
                    height: 35.0,
                  ),
                  RoundedButton(
                    onPressed: () {
                      final FormState form = key.currentState;
                      if (form.validate()) {
                        form.save();
                        //create course here
                        controller.createCourse();
                      }
                    },
                    text: 'create_course'.tr,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
