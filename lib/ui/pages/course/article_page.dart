import 'package:app/core/enums/course_enums.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/add_article_content_dialog.dart';
import 'package:app/ui/pages/course/components/article_bottosheet.dart';
import 'package:app/ui/pages/course/components/article_list.dart';
import 'package:app/ui/widgets/image_picker_bottomsheet.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ArticlePage extends GetView<CoursesController> {
  // final LessonContent article = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: controller.selectedLessonContent.value.title,
        actions: [
          GestureDetector(
            onTap: () => Get.toNamed(Routes.DISCUSSION),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Icon(
                Icons.chat,
                color: AppTheme.primaryColor,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => controller.selectedLessonContent.value != null
            ? controller.isInstructor &&
                    !controller
                        .selectedLessonContent.value.articles.isNullOrBlank
                ? FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () => Get.bottomSheet(
                      ArticleBottomsheet(
                        onPickCallback: (articleOption) {
                          Navigator.pop(context);
                          //Todo: pick file for the case of image
                          showAddContentDialog(context, articleOption);
                        },
                      ),
                    ),
                  )
                : Container()
            : null,
      ),
      body: Obx(
        () => ArticleList(
          articles: controller.selectedLessonContent.value.articles,
          isInstructor: controller.isInstructor,
          emptyActionCallback: () => Get.bottomSheet(
            ArticleBottomsheet(
              onPickCallback: (articleOption) {
                Navigator.pop(context);
                if (articleOption == ArticleOptions.image) {
                  Get.bottomSheet(
                    ImagePickerBottomsheet(
                      onPickFromCallback: (imageSource) =>
                          controller.getImage(imageSource, ImageFor.article),
                    ),
                    backgroundColor: Colors.white,
                  );
                } else
                  showAddContentDialog(context, articleOption);
              },
            ),
          ),
        ),
      ),
    );
  }

  void showAddContentDialog(
      BuildContext context, ArticleOptions articleOption) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AddArticleDialog(
          topTitle: describeEnum(articleOption),
          callback: (inputText) {
            switch (articleOption) {
              case ArticleOptions.title:
                controller.addArticle(title: inputText);
                break;
              case ArticleOptions.paragraph:
                controller.addArticle(
                  paragraph: inputText,
                );
                break;
              case ArticleOptions.image:
                controller.addArticle(
                  filePath: 'someImageId',
                );
                break;
              default:
            }
          },
        );
      },
    );
  }
}
