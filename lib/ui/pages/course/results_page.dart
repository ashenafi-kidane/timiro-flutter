import 'package:app/core/enums/course_enums.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/question_tile.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResultsPage extends GetView<CoursesController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Obx(
          () => ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final submission =
                  controller.selectedSubmissionContent.submissions[index];
              return QuestionTile(
                question: submission.question,
                isInstructor: false,
                index: index,
                questionMode: QuestionMode.submission,
                submissionAnswer: submission.textAnswer,
                resultPoint: submission.resultPoint,
              );
            },
            itemCount: controller.selectedSubmissionContent.submissions.length,
          ),
        ),
      ),
    );
  }
}
