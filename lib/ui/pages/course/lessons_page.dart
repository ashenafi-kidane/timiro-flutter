import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/add_lesson_dialog.dart';
import 'package:app/ui/pages/course/components/lesson_list.dart';
import 'package:app/ui/widgets/title_view.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class LessonsPage extends GetView<CoursesController> {
  void showAddLessonDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AddLessonDialog(
          callback: (title, description, videoUrl) =>
              controller.addLesson(title, description),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: controller.selectedCourse.value != null
            ? controller.selectedCourse.value.title
            : '',
        avator: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: CircleAvatar(
            radius: 20.0,
            backgroundImage: AssetImage('assets/images/test2.jpeg'),
          ),
        ),
        onTitleTapCallback: () {
          Get.toNamed(Routes.COURSEPROFILE);
          controller.initAnimation();
        },
        actions: [
          GestureDetector(
            onTap: () => Get.toNamed(Routes.DISCUSSION),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Icon(
                Icons.chat,
                color: AppTheme.primaryColor,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => controller.selectedCourse.value != null
            ? controller.isInstructor &&
                    !controller.selectedCourse.value.lessons.isNullOrBlank
                ? FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () => showAddLessonDialog(context),
                  )
                : SizedBox()
            : null,
      ),
      body: controller.selectedCourse.value != null
          ? Obx(
              () => Column(
                children: [
                  //change the check here.
                  !controller.selectedCourse.value.lessons.isNullOrBlank
                      ? TitleView(
                          title: 'lessons',
                          subTitle: 'lesson_info',
                        )
                      : SizedBox(),
                  Expanded(
                    child: LessonList(
                      lessons: controller.selectedCourse.value.lessons,
                      emptyActionCallback: () => showAddLessonDialog(context),
                      isInstructor: controller.isInstructor,
                      itemTapCallBack: (lesson) {
                        controller.selectedlesson.value = lesson;
                        //show the course page, check if the user is the instructor or not.
                        Get.toNamed(Routes.COURSECONTENT);
                      },
                    ),
                  ),
                ],
              ),
            )
          : Center(
              child: Text('selected course is empty'),
            ),
    );
  }
}
