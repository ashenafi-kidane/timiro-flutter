import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:app/ui/controllers/submission_controller.dart';
import 'package:app/ui/pages/course/components/submission_list.dart';
import 'package:app/ui/pages/profile/profile_page.dart';
import 'package:app/ui/widgets/title_view.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubmissionsPage extends GetView<SubmissionController> {
  @override
  Widget build(BuildContext context) {
    final student = controller.selectedStudent.profile;
    return Scaffold(
      appBar: WhiteAppBar(
        title: student.firstName + ' ' + student.lastName,
        avator: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: CircleAvatar(
            radius: 20.0,
            backgroundImage: AssetImage('assets/images/test2.jpeg'),
          ),
        ),
        //Todo: may be required to send the current student info to profiles page.
        onTitleTapCallback: () => Get.to(ProfilePage(
          animationController: controller.animationController,
          studentProfile: student,
        )),
      ),
      body: Column(
        children: [
          !controller.selectedStudent.submissionResponses.isEmpty
              ? TitleView(
                  title: 'submissions'.tr,
                  subTitle: 'submissions_info'.tr,
                )
              : SizedBox(),
          Expanded(
            child: SubmissionList(
              submissionLessonContents:
                  controller.selectedSubmissionResponse.lessonContents,
              itemTapCallBack: (submissionLessonContent) {
                controller.selectedSubmissionContent = submissionLessonContent;
                Get.toNamed(Routes.EVALUATIONS);
              },
            ),
          )
        ],
      ),
    );
  }
}
