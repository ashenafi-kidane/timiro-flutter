import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/course_video_player.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VideoPage extends GetView<CoursesController> {
  // VideoPage({this.videoItem});
  // final VideoItem videoItem;
  // final LessonContent videoItem = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: controller.selectedLessonContent.value.title,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.chat),
        onPressed: () => Get.toNamed(Routes.DISCUSSION),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // CourseVideoPlayer(
            //   url: controller.selectedLessonContent.value.videoUrl,
            // ),
            !controller.selectedLessonContent.value.description.isNullOrBlank
                ? Padding(
                    padding: const EdgeInsets.only(
                        top: 24.0, left: 8.0, right: 8.0, bottom: 8.0),
                    child: Text(
                      'description'.tr,
                      style: AppTheme.title,
                    ),
                  )
                : SizedBox(),
            !controller.selectedLessonContent.value.description.isNullOrBlank
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      controller.selectedLessonContent.value.description,
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
      // body: ChewieDemo(),
    );
  }
}
