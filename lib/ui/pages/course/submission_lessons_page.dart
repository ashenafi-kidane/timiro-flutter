import 'package:app/shared/routes/app_pages.dart';
import 'package:app/ui/controllers/submission_controller.dart';
import 'package:app/ui/pages/course/components/submissions_lesson_list.dart';
import 'package:app/ui/widgets/title_view.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubmissionLessonPage extends GetView<SubmissionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          controller.selectedStudent.submissionResponses.isNotEmpty
              ? TitleView(
                  title: 'submitted_lessons',
                  subTitle: 'submitted_lessons_info',
                )
              : SizedBox(),
          Expanded(
            child: SubmissionsLessonList(
              submissionLessons: controller.selectedStudent.submissionResponses,
              itemTapCallBack: (submissionResponse) {
                controller.selectedSubmissionResponse = submissionResponse;
                Get.toNamed(Routes.SUBMISSIONS);
              },
            ),
          )
        ],
      ),
    );
  }
}
