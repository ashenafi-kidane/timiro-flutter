import 'package:app/core/enums/course_enums.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/add_lesson_dialog.dart';
import 'package:app/ui/pages/course/components/content_picker_bottomsheet.dart';
import 'package:app/ui/pages/course/components/contents_list.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class ContentsPage extends GetView<CoursesController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: controller.selectedlesson.value.title,
        actions: [
          GestureDetector(
            onTap: () => Get.toNamed(Routes.DISCUSSION),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Icon(
                Icons.chat,
                color: AppTheme.primaryColor,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => controller.selectedlesson.value != null
            ? controller.isInstructor &&
                    !controller
                        .selectedlesson.value.lessonContents.isNullOrBlank
                ? FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () => Get.bottomSheet(
                      ContentPickerBottomsheet(
                        onPickCallback: (contentOption) {
                          Navigator.pop(context);
                          showAddContentDialog(context, contentOption);
                        },
                      ),
                      backgroundColor: Colors.white,
                    ),
                  )
                : Container()
            : Container(),
      ),
      body: Obx(
        () => Column(
          children: [
            //change the check here.
            !controller.selectedlesson.value.lessonContents.isNullOrBlank
                ? Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            'lesson_contents'.tr,
                            style: AppTheme.title,
                          ),
                        ),
                        Text(
                          'contents_info'.tr,
                          style: AppTheme.subtitle,
                        ),
                      ],
                    ),
                  )
                : SizedBox(),

            Expanded(
              child: ContentList(
                contents: controller.selectedlesson.value.lessonContents,
                isInstructor: controller.isInstructor,
                emptyActionCallback: () => Get.bottomSheet(
                  ContentPickerBottomsheet(
                    onPickCallback: (contentOption) {
                      Navigator.pop(context);
                      showAddContentDialog(context, contentOption);
                    },
                  ),
                  backgroundColor: Colors.white,
                ),
                itemTapCallBack: (content, index) =>
                    controller.onSelectContent(content, index),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showAddContentDialog(
      BuildContext context, ContentOptions contentOption) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AddLessonDialog(
          topTitle: describeEnum(contentOption),
          pickVideocallback: (videoSource) => controller.getVideo(videoSource),
          callback: (title, description, videoUrl) {
            if (contentOption == ContentOptions.FILE) {
              if (videoUrl.isNullOrBlank &&
                  controller.pickedVideoPath.value.isNullOrBlank) {
                Get.snackbar('video_error', 'video_error_info');
                return;
              } else
                Navigator.of(context).pop();
            }
            controller.addLessonContent(
              title,
              description,
              contentOption,
              videoUrl,
            );
          },
        );
      },
    );
  }
}
