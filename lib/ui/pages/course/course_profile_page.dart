import 'package:app/core/enums/course_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/routes/app_pages.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/box_ui.dart';
import 'package:app/ui/pages/course/components/rating_dialog.dart';
import 'package:app/ui/widgets/back_arrow.dart';
import 'package:app/ui/widgets/image_picker_bottomsheet.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'dart:io';

class CourseProfilePage extends GetView<CoursesController> {
  // CourseProfilePage({Key key, this.animationController}) : super(key: key);

  List<Widget> listViews = [];
  final viewCount = 6;

  @override
  Widget build(BuildContext context) {
    getViews();
    return Scaffold(
      floatingActionButton: Obx(
        () => controller.isInstructor && controller.isCourseEdited
            ? FloatingActionButton(
                child: Text('save'.tr),
                onPressed: () => controller.editCourse(),
              )
            : Container(),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Obx(
                () => GestureDetector(
                  onTap: () {
                    if (controller.isInstructor) {
                      Get.bottomSheet(
                        ImagePickerBottomsheet(
                          onPickFromCallback: (imageSource) => controller
                              .getImage(imageSource, ImageFor.courseProfile),
                        ),
                        backgroundColor: Colors.white,
                      );
                    }
                  },
                  child: AspectRatio(
                    aspectRatio: 2,
                    child: controller.courseImagePath.isNotEmpty
                        ? kIsWeb
                            ? Image.network(controller.courseImagePath)
                            : Image.file(File(controller.courseImagePath),
                                fit: BoxFit.cover)
                        : Image.asset(
                            'assets/images/test2.jpeg',
                            fit: BoxFit.fill,
                          ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: (MediaQuery.of(context).size.width / 2) - 40.0,
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                color: AppTheme.nearlyWhite,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(32.0),
                    topRight: Radius.circular(32.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: AppTheme.grey.withOpacity(0.2),
                      offset: const Offset(1.1, 1.1),
                      blurRadius: 10.0),
                ],
              ),
              child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom,
                ),
                itemCount: listViews.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  // animationController.forward();
                  return listViews[index];
                },
              ),
            ),
          ),
          Positioned(
            top: (MediaQuery.of(context).size.width / 2) - 40.0 - 35,
            right: 35,
            child: ScaleTransition(
              alignment: Alignment.center,
              scale: CurvedAnimation(
                  parent: controller.animationController,
                  curve: Curves.fastOutSlowIn),
              child: GestureDetector(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      //those who can rate are those who are not instructors
                      //and didn't rate already
                      return RatingDialog(
                        canRate: !controller.isInstructor &&
                            controller.userCourseRating == null,
                        value: controller.userCourseRating ?? 2.5,
                        numberOfRates: controller.selectedCourse.value.rating !=
                                null
                            ? controller.selectedCourse.value.rating.totalRates
                            : 0,
                        onRateCallback: (rating) {
                          controller.rateCourse(rating);
                        },
                      );
                    },
                  );
                },
                child: Card(
                  color: AppTheme.nearlyBlue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0)),
                  elevation: 10.0,
                  child: Container(
                    width: 60,
                    height: 60,
                    child: Center(
                      child: Icon(
                        Icons.favorite,
                        color: AppTheme.nearlyWhite,
                        size: 30,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          BackArrow(),
        ],
      ),
    );
  }

  void getViews() {
    listViews = [
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
            child: TextFormField(
              initialValue: controller.title,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
              ),
              enabled: controller.isInstructor,
              style: TextStyle(
                  color: Colors.teal,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Source Sans Pro',
                  letterSpacing: 2.5),
              onChanged: (value) {
                controller.title = value;
                controller.checkCourseEdited();
              },
            ),
          ),
          SizedBox(
            width: Get.width / 2,
            child: Divider(
              color: Colors.teal,
            ),
          ),
        ],
      ),
      Obx(
        () => AnimatedOpacity(
          duration: const Duration(milliseconds: 1000),
          opacity: controller.opacity1.value,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
            child: TextFormField(
              initialValue: controller.description,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
              ),
              enabled: controller.isInstructor,
              style: TextStyle(
                fontSize: 16.0,
                letterSpacing: 1.2,
              ),
              maxLines: 3,
              onChanged: (value) {
                controller.description = value;
                controller.checkCourseEdited();
              },
            ),
          ),
        ),
      ),
      Obx(
        () => AnimatedOpacity(
          duration: const Duration(milliseconds: 500),
          opacity: controller.opacity2.value,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Text(
              'category'.tr,
              style: AppTheme.title,
            ),
          ),
        ),
      ),
      Obx(
        () => AnimatedOpacity(
          duration: const Duration(milliseconds: 500),
          opacity: controller.opacity2.value,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 24.0,
            ),
            child: DropdownButtonFormField<CourseCategory>(
              decoration: InputDecoration(
                hintText: 'select_category'.tr,
                fillColor: Colors.white,
                alignLabelWithHint: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
              items: controller.categories
                  .map<DropdownMenuItem<CourseCategory>>(
                      (CourseCategory category) {
                return DropdownMenuItem<CourseCategory>(
                  value: category,
                  child: new Text(category.name),
                );
              }).toList(),
              //Todo: change the following with this
              //controller.selectedCourse.value.category.name
              disabledHint: Text('Test category'),
              value: controller.category == null ? null : controller.category,
              onChanged: controller.isInstructor
                  ? (value) {
                      controller.category = value;
                      controller.checkCourseEdited();
                    }
                  : null,
            ),
          ),
        ),
      ),
      Obx(
        () => AnimatedOpacity(
          duration: const Duration(milliseconds: 500),
          opacity: controller.opacity3.value,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'instructor'.tr,
                        style: AppTheme.title,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Container(
                          height: 40,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              final instructor = controller
                                  .selectedCourse.value.instructors[index];
                              return Chip(label: Text(instructor.firstName));
                            },
                            itemCount: controller
                                .selectedCourse.value.instructors.length,
                          ),
                        ),
                      ),
                      //Todo: show and implementa add instructor logic here (if supported)
                      // Padding(
                      //   padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      //   child: Align(
                      //     alignment: Alignment.centerLeft,
                      //     child: CurvedButton(
                      //       text: 'add_instructor'.tr,
                      //       onPressed: () {
                      //         //Todo: add instructor.
                      //       },
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 24.0),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: TextFormField(
                      initialValue: controller.price,
                      enabled: controller.isInstructor,
                      onChanged: (value) {
                        controller.price = value;
                        controller.checkCourseEdited();
                      },
                      keyboardType: TextInputType.number,
                      decoration: AppTheme.textFormFieldDecoration.copyWith(
                        labelText: 'price'.tr,
                      ),
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      Obx(
        () => AnimatedOpacity(
          duration: const Duration(milliseconds: 500),
          opacity: controller.opacity4.value,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    BoxUI(
                      topText:
                          '${controller.selectedCourse.value.studentsCount ?? 0}',
                      bottomText: 'enrolled'.tr,
                      onTapCallback: () {
                        if (controller.isInstructor)
                          Get.toNamed(Routes.STUDENTS);
                      },
                    ),
                    BoxUI(
                      topText:
                          '${controller.selectedCourse.value.lessonsCount ?? 0}',
                      bottomText: 'lessons'.tr,
                      onTapCallback: () => Get.back(),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '${controller.selectedCourse.value.rating != null ? controller.selectedCourse.value.rating.value : 0}',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w200,
                        fontSize: 22,
                        letterSpacing: 0.27,
                        color: AppTheme.grey,
                      ),
                    ),
                    Icon(
                      Icons.star,
                      color: AppTheme.nearlyBlue,
                      size: 24,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ];
  }
}
