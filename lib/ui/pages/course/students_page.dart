import 'package:app/shared/routes/app_pages.dart';
import 'package:app/ui/controllers/submission_controller.dart';
import 'package:app/ui/pages/course/components/student_list.dart';
import 'package:app/ui/widgets/title_view.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Studentspage extends GetView<SubmissionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(),
      body: controller.obx((state) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              state.isNotEmpty
                  ? TitleView(
                      title: 'students',
                      subTitle: 'students_info',
                    )
                  : SizedBox(),
              Expanded(
                child: StudentList(
                  students: state,
                  itemTapCallBack: (student) {
                    controller.selectedStudent = student;
                    Get.toNamed(Routes.SUBMISSION_LESSONS);
                  },
                ),
              )
            ],
          )),
    );
  }
}
