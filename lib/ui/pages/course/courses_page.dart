import 'package:app/shared/routes/app_pages.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/pages/course/components/course_list.dart';
import 'package:app/ui/widgets/home_animation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CoursesPage extends GetView<CoursesController> {
  CoursesPage({Key key, this.animationController}) : super(key: key);

  final AnimationController animationController;
  @override
  Widget build(BuildContext context) {
    controller.animationController = animationController;
    animationController.forward();
    return Container(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        floatingActionButton: Obx(
          () => controller.courses.isNotEmpty
              ? FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () => Get.toNamed(Routes.CREATECOURSE),
                )
              : SizedBox(),
        ),
        body: HomeAnimation(
          animationController: animationController,
          viewCount: 1,
          index: 0,
          view: Stack(
            children: [
              controller.obx((state) => CourseList(
                    courses: state,
                    withAppBar: true,
                    itemTapCallBack: (course, index) {
                      controller.onCourseSelected(course, index);
                    },
                    emptyActionCallback: () => Get.toNamed(Routes.CREATECOURSE),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
