import 'package:app/core/enums/course_enums.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AddQuestionDialog extends StatefulWidget {
  AddQuestionDialog({
    this.topTitle = 'Question',
    this.callback,
  });

  final String topTitle;
  final Function({
    String textQuestion,
    QuestionType questionType,
    List<String> choices,
    String answer,
    double point,
  }) callback;

  @override
  _AddQuestionDialogState createState() => _AddQuestionDialogState();
}

class _AddQuestionDialogState extends State<AddQuestionDialog> {
  List<QuestionType> questionTypes = [
    QuestionType.TRUE_FALSE,
    QuestionType.MULTIPLE_CHOICE,
    QuestionType.SHORT_ANSWER,
    QuestionType.FILE_SUBMISSION,
  ];
  List<String> _choices;
  String _selection;
  TextEditingController _textQuestionController = TextEditingController();
  TextEditingController _pointController = TextEditingController();
  TextEditingController _shortAnswerController = TextEditingController();
  QuestionType _questionType;
  String _answerFilepath;

  @override
  void initState() {
    _resetState();
    _pointController.text = '1';
    super.initState();
  }

  void _resetState() {
    _choices = [''];
    _selection = ' ';
    _shortAnswerController.text = '';
    _answerFilepath = '';
  }

  @override
  void dispose() {
    _textQuestionController?.dispose();
    _shortAnswerController?.dispose();
    _pointController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey key = GlobalKey<FormState>();
    return AlertDialog(
      title: Text(
        'add_question'.tr,
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: key,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                controller: _textQuestionController,
                decoration: AppTheme.textFormFieldDecoration.copyWith(
                  labelText: widget.topTitle.tr,
                ),
                enableSuggestions: true,
                maxLines: 3,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) =>
                    value.trim().isEmpty ? 'error_input'.tr : null,
              ),
              Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButtonFormField<QuestionType>(
                        isExpanded: true,
                        decoration: AppTheme.textFormFieldDecoration.copyWith(
                          labelText: 'select_category'.tr,
                        ),
                        items: questionTypes.map((QuestionType type) {
                          return DropdownMenuItem<QuestionType>(
                            value: type,
                            child: Text(_getQuestionName(type)),
                          );
                        }).toList(),
                        value: _questionType,
                        onChanged: (value) {
                          setState(() {
                            _questionType = value;
                            _resetState();
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextFormField(
                      controller: _pointController,
                      keyboardType: TextInputType.number,
                      decoration: AppTheme.textFormFieldDecoration.copyWith(
                        labelText: 'point'.tr,
                      ),
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) =>
                          value.trim().isEmpty || int.parse(value) < 1
                              ? 'error'.tr
                              : null,
                    ),
                  ),
                ],
              ),
              getQuestionAnswer(),
              SizedBox(
                height: 24.0,
              ),
              RoundedButton(
                onPressed: () {
                  final FormState form = key.currentState;
                  if (form.validate()) {
                    if (_questionType.isNullOrBlank) {
                      Get.snackbar('error_input'.tr, 'error_question_type'.tr);
                    } else if (_questionType == QuestionType.MULTIPLE_CHOICE &&
                        _choices.length < 2) {
                      Get.snackbar('error_input'.tr, 'error_choices'.tr);
                    } else if ((_questionType == QuestionType.MULTIPLE_CHOICE ||
                            _questionType == QuestionType.TRUE_FALSE) &&
                        _selection.isNullOrBlank) {
                      Get.snackbar('error_input'.tr, 'error_selection'.tr);
                    } else {
                      form.save();
                      String answer = '';
                      switch (_questionType) {
                        case QuestionType.TRUE_FALSE:
                        case QuestionType.MULTIPLE_CHOICE:
                          answer = _selection;
                          break;
                        case QuestionType.SHORT_ANSWER:
                          answer = _shortAnswerController.text;
                          break;
                        case QuestionType.FILE_SUBMISSION:
                          answer = _shortAnswerController.text;
                          break;
                        default:
                      }
                      widget.callback(
                        textQuestion: _textQuestionController.text,
                        questionType: _questionType,
                        choices: _choices,
                        answer: answer,
                        point: double.tryParse(_pointController.text) ?? 0,
                      );
                      Navigator.of(context).pop();
                    }
                  }
                },
                text: 'done'.tr,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getQuestionAnswer() {
    switch (_questionType) {
      case QuestionType.TRUE_FALSE:
        _choices = ['True', 'False'];
        return getMultipleChoice();
        break;
      case QuestionType.MULTIPLE_CHOICE:
        return getMultipleChoice();
        break;
      case QuestionType.SHORT_ANSWER:
        return addShortAnswer();
        break;
      case QuestionType.FILE_SUBMISSION:
        return attachFile();
        break;
      default:
        return Container();
    }
  }

  Widget getMultipleChoice() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _questionType == QuestionType.MULTIPLE_CHOICE
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'add_choices'.tr,
                    style: AppTheme.title,
                  ),
                  FloatingActionButton(
                    child: Icon(Icons.add),
                    mini: true,
                    onPressed: () {
                      setState(() {
                        if (_choices.contains('')) {
                          _choices = _choices;
                        } else {
                          _choices.add('');
                        }
                      });
                    },
                  )
                ],
              )
            : Container(),
        Column(
          children: _choices.asMap().entries.map((item) {
            //change index of choices array as you need
            return RadioListTile(
              groupValue: _selection,
              title: TextField(
                controller: TextEditingController()..text = item.value,
                readOnly:
                    _questionType == QuestionType.TRUE_FALSE ? true : false,
                decoration: InputDecoration(
                  hintText: 'enter_value'.tr,
                  errorText: item.value.isNullOrBlank ? 'error_input'.tr : null,
                  border: _questionType == QuestionType.TRUE_FALSE
                      ? InputBorder.none
                      : UnderlineInputBorder(),
                ),
                onChanged: (value) {
                  // _choiceText = value;
                  _choices[item.key] = value;
                },
              ),
              value: item.value,
              activeColor: Colors.blue,
              onChanged: (val) {
                print(val);
                setState(() {
                  _selection = val;
                });
              },
            );
          }).toList(),
        ),
      ],
    );
  }

  Widget addShortAnswer() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'add_short_answer'.tr,
            style: AppTheme.title,
          ),
          SizedBox(
            height: 8.0,
          ),
          TextFormField(
            controller: _shortAnswerController,
            decoration: AppTheme.textFormFieldDecoration.copyWith(
              labelText: 'short_answer'.tr,
            ),
            enableSuggestions: true,
            maxLines: 2,
          ),
        ],
      ),
    );
  }

  Widget attachFile() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'attach_answer'.tr,
            style: AppTheme.title,
          ),
          SizedBox(height: 8.0),
          TextButton.icon(
            style: TextButton.styleFrom(
                primary: AppTheme.primaryColor,
                textStyle: TextStyle(
                  fontSize: 18.0,
                )),
            onPressed: () async {
              //Todo: move the file picking logic to the controller
              FilePickerResult result = await FilePicker.platform.pickFiles();
              if (result != null) {
                _answerFilepath = result.files.single.path;
                // File file = File(result.files.single.path);
                Get.snackbar('File Pick',
                    'File pick success! ${result.files.single.path}');
              } else {
                // User canceled the picker
                Get.snackbar('File pick Error', 'File Pick Error');
              }
            },
            icon: Icon(Icons.photo_album),
            label: Text('pick_file'.tr),
          ),
        ],
      ),
    );
  }

  String _getQuestionName(QuestionType questionType) {
    switch (questionType) {
      case QuestionType.TRUE_FALSE:
        return 'true_false'.tr;
        break;
      case QuestionType.MULTIPLE_CHOICE:
        return 'multiple_choice'.tr;
        break;
      case QuestionType.TRUE_FALSE:
        return 'short_answer'.tr;
        break;
      case QuestionType.TRUE_FALSE:
        return 'file_submission'.tr;
        break;
      default:
        return 'multiple_choice'.tr;
        break;
    }
  }
}
