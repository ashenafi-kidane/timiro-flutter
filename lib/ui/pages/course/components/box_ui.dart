import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';

class BoxUI extends StatelessWidget {
  BoxUI({
    Key key,
    @required this.topText,
    @required this.bottomText,
    this.onTapCallback,
  }) : super(key: key);

  final String topText;
  final String bottomText;
  final VoidCallback onTapCallback;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: onTapCallback,
        child: Container(
          decoration: BoxDecoration(
            color: AppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(16.0)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: AppTheme.grey.withOpacity(0.2),
                  offset: const Offset(1.1, 1.1),
                  blurRadius: 8.0),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  topText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    letterSpacing: 0.27,
                    color: AppTheme.nearlyBlue,
                  ),
                ),
                Text(
                  bottomText,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w200,
                    fontSize: 14,
                    letterSpacing: 0.27,
                    color: AppTheme.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
