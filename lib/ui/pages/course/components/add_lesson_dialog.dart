import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class AddLessonDialog extends StatelessWidget {
  AddLessonDialog({
    this.topTitle = 'lesson',
    @required this.callback,
    this.pickVideocallback,
  });

  final String topTitle;
  final Function(String title, String description, String videoUrl) callback;
  final Function(ImageSource videoSourse) pickVideocallback;
  @override
  Widget build(BuildContext context) {
    String title;
    String description;
    String videoUrl;
    final GlobalKey key = GlobalKey<FormState>();
    return AlertDialog(
      title: Text(
        'add'.tr + ' ' + topTitle.tr,
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: key,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                onSaved: (value) => title = value,
                decoration: AppTheme.textFormFieldDecoration.copyWith(
                  labelText: 'title'.tr,
                ),
                maxLength: 50,
                enableSuggestions: true,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) =>
                    value.trim().length < 1 ? 'error_input'.tr : null,
              ),
              SizedBox(
                height: 8.0,
              ),
              TextFormField(
                onSaved: (value) => description = value,
                decoration: AppTheme.textFormFieldDecoration.copyWith(
                  labelText: topTitle == 'lesson'
                      ? 'description'.tr
                      : 'description_optional'.tr,
                ),
                maxLength: 250,
                enableSuggestions: true,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) =>
                    topTitle == 'lesson' && value.trim().length < 1
                        ? 'error_input'.tr
                        : null,
              ),
              topTitle == 'video'
                  ? Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'add_video_url'.tr,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: AppTheme.darkText,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: TextFormField(
                              onSaved: (value) => videoUrl = value,
                              decoration:
                                  AppTheme.textFormFieldDecoration.copyWith(
                                labelText: 'video_url_hint'.tr,
                              ),
                              enableSuggestions: true,
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              // validator: (value) => value.trim().length < 1
                              //     ? 'error_input'.tr
                              //     : null,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              'or_upload_from'.tr,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                                color: AppTheme.darkText,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton.icon(
                                style: TextButton.styleFrom(
                                  primary: AppTheme.primaryColor,
                                ),
                                onPressed: () {
                                  pickVideocallback(ImageSource.camera);
                                },
                                icon: Icon(Icons.camera),
                                label: Text('camera'.tr),
                              ),
                              TextButton.icon(
                                style: TextButton.styleFrom(
                                  primary: AppTheme.primaryColor,
                                ),
                                onPressed: () {
                                  pickVideocallback(ImageSource.gallery);
                                },
                                icon: Icon(Icons.photo_album),
                                label: Text('gallery'.tr),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Container(),
              SizedBox(
                height: 24.0,
              ),
              RoundedButton(
                onPressed: () {
                  final FormState form = key.currentState;
                  if (form.validate()) {
                    form.save();
                    callback(title, description, videoUrl);
                    if (topTitle != 'video') Navigator.of(context).pop();
                  }
                },
                text: 'done'.tr,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
