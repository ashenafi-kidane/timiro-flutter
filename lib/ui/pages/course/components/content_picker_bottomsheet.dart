import 'package:app/core/enums/course_enums.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContentPickerBottomsheet extends StatelessWidget {
  ContentPickerBottomsheet({this.onPickCallback});

  final Function(ContentOptions contentOption) onPickCallback;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff757575),
      height: 235,
      child: Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
          bottom: 20.0,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'pick_content_title'.tr,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.teal,
                      letterSpacing: 1.5,
                      fontWeight: FontWeight.bold),
                ),
              ),
              ListTile(
                leading: Icon(
                  Icons.videocam,
                  color: Colors.teal,
                ),
                title: Text('video'.tr),
                onTap: () {
                  onPickCallback(ContentOptions.FILE);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.article,
                  color: Colors.teal,
                ),
                title: Text('article'.tr),
                onTap: () {
                  onPickCallback(ContentOptions.ARTICLE);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.question_answer,
                  color: Colors.teal,
                ),
                title: Text('questions'.tr),
                onTap: () {
                  onPickCallback(ContentOptions.QUESTION);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
