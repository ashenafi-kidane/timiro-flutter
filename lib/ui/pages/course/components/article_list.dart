import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'dart:io';

class ArticleList extends StatelessWidget {
  ArticleList({
    @required this.articles,
    this.itemTapCallBack,
    this.emptyActionCallback,
    this.isInstructor = false,
  });

  final List<Article> articles;
  final Function(Article article) itemTapCallBack;
  final VoidCallback emptyActionCallback;
  final bool isInstructor;

  @override
  Widget build(BuildContext context) {
    return !articles.isNullOrBlank
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final article = articles[index];
              return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    !article.title.isNullOrBlank
                        ? Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              article.title,
                              style: AppTheme.title,
                            ),
                          )
                        : Container(),
                    !article.paragraph.isNullOrBlank
                        ? Text(
                            article.paragraph,
                            style: AppTheme.body1,
                          )
                        : Container(),
                    //Todo: handle multiple files
                    !article.fileIds.isNullOrBlank
                        ? Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Image.file(File(article.fileIds[0])),
                            ),
                          )
                        : Container(),
                  ],
                ),
              );
            },
            itemCount: articles.length,
          )
        : EmptyState(
            title: isInstructor
                ? 'empty_article_title'.tr
                : 'empty_article_title_viewer'.tr,
            description: isInstructor ? 'empty_article_description'.tr : '',
            action: isInstructor
                ? CustomButton(
                    text: 'add_article'.tr,
                    onPressed: emptyActionCallback,
                  )
                : SizedBox(),
          );
  }
}
