import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class SubmissionsLessonList extends StatelessWidget {
  SubmissionsLessonList({
    @required this.submissionLessons,
    this.itemTapCallBack,
    this.emptyActionCallback,
  });

  final List<SubmissionResponse> submissionLessons;
  final Function(SubmissionResponse submissionResponse) itemTapCallBack;
  final VoidCallback emptyActionCallback;

  @override
  Widget build(BuildContext context) {
    return !submissionLessons.isNullOrBlank
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final lesson = submissionLessons[index].lesson;
              return Card(
                child: ListTile(
                  title: Text(lesson.title),
                  subtitle: Text(lesson.description),
                  onTap: () {
                    itemTapCallBack(submissionLessons[index]);
                  },
                ),
              );
            },
            itemCount: submissionLessons.length,
          )
        : EmptyState(
            title: 'empty_submissions_title'.tr,
            description: '',
          );
  }
}
