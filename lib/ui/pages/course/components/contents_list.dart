import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class ContentList extends StatelessWidget {
  ContentList({
    @required this.contents,
    this.itemTapCallBack,
    this.emptyActionCallback,
    this.isInstructor = false,
  });

  final List<LessonContent> contents;
  final Function(LessonContent content, int index) itemTapCallBack;
  final VoidCallback emptyActionCallback;
  final bool isInstructor;

  @override
  Widget build(BuildContext context) {
    return !contents.isNullOrBlank
        ? ListView.builder(
            itemBuilder: (context, index) {
              final content = contents[index];
              return Card(
                child: ListTile(
                  title: Text(content.title),
                  subtitle: !content.description.isNullOrBlank
                      ? Text(content.description)
                      : null,
                  leading: Icon(
                    getContentIcon(content.type),
                    color: AppTheme.primaryColor,
                  ),
                  trailing: isInstructor
                      ? Icon(
                          Icons.delete,
                        )
                      : SizedBox(),
                  onTap: () {
                    itemTapCallBack(content, index);
                  },
                ),
              );
            },
            itemCount: contents.length,
          )
        : EmptyState(
            title: isInstructor
                ? 'empty_content_title'.tr
                : 'empty_content_title_student'.tr,
            description: isInstructor ? 'empty_content_description'.tr : '',
            action: isInstructor
                ? CustomButton(
                    text: 'add_content'.tr,
                    onPressed: emptyActionCallback,
                  )
                : SizedBox(),
          );
  }

  IconData getContentIcon(String type) {
    switch (type) {
      case 'video':
        return Icons.videocam;
        break;
      case 'article':
        return Icons.article;
        break;
      case 'questions':
        return Icons.question_answer;
        break;
      default:
        return Icons.article;
    }
  }
}
