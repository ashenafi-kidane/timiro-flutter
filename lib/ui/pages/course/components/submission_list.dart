import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class SubmissionList extends StatelessWidget {
  SubmissionList({
    @required this.submissionLessonContents,
    this.itemTapCallBack,
  });

  final List<LessonContentSubmission> submissionLessonContents;
  final Function(LessonContentSubmission submissionLessonContent)
      itemTapCallBack;

  @override
  Widget build(BuildContext context) {
    return !submissionLessonContents.isNullOrBlank
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final submissionLessonContent = submissionLessonContents[index];
              return Card(
                child: ListTile(
                  title: Text(submissionLessonContent.lessonContent.title),
                  subtitle:
                      Text(submissionLessonContent.lessonContent.description),
                  trailing: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        getTotalResultPoint(submissionLessonContent.submissions)
                                .toString() +
                            ' / ' +
                            getTotalQuestionPoint(
                                    submissionLessonContent.submissions)
                                .toString(),
                        style: TextStyle(
                          color: Colors.teal,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    itemTapCallBack(submissionLessonContent);
                  },
                ),
              );
            },
            itemCount: submissionLessonContents.length,
          )
        : EmptyState(
            title: 'empty_submissions_title'.tr,
            description: '',
          );
  }

  double getTotalQuestionPoint(List<QuestionSubmission> submissions) {
    double totalPoint = 0;
    for (QuestionSubmission submission in submissions) {
      if (submission.question != null) {
        totalPoint += submission.question.point;
      }
    }
    return totalPoint;
  }

  double getTotalResultPoint(List<QuestionSubmission> submissions) {
    double totalResultPoint = 0;
    for (QuestionSubmission submission in submissions) {
      if (submission.resultPoint != null) {
        totalResultPoint += submission.resultPoint;
      }
    }
    return totalResultPoint;
  }
}
