import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class StudentList extends StatelessWidget {
  StudentList({
    @required this.students,
    this.itemTapCallBack,
  });

  final List<Student> students;
  final Function(Student student) itemTapCallBack;

  @override
  Widget build(BuildContext context) {
    return !students.isNullOrBlank
        ? ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final student = students[index].profile;
              return Card(
                child: ListTile(
                  title: Text('${student.firstName} ${student.lastName}'),
                  leading: CircleAvatar(
                    radius: 20.0,
                    backgroundImage: AssetImage('assets/images/test2.jpeg'),
                  ),
                  onTap: () {
                    itemTapCallBack(students[index]);
                  },
                ),
              );
            },
            itemCount: students.length,
          )
        : EmptyState(
            title: 'empty_student_title'.tr,
            description: '',
          );
  }
}
