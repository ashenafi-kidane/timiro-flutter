import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/pages/course/components/question_tile.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class QuestionList extends StatelessWidget {
  QuestionList({
    @required this.questions,
    this.itemTapCallBack,
    this.emptyActionCallback,
    this.isInstructor = false,
    this.onAnswerCallback,
  });

  final List<Question> questions;
  final Function(LessonContent content) itemTapCallBack;
  final VoidCallback emptyActionCallback;
  final bool isInstructor;
  final Function(String answer, int index) onAnswerCallback;

  @override
  Widget build(BuildContext context) {
    return !questions.isNullOrBlank
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                final question = questions[index];
                return QuestionTile(
                  question: question,
                  index: index,
                  isInstructor: isInstructor,
                  onGiveAnswerCallback: (answer) =>
                      onAnswerCallback(answer, index),
                );
              },
              itemCount: questions.length,
            ),
          )
        : EmptyState(
            title: isInstructor
                ? 'empty_question_title'.tr
                : 'empty_question_title_student'.tr,
            description: isInstructor ? 'empty_question_description'.tr : '',
            action: isInstructor
                ? CustomButton(
                    text: 'add_question'.tr,
                    onPressed: emptyActionCallback,
                  )
                : SizedBox(),
          );
  }
}
