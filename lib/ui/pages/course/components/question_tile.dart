import 'package:app/core/enums/course_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/course/components/input_dialog.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuestionTile extends StatefulWidget {
  const QuestionTile({
    Key key,
    @required this.question,
    @required this.index,
    this.onMarkCallback,
    this.questionMode = QuestionMode.question,
    this.resultPoint,
    this.isInstructor = false,
    this.submissionAnswer,
    this.onGiveAnswerCallback,
  }) : super(key: key);

  final Question question;
  final int index;
  final VoidCallback onMarkCallback;
  final QuestionMode questionMode;
  final double resultPoint;
  final bool isInstructor;
  final String submissionAnswer;
  final Function(String answer) onGiveAnswerCallback;

  @override
  _QuestionTileState createState() => _QuestionTileState();
}

class _QuestionTileState extends State<QuestionTile> {
  String answer = '';
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
                color: AppTheme.darkColor,
              ),
              children: [
                TextSpan(
                  text: '${widget.index + 1}. ',
                ),
                TextSpan(
                  text:
                      '${widget.question.textQuestion} (${widget.question.point} pt.)',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    letterSpacing: 1.2,
                  ),
                ),
              ],
            ),
          ),
          (widget.question.questionType == 'MULTIPLE_CHOICE')
              ? Column(
                  children: widget.question.multipleChoices.map((item) {
                    return RadioListTile(
                      groupValue: widget.questionMode == QuestionMode.submission
                          ? widget.submissionAnswer
                          : widget.isInstructor
                              ? widget.question.textAnswer
                              : answer,
                      title: Text(
                        item,
                      ),
                      value: item,
                      activeColor:
                          widget.questionMode == QuestionMode.submission
                              ? widget.submissionAnswer ==
                                      widget.question.textAnswer
                                  ? Colors.blue
                                  : Colors.red
                              : Colors.blue,
                      onChanged: (val) {
                        // print(val);
                        if (!widget.isInstructor &&
                            widget.questionMode == QuestionMode.question) {
                          setState(() {
                            answer = val;
                            widget.onGiveAnswerCallback(val);
                          });
                        }
                      },
                    );
                  }).toList(),
                )
              : Container(),
          SizedBox(
            height: 8.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              (widget.isInstructor ||
                          widget.questionMode == QuestionMode.submission) &&
                      !widget.question.textAnswer.isNullOrBlank
                  ? RichText(
                      text: TextSpan(
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: AppTheme.primaryColor,
                          letterSpacing: 1.1,
                        ),
                        children: [
                          TextSpan(
                            text: 'Answer: ',
                          ),
                          TextSpan(
                            text: '${widget.question.textAnswer}',
                            style: TextStyle(
                              color: AppTheme.grey,
                            ),
                          ),
                        ],
                      ),
                    )
                  : (!widget.isInstructor &&
                          widget.questionMode == QuestionMode.question &&
                          !(widget.question.questionType == 'MULTIPLE_CHOICE'))
                      ? CustomButton(
                          text: 'give_answer'.tr,
                          width: 100,
                          onPressed: () async {
                            switch (widget.question.questionType) {
                              case 'SHORT_ANSWER':
                                //show input answer dialog
                                Get.dialog(InputDialog(
                                  title: 'give_answer',
                                  inputHint: 'answer',
                                  onDoneCallback: (answer) =>
                                      widget.onGiveAnswerCallback(answer),
                                ));
                                break;
                              case 'FILE_SUBMISSION':
                                //pick file answer.
                                //Todo: move the file picking logic to the controller
                                FilePickerResult result =
                                    await FilePicker.platform.pickFiles();
                                if (result != null) {
                                  widget.onGiveAnswerCallback(
                                      result.files.single.path);
                                  // File file = File(result.files.single.path);
                                  // Get.snackbar('File Pick',
                                  //     'File pick success! ${result.files.single.path}');
                                } else {
                                  // User canceled the picker
                                  Get.snackbar(
                                      'error_file_pick', 'error_file_pick');
                                }
                                break;
                              default:
                            }
                          },
                        )
                      : SizedBox(),
              widget.questionMode == QuestionMode.submission
                  ? GestureDetector(
                      onTap: widget.isInstructor ? widget.onMarkCallback : null,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          widget.resultPoint == null
                              ? '__ / ${widget.question.point}'
                              : '${widget.resultPoint} / ${widget.question.point}',
                          style: TextStyle(
                            color: Colors.teal,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }
}
