import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/widgets/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddArticleDialog extends StatelessWidget {
  AddArticleDialog({
    this.topTitle = 'title',
    this.callback,
  });

  final String topTitle;
  final Function(String input) callback;
  @override
  Widget build(BuildContext context) {
    String inputText;
    final GlobalKey key = GlobalKey<FormState>();
    return AlertDialog(
      title: Text(
        'add'.tr + ' ' + topTitle.tr,
        textAlign: TextAlign.center,
      ),
      content: SingleChildScrollView(
        child: Form(
          key: key,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                onSaved: (value) => inputText = value,
                decoration: AppTheme.textFormFieldDecoration.copyWith(
                  labelText: topTitle.tr,
                ),
                enableSuggestions: true,
                maxLines: topTitle == 'paragraph' ? 6 : 1,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) =>
                    value.trim().length < 1 ? 'error_input'.tr : null,
              ),
              SizedBox(
                height: 24.0,
              ),
              RoundedButton(
                onPressed: () {
                  final FormState form = key.currentState;
                  if (form.validate()) {
                    form.save();
                    callback(inputText);
                    Navigator.of(context).pop();
                  }
                },
                text: 'done'.tr,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
