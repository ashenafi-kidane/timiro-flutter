import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app/utils/utils.dart';

class InputDialog extends StatefulWidget {
  InputDialog({
    @required this.title,
    this.inputHint = '',
    this.onDoneCallback,
    this.totalPoint,
  });
  final String title;
  final String inputHint;
  final Function(String input) onDoneCallback;
  final double totalPoint;

  @override
  _InputDialogState createState() => _InputDialogState();
}

class _InputDialogState extends State<InputDialog> {
  TextEditingController _inputController = TextEditingController();
  String inputError;

  @override
  void dispose() {
    _inputController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        widget.title.tr,
        textAlign: TextAlign.center,
      ),
      content: TextFormField(
        controller: _inputController,
        keyboardType: widget.totalPoint.isNullOrBlank
            ? TextInputType.text
            : TextInputType.number,
        decoration: AppTheme.textFormFieldDecoration.copyWith(
          labelText: widget.inputHint.tr,
        ),
        // inputFormatters: <TextInputFormatter>[
        //   FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        // ],
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) => inputError,
      ),
      actions: [
        TextButton(
          child: Text(
            'cancel'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
            'done'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            // if (form.validate()) {}
            if (_inputController.text.isEmpty) {
              setState(() {
                inputError = 'error_input'.tr;
              });
            } else if (!widget.totalPoint.isNullOrBlank && !isPointValid()) {
              setState(() {
                inputError = 'error_point'.tr + widget.totalPoint.toString();
              });
            } else {
              setState(() {
                inputError = null;
              });
              widget.onDoneCallback(_inputController.text);
              Navigator.of(context).pop();
            }
          },
        ),
      ],
    );
  }

  bool isPointValid() {
    if (!isNumeric(_inputController.text))
      return false;
    else if (double.parse(_inputController.text) < 1 ||
        double.parse(_inputController.text) > widget.totalPoint) {
      return false;
    } else
      return true;
  }
}
