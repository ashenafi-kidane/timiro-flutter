// import 'package:flutter/material.dart';
// import 'package:ext_video_player/ext_video_player.dart';

// class CourseVideoPlayer extends StatefulWidget {
//   CourseVideoPlayer({@required this.url});
//   // final VideoItem videoItem;
//   final String url;
//   @override
//   _CourseVideoPlayerState createState() => _CourseVideoPlayerState();
// }

// class _CourseVideoPlayerState extends State<CourseVideoPlayer> {
//   VideoPlayerController _controller;

//   @override
//   void initState() {
//     super.initState();
//     // _controller = VideoPlayerController.network(
//     //   'https://www.youtube.com/watch?v=w8a8AWtsaOA',
//     // );

//     _controller = VideoPlayerController.network(
//       widget.url,
//     );

//     _controller.addListener(() {
//       setState(() {});
//     });
//     _controller.setLooping(true);
//     _controller.initialize();
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       child: Column(
//         children: [
//           // Container(padding: const EdgeInsets.symmetric(vertical: 8.0)),
//           Container(
//             // padding: const EdgeInsets.symmetric(vertical: 8.0),
//             child: AspectRatio(
//               aspectRatio: _controller.value.aspectRatio,
//               child: Stack(
//                 alignment: Alignment.bottomCenter,
//                 children: [
//                   VideoPlayer(_controller),
//                   ClosedCaption(text: _controller.value.caption.text),
//                   _PlayPauseOverlay(controller: _controller),
//                   VideoProgressIndicator(
//                     _controller,
//                     allowScrubbing: true,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

// class _PlayPauseOverlay extends StatelessWidget {
//   const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

//   final VideoPlayerController controller;

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         AnimatedSwitcher(
//           duration: Duration(milliseconds: 50),
//           reverseDuration: Duration(milliseconds: 200),
//           child: controller.value.isPlaying
//               ? SizedBox.shrink()
//               : Container(
//                   color: Colors.black26,
//                   child: Center(
//                     child: Icon(
//                       Icons.play_arrow,
//                       color: Colors.white,
//                       size: 100.0,
//                     ),
//                   ),
//                 ),
//         ),
//         GestureDetector(
//           onTap: () {
//             controller.value.isPlaying ? controller.pause() : controller.play();
//           },
//         ),
//       ],
//     );
//   }
// }
