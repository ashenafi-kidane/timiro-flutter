import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class LessonList extends StatelessWidget {
  LessonList({
    @required this.lessons,
    this.itemTapCallBack,
    this.emptyActionCallback,
    this.isInstructor = false,
  });

  final List<Lesson> lessons;
  final Function(Lesson lesson) itemTapCallBack;
  final VoidCallback emptyActionCallback;
  final bool isInstructor;

  @override
  Widget build(BuildContext context) {
    return !lessons.isNullOrBlank
        ? ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final lesson = lessons[index];
              return Card(
                child: ListTile(
                  title: Text(lesson.title),
                  subtitle: Text(lesson.description),
                  trailing: isInstructor
                      ? Icon(
                          Icons.delete,
                        )
                      : SizedBox(),
                  onTap: () {
                    itemTapCallBack(lesson);
                  },
                ),
              );
            },
            itemCount: lessons.length,
          )
        : EmptyState(
            title: isInstructor
                ? 'empty_lesson_title'.tr
                : 'empty_lesson_title_student'.tr,
            description: isInstructor ? 'empty_lesson_description'.tr : '',
            action: isInstructor
                ? CustomButton(
                    text: 'add_lesson'.tr,
                    onPressed: emptyActionCallback,
                  )
                : SizedBox(),
          );
  }
}
