import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/widgets/custom_chip.dart';
import 'package:flutter/material.dart';

class CategoriesList extends StatefulWidget {
  CategoriesList(
      {@required this.categories, this.selectedIndex, this.onTapCallback});

  final List<CourseCategory> categories;
  final int selectedIndex;
  final Function(int index) onTapCallback;

  @override
  _CategoriesListState createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList>
    with TickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: FutureBuilder(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final int count = widget.categories.length > 10
                    ? 10
                    : widget.categories.length;
                final Animation<double> animation =
                    Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                        parent: animationController,
                        curve: Interval((1 / count) * index, 1.0,
                            curve: Curves.fastOutSlowIn)));
                animationController.forward();
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: CustomChip(
                    index: index,
                    animation: animation,
                    animationController: animationController,
                    text: widget.categories[index].name,
                    isSelected: index == widget.selectedIndex,
                    onTapCallback: (value) => widget.onTapCallback(value),
                  ),
                );
              },
              itemCount: widget.categories.length,
            );
          }
        },
      ),
    );
  }
}
