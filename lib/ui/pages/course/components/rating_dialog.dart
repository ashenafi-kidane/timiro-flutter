import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class RatingDialog extends StatelessWidget {
  RatingDialog({
    this.canRate = true,
    this.value = 2.5,
    this.numberOfRates = 0,
    this.onRateCallback,
  });

  final bool canRate;
  final double value;
  final int numberOfRates;
  final Function(double rateValue) onRateCallback;
  @override
  Widget build(BuildContext context) {
    double rateValue = value;
    return AlertDialog(
      title: Column(
        children: [
          Icon(
            Icons.star,
            size: 100.0,
            color: AppTheme.primaryColor,
          ),
          Text(
            canRate ? 'rate_course'.tr : 'course_rate'.tr,
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: RatingBar(
              initialRating: value,
              minRating: 0,
              itemSize: 40,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: AppTheme.primaryColor,
              ),
              onRatingUpdate: (rating) {
                rateValue = rating;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              '$numberOfRates ' + 'rated'.tr,
              style: AppTheme.title.copyWith(
                color: Colors.grey,
                fontWeight: FontWeight.w400,
                fontSize: 18,
              ),
              textAlign: TextAlign.start,
            ),
          ),
        ],
      ),
      actions: [
        TextButton(
          child: Text(
            'cancel'.tr,
            style: TextStyle(
              fontFamily: AppTheme.fontName,
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              letterSpacing: 1,
              color: AppTheme.primaryColor,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        canRate
            ? TextButton(
                child: Text(
                  'submit'.tr,
                  style: TextStyle(
                    fontFamily: AppTheme.fontName,
                    fontWeight: FontWeight.w700,
                    fontSize: 18.0,
                    letterSpacing: 1,
                    color: AppTheme.primaryColor,
                  ),
                ),
                onPressed: () {
                  onRateCallback(rateValue);
                  Navigator.of(context).pop();
                },
              )
            : SizedBox(),
      ],
    );
  }
}
