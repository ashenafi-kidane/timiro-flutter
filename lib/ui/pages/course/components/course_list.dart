import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/pages/course/components/course_view.dart';
import 'package:app/ui/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CourseList extends StatefulWidget {
  const CourseList({
    Key key,
    @required this.courses,
    this.itemTapCallBack,
    this.emptyActionCallback,
    this.withAppBar = false,
  }) : super(key: key);

  final Function(Course course, int index) itemTapCallBack;
  final List<Course> courses;
  final VoidCallback emptyActionCallback;
  final bool withAppBar;
  @override
  _CourseListState createState() => _CourseListState();
}

class _CourseListState extends State<CourseList> with TickerProviderStateMixin {
  AnimationController animationController;
  final ScrollController scrollController = ScrollController();
  double topBarOpacity = 0.0;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: widget.withAppBar
              ? EdgeInsets.only(
                  top: AppBar().preferredSize.height +
                      MediaQuery.of(context).padding.top,
                  left: 18,
                  right: 16,
                  bottom: MediaQuery.of(context).padding.bottom,
                )
              : EdgeInsets.only(top: 8.0),
          // padding: const EdgeInsets.only(top: 8),
          child: FutureBuilder<bool>(
            future: getData(),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (!snapshot.hasData) {
                return const SizedBox();
              } else {
                return !widget.courses.isNullOrBlank
                    ? GridView(
                        controller: scrollController,
                        padding: const EdgeInsets.symmetric(vertical: 40.0),
                        physics: const BouncingScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        children: List.generate(
                          widget.courses.length,
                          (int index) {
                            final int count = widget.courses.length;
                            final Animation<double> animation =
                                Tween<double>(begin: 0.0, end: 1.0).animate(
                              CurvedAnimation(
                                parent: animationController,
                                curve: Interval((1 / count) * index, 1.0,
                                    curve: Curves.fastOutSlowIn),
                              ),
                            );
                            animationController.forward();
                            return CourseView(
                              callback: () {
                                widget.itemTapCallBack(
                                    widget.courses[index], index);
                              },
                              course: widget.courses[index],
                              animation: animation,
                              animationController: animationController,
                            );
                          },
                        ),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 32.0,
                          crossAxisSpacing: 32.0,
                          childAspectRatio: 0.8,
                        ),
                      )
                    : EmptyState(
                        title: 'empty_course_title'.tr,
                        description: 'empty_course_description'.tr,
                        action: TextButton(
                          child: Text(
                            'click_here'.tr,
                            style: TextStyle(
                              color: AppTheme.nearlyBlue,
                            ),
                          ),
                          onPressed: widget.emptyActionCallback,
                        ),
                      );
              }
            },
          ),
        ),
        widget.withAppBar
            ? CustomAppBar(
                title: 'your_courses'.tr,
                topBarOpacity: topBarOpacity,
              )
            : Container(),
      ],
    );
  }
}
