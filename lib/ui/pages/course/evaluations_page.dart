import 'package:app/core/enums/course_enums.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/controllers/submission_controller.dart';
import 'package:app/ui/pages/course/components/input_dialog.dart';
import 'package:app/ui/pages/course/components/question_tile.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EvaluationPage extends GetView<SubmissionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        actions: [
          CustomButton(
            text: 'done'.tr,
            width: 80,
            onPressed: () {
              //Todo: submit evaluation. (you can add change check here)
              controller.submitEvaluation();
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Obx(
          () => ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final submission =
                  controller.selectedSubmissionContent.submissions[index];
              return QuestionTile(
                question: submission.question,
                isInstructor: true,
                index: index,
                questionMode: QuestionMode.submission,
                submissionAnswer: submission.textAnswer,
                resultPoint: submission.resultPoint,
                onMarkCallback: () {
                  //Todo: show give mark dialog (only for instructors)
                  Get.dialog(InputDialog(
                    title: 'give_point',
                    inputHint: 'point',
                    totalPoint: submission.question.point,
                    onDoneCallback: (input) {
                      //Todo: set mark here.
                      controller.updatePoint(input, index);
                    },
                  ));
                },
              );
            },
            itemCount: controller.selectedSubmissionContent.submissions.length,
          ),
        ),
      ),
    );
  }
}
