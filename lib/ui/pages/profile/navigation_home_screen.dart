import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/pages/common/about_us_page.dart';
import 'package:app/ui/pages/common/custom_drawer/drawer_user_controller.dart';
import 'package:app/ui/pages/common/custom_drawer/home_drawer.dart';
import 'package:app/ui/pages/common/empty_state.dart';
import 'package:app/ui/pages/common/feedback_dialog.dart';
import 'package:app/ui/pages/common/language_dialog.dart';
import 'package:app/ui/pages/profile/profile_page.dart';
import 'package:app/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:get/get.dart';

class NavigationHomeScreen extends StatefulWidget {
  const NavigationHomeScreen({Key key, this.animationController})
      : super(key: key);
  final AnimationController animationController;
  @override
  _NavigationHomeScreenState createState() => _NavigationHomeScreenState();
}

class _NavigationHomeScreenState extends State<NavigationHomeScreen> {
  Widget screenView;
  DrawerIndex drawerIndex;

  @override
  void initState() {
    drawerIndex = DrawerIndex.PROFILE;
    screenView = ProfilePage(
      animationController: widget.animationController,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.nearlyWhite,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppTheme.nearlyWhite,
          body: DrawerUserController(
            screenIndex: drawerIndex,
            drawerWidth: MediaQuery.of(context).size.width * 0.75,
            onDrawerCall: (DrawerIndex drawerIndexdata) {
              changeIndex(drawerIndexdata);
              //callback from drawer for replace screen as user need with passing DrawerIndex(Enum index)
            },
            screenView: screenView,
            //we replace screen view as we need on navigate starting screens like MyHomePage, HelpScreen, FeedbackScreen, etc...
          ),
        ),
      ),
    );
  }

  void changeIndex(DrawerIndex drawerIndexdata) {
    if (drawerIndex != drawerIndexdata ||
        (drawerIndex == DrawerIndex.FeedBack ||
            drawerIndex == DrawerIndex.Invite ||
            drawerIndex == DrawerIndex.Language)) {
      drawerIndex = drawerIndexdata;
      if (drawerIndex == DrawerIndex.PROFILE) {
        setState(() {
          screenView = ProfilePage(
            animationController: widget.animationController,
          );
        });
      } else if (drawerIndex == DrawerIndex.Help) {
        setState(() {
          screenView = EmptyState(
            title: 'help_title'.tr,
            description: 'help_info'.tr,
            action: CustomButton(
              text: 'chat_with_us'.tr,
              onPressed: () {
                //Todo: show chatting pages with us or feedback.
              },
            ),
          );
        });
      } else if (drawerIndex == DrawerIndex.FeedBack) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return FeedbackDialog();
          },
        );
      } else if (drawerIndex == DrawerIndex.About) {
        setState(() {
          screenView = AboutUsPage();
        });
      } else if (drawerIndex == DrawerIndex.Invite) {
        Share.share('share_info'.tr, subject: 'share_subject'.tr);
      } else if (drawerIndex == DrawerIndex.Language) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return LanguageDialog();
          },
        );
      }
    }
  }
}
