import 'package:flutter/material.dart';

class InfoTile extends StatelessWidget {
  InfoTile({
    @required this.text,
    this.hintText,
    this.icon,
    this.canEdit = false,
    this.onChanged,
  });

  final String text;
  final String hintText;
  final IconData icon;
  final bool canEdit;
  final Function(String value) onChanged;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      elevation: 5.0,
      child: ListTile(
        leading: Icon(
          icon,
          color: Colors.teal,
        ),
        title: TextFormField(
          initialValue: text,
          enabled: canEdit,
          decoration: InputDecoration(
            hintText: hintText,
            border: InputBorder.none,
          ),
          onChanged: (value) => onChanged(value),
        ),
      ),
    );
  }
}
