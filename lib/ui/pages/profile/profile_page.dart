import 'package:app/core/enums/common_enums.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/profile_controller.dart';
import 'package:app/ui/pages/profile/components/info_tile.dart';
import 'package:app/ui/widgets/home_animation.dart';
import 'package:app/ui/widgets/image_picker_bottomsheet.dart';
import 'package:app/ui/widgets/profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ProfilePage extends GetView<ProfileController> {
  ProfilePage(
      {Key key, @required this.animationController, this.studentProfile})
      : super(key: key);

  final AnimationController animationController;
  final User studentProfile;
  final ProfileController controller = Get.find();
  List<Widget> listViews = [];
  final viewCount = 6;

  @override
  Widget build(BuildContext context) {
    controller.getUser();
    controller.setProfileValues(studentProfile);
    getViews();
    return Scaffold(
      floatingActionButton: Obx(
        () => controller.isProfileEdited && studentProfile == null
            ? FloatingActionButton(
                child: controller.status.value == Status.loading
                    ? Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: CircularProgressIndicator(
                            backgroundColor: Colors.white),
                      )
                    : Text('save'.tr),
                onPressed: () => controller.status.value == Status.loading
                    ? null
                    : controller.editProfile(),
              )
            : Container(),
      ),
      body: ListView.builder(
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom,
        ),
        itemCount: listViews.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          animationController.forward();
          return listViews[index];
        },
      ),
    );
  }

  void getViews() {
    listViews = [
      HomeAnimation(
        animationController: animationController,
        viewCount: viewCount,
        index: 0,
        view: Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              width: double.infinity,
              height: Get.height * 0.32,
            ),
            Container(
              decoration: BoxDecoration(
                color: AppTheme.background,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                ),
              ),
              width: double.infinity,
              height: Get.height * 0.20,
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Obx(
                () => ProfileAvatar(
                  name: controller.profile.firstName +
                      ' ' +
                      controller.profile.lastName,
                  pickedFilePath:
                      studentProfile == null ? controller.pickedFilePath : '',
                  onTapCallback: () {
                    if (studentProfile == null) {
                      Get.bottomSheet(
                        ImagePickerBottomsheet(
                          onPickFromCallback: (imageSource) =>
                              controller.getImage(imageSource),
                        ),
                        backgroundColor: Colors.white,
                      );
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
      HomeAnimation(
        animationController: animationController,
        viewCount: viewCount,
        index: 1,
        view: Text(
          controller.profile.firstName + ' ' + controller.profile.lastName,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: AppTheme.primaryColor,
            fontSize: 40.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'Pacifico',
          ),
        ),
      ),
      HomeAnimation(
        animationController: animationController,
        viewCount: viewCount,
        index: 2,
        view: Column(
          children: [
            Text(
              studentProfile == null
                  ? 'instructor'.tr.toUpperCase()
                  : 'student'.tr.toUpperCase(),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.teal,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Source Sans Pro',
                  letterSpacing: 2.5),
            ),
            SizedBox(
              height: 20.0,
              width: 150.0,
              child: Divider(
                color: Colors.teal,
              ),
            ),
          ],
        ),
      ),
      HomeAnimation(
        animationController: animationController,
        viewCount: viewCount,
        index: 3,
        view: InfoTile(
          text: controller.profile.email,
          icon: Icons.email,
        ),
      ),
      (studentProfile == null || studentProfile.phone != null)
          ? HomeAnimation(
              animationController: animationController,
              viewCount: viewCount,
              index: 4,
              view: InfoTile(
                text: controller.profile.phone ?? null,
                hintText: controller.profile.phone?.isEmpty ?? true
                    ? 'Your Phone No.'
                    : null,
                canEdit: studentProfile == null,
                icon: Icons.phone,
                onChanged: (value) {
                  controller.phone = value;
                  controller.checkProfileChanged();
                },
              ),
            )
          : SizedBox(),
      (studentProfile == null || studentProfile.address != null)
          ? HomeAnimation(
              animationController: animationController,
              viewCount: viewCount,
              index: 5,
              view: InfoTile(
                text: controller.profile.address ?? null,
                hintText: controller.profile.phone?.isEmpty ?? true
                    ? 'Your address'
                    : null,
                canEdit: studentProfile == null,
                icon: Icons.place,
                onChanged: (value) {
                  controller.address = value;
                  controller.checkProfileChanged();
                },
              ),
            )
          : SizedBox(),
    ];
  }
}
