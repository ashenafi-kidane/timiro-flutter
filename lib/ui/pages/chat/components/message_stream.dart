import 'package:app/core/models/freezed_models.dart';
import 'package:app/ui/pages/chat/components/message_bubble.dart';
import 'package:flutter/material.dart';

class MessageStream extends StatelessWidget {
  MessageStream({this.messages});
  final List<Message> messages;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        reverse: true,
        itemBuilder: (contxt, index) {
          final message = messages[index];
          return MessageBubble(
            message: message,
          );
        },
        itemCount: messages.length,
      ),
    );
  }
}

// List<MessageBubble> messageBubbles = [];
//     for (var message in messages) {
//       final messageBubble = MessageBubble(
//         message: message,
//       );
//       messageBubbles.add(messageBubble);
//     }
//     return Expanded(
//       child: ListView(
//         reverse: true,
//         children: messageBubbles,
//       ),
//     );
// class MessageStream extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return StreamBuilder<QuerySnapshot>(
//         stream: _fireStore.collection('messages').snapshots(),
//         builder: (context, snapshot) {
//           if (!snapshot.hasData) {
//             return Center(
//               child: CircularProgressIndicator(
//                 backgroundColor: Colors.lightBlueAccent,
//               ),
//             );
//           }

//           final messages = snapshot.data.docs.reversed;
//           List<MessageBubble> messageBubbles = [];
//           for (var message in messages) {
//             final messageText = message.data()['text'];
//             final messageSender = message.data()['sender'];
//             final currentUser = loggedInUser.email;

//             final messageBubble = MessageBubble(
//               text: messageText,
//               sender: messageSender,
//               isMe: currentUser == messageSender,
//             );
//             messageBubbles.add(messageBubble);
//           }
//           return Expanded(
//             child: ListView(
//               reverse: true,
//               children: messageBubbles,
//             ),
//           );
//         });
//   }
// }
