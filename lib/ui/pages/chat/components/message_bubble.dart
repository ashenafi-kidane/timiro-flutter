import 'package:flutter/material.dart';
import 'package:app/core/models/freezed_models.dart';
import 'package:get/get.dart';

class MessageBubble extends StatelessWidget {
  MessageBubble({this.message});
  final Message message;
  @override
  Widget build(BuildContext context) {
    final currentUserId = 'ash@test.com';
    final isMe = currentUserId == message.sender;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Wrap(
        alignment: isMe ? WrapAlignment.end : WrapAlignment.start,
        crossAxisAlignment: WrapCrossAlignment.end,
        children: [
          isMe
              ? Container()
              : CircleAvatar(
                  radius: 20.0,
                  backgroundImage: AssetImage('assets/images/test2.jpeg'),
                ),
          Column(
            crossAxisAlignment:
                isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              !message.fileId.isNullOrBlank
                  ? Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Image.asset(
                        'assets/images/userImage.png',
                        height: 200,
                        width: 250,
                        fit: BoxFit.fill,
                      ),
                    )
                  : SizedBox(),
              !message.text.isNullOrBlank
                  ? Padding(
                      padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                      child: Material(
                        elevation: 5.0,
                        borderRadius: isMe
                            ? BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                topRight: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0),
                              )
                            : BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                topRight: Radius.circular(15.0),
                                bottomRight: Radius.circular(15.0),
                              ),
                        color: isMe ? Colors.lightBlueAccent : Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 10.0,
                            horizontal: 20.0,
                          ),
                          child: Text(
                            message.text,
                            style: TextStyle(
                              color: isMe ? Colors.white : Colors.black54,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }
}
