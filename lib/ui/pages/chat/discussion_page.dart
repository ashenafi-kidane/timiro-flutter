import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/controllers/chat_controller.dart';
import 'package:app/ui/pages/chat/components/message_stream.dart';
import 'package:app/ui/widgets/image_picker_bottomsheet.dart';
import 'package:app/ui/widgets/white_appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DiscussionPage extends GetView<ChatController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        title: 'discussion'.tr,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Obx(() =>
                MessageStream(messages: controller.messages.reversed.toList())),
            Container(
              decoration: BoxDecoration(
                color: AppTheme.primaryLightColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Icon(
                        Icons.photo,
                        color: AppTheme.grey,
                      ),
                    ),
                    onTap: () => Get.bottomSheet(
                      ImagePickerBottomsheet(
                        onPickFromCallback: (imageSource) =>
                            controller.getImage(imageSource),
                      ),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: controller.messageTextController,
                      onChanged: (value) {
                        controller.messageText = value;
                      },
                      decoration: AppTheme.messageTextFieldDecoration,
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      //send message
                      controller.sendMessage();
                    },
                    child: Icon(
                      Icons.send,
                      color: Colors.lightBlueAccent,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
