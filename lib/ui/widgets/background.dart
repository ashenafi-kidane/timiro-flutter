import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  Background({this.asset, this.body});
  final String asset;
  final Widget body;
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      decoration: asset != null
          ? BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage(asset),
                fit: BoxFit.cover,
              ),
            )
          : BoxDecoration(
              color: Colors.white,
            ),
      child: body,
    );
  }
}
