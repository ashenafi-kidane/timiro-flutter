import 'package:app/shared/theme/app_theme.dart';
import 'package:app/ui/widgets/text_field_container.dart';
import 'package:flutter/material.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final TextInputType keyboardType;
  final bool obscureText;
  final ValueChanged<String> onChanged;
  final InputDecoration decoration;
  final Function onSaved;
  final Function validator;

  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.onChanged,
    this.decoration,
    this.onSaved,
    this.validator,
  }) : super(key: key);

  InputDecoration getDecoration() {
    return decoration ??
        InputDecoration(
          icon: icon != null
              ? Icon(
                  icon,
                  color: AppTheme.primaryColor,
                )
              : null,
          hintText: hintText,
          border: InputBorder.none,
        );
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        onChanged: onChanged,
        onSaved: onSaved,
        validator: validator,
        cursorColor: AppTheme.primaryColor,
        enableSuggestions: true,
        obscureText: this.obscureText,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: getDecoration(),
      ),
    );
  }
}
