import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';

class CustomChip extends StatelessWidget {
  CustomChip({
    @required this.text,
    this.index,
    this.isSelected = false,
    this.onTapCallback,
    this.animationController,
    this.animation,
  });

  final int index;
  final String text;
  final bool isSelected;
  final Function(int index) onTapCallback;
  final AnimationController animationController;
  final Animation<dynamic> animation;
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                100 * (1.0 - animation.value), 0.0, 0.0),
            child: GestureDetector(
              onTap: () => onTapCallback(index),
              child: Container(
                decoration: BoxDecoration(
                    color:
                        isSelected ? AppTheme.nearlyBlue : AppTheme.nearlyWhite,
                    borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                    border: Border.all(color: AppTheme.nearlyBlue)),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    splashColor: Colors.white24,
                    borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 12, bottom: 12, left: 18, right: 18),
                      child: Center(
                        child: Text(
                          text,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 12,
                            letterSpacing: 0.27,
                            color: isSelected
                                ? AppTheme.nearlyWhite
                                : AppTheme.nearlyBlue,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
