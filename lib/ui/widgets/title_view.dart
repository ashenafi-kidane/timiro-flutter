import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TitleView extends StatelessWidget {
  const TitleView({
    Key key,
    @required this.title,
    this.subTitle,
  }) : super(key: key);

  final String title;
  final String subTitle;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              title.tr,
              style: AppTheme.title,
            ),
          ),
          subTitle != null
              ? Text(
                  subTitle.tr,
                  style: AppTheme.subtitle,
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
