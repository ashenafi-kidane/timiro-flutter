import 'package:flutter/material.dart';

class HomeAnimation extends StatelessWidget {
  const HomeAnimation({
    @required this.animationController,
    @required this.viewCount,
    @required this.index,
    @required this.view,
  }) : assert(viewCount != 0);

  final AnimationController animationController;
  final int viewCount;
  final int index;
  final Widget view;

  @override
  Widget build(BuildContext context) {
    Animation animation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval((1 / viewCount) * index, 1.0,
                curve: Curves.fastOutSlowIn)));
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 30 * (1.0 - animation.value), 0.0),
            child: view,
          ),
        );
      },
    );
  }
}
