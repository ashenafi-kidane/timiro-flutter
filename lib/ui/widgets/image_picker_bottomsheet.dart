import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerBottomsheet extends StatelessWidget {
  ImagePickerBottomsheet({this.onPickFromCallback});

  final Function(ImageSource imageSource) onPickFromCallback;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff757575),
      height: 180,
      child: Container(
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
          bottom: 20.0,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'pick_image_title'.tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.teal,
                    letterSpacing: 1.5,
                    fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.camera,
                color: Colors.teal,
              ),
              title: Text('camera'.tr),
              onTap: () {
                Navigator.pop(context);
                onPickFromCallback(ImageSource.camera);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.photo_album,
                color: Colors.teal,
              ),
              title: Text('gallery'.tr),
              onTap: () {
                Navigator.pop(context);
                onPickFromCallback(ImageSource.gallery);
              },
            ),
          ],
        ),
      ),
    );
  }
}
