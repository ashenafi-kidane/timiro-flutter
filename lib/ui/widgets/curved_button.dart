import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';

class CurvedButton extends StatelessWidget {
  const CurvedButton({
    this.text,
    this.color = AppTheme.nearlyBlue,
    this.width = 140,
    @required this.onPressed,
  });

  final String text;
  final Color color;
  final double width;
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 48,
        width: width,
        decoration: BoxDecoration(
          color: color,
          borderRadius: const BorderRadius.all(
            Radius.circular(16.0),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: AppTheme.nearlyBlue.withOpacity(0.5),
                offset: const Offset(1.1, 1.1),
                blurRadius: 10.0),
          ],
        ),
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
              letterSpacing: 0.0,
              color: AppTheme.nearlyWhite,
            ),
          ),
        ),
      ),
    );
  }
}
