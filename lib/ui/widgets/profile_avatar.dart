import 'dart:io';
import 'package:app/shared/theme/app_theme.dart';
import 'package:app/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar({
    Key key,
    this.name,
    this.pickedFilePath,
    this.onTapCallback,
    this.imageUri,
  }) : super(key: key);

  final String name;
  final String pickedFilePath;
  final Function onTapCallback;
  final String imageUri;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCallback,
      child: CircleAvatar(
        radius: 65.0,
        backgroundColor: Colors.white,
        child: CircleAvatar(
          radius: 50.0,
          backgroundColor: AppTheme.primaryColor,
          child: !pickedFilePath.isNullOrBlank
              ? CircleAvatar(
                  radius: 50.0,
                  backgroundImage: kIsWeb
                      ? NetworkImage(pickedFilePath)
                      : FileImage(File(pickedFilePath)),
                )
              : !imageUri.isNullOrBlank
                  ? CircleAvatar(
                      radius: 50.0,
                      backgroundImage: AssetImage(imageUri),
                    )
                  : Text(
                      getInitials(name),
                      style: TextStyle(
                        fontSize: 40.0,
                      ),
                    ),
        ),
      ),
    );
  }
}
