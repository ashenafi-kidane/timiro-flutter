import 'package:app/shared/theme/app_theme.dart';
import 'package:flutter/material.dart';

class WhiteAppBar extends StatelessWidget implements PreferredSizeWidget {
  const WhiteAppBar({
    Key key,
    this.title = '',
    this.actions,
    this.avator,
    this.onTitleTapCallback,
  }) : super(key: key);

  final String title;
  final List<Widget> actions;
  final Widget avator;
  final VoidCallback onTitleTapCallback;
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: GestureDetector(
        onTap: onTitleTapCallback,
        child: Row(
          children: [
            avator != null ? avator : Container(),
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                  color: AppTheme.darkerText,
                ),
              ),
            ),
          ],
        ),
      ),
      elevation: 0.0,
      actions: actions,
      backgroundColor: Colors.transparent,
      brightness: Brightness.light,
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
    );
  }
}
