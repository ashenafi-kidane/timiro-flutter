import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/repository/auth_repository.dart';
import 'package:app/core/services/api.dart';
import 'package:app/ui/controllers/auth_controller.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';

class AuthBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Dio());
    Get.lazyPut(() => ApiClient(dio: Get.find()));
    Get.lazyPut<IAuthRepository>(() => AuthRepository(apiClient: Get.find()));
    Get.lazyPut(() => AuthController(repository: Get.find()));
  }
}
