import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/repository/main_repository.dart';
import 'package:app/core/services/api.dart';
import 'package:app/ui/controllers/chat_controller.dart';
import 'package:app/ui/controllers/courses_cotroller.dart';
import 'package:app/ui/controllers/home_controller.dart';
import 'package:app/ui/controllers/profile_controller.dart';
import 'package:app/ui/controllers/submission_controller.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';

class MainBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Dio());
    Get.lazyPut(() => ApiClient(dio: Get.find()));
    Get.lazyPut<IMainRepository>(() => MainRepository(apiClient: Get.find()));
    Get.lazyPut(() => ProfileController(repository: Get.find()));
    Get.lazyPut(() => HomeController(repository: Get.find()));
    Get.lazyPut(() => CoursesController(repository: Get.find()));
    Get.lazyPut(() => SubmissionController(repository: Get.find()));
    Get.lazyPut(() => ChatController(repository: Get.find()));
  }
}
