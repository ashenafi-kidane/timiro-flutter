import 'package:app/core/adapters/repository_adapter.dart';
import 'package:app/core/repository/main_repository.dart';
import 'package:app/core/services/api.dart';
import 'package:app/ui/controllers/home_controller.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Dio());
    Get.lazyPut(() => ApiClient(dio: Get.find()));
    Get.lazyPut<IMainRepository>(() => MainRepository(apiClient: Get.find()));
    Get.lazyPut(() => HomeController(repository: Get.find()));
  }
}
