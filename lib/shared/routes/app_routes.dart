part of 'app_pages.dart';

abstract class Routes {
  //Auth Routes
  static const INITIAL = '/';
  static const WELCOME = '/welcome';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const VERIFY = '/verify';

  //main routes
  static const MAIN = '/main';

  //course routes
  static const COURSEINFO = '/courseInfo';
  static const CREATECOURSE = '/createCourse';
  static const COURSECONTENT = '/courseContent';
  static const COURSELESSON = '/courseLesson';

  static const COURSEVIDEO = '/courseVideo';
  static const ARTICLE = '/article';
  static const QUESTIONS = '/questions';

  static const COURSEPROFILE = '/courseProfile';
  static const DISCUSSION = '/discussion';
  static const STUDENTS = '/students';

  static const SUBMISSIONS = '/submissions';
  static const SUBMISSION_LESSONS = '/submissionLessons';
  static const EVALUATIONS = '/evaluations';

  static const RESULTS = '/results';
}
