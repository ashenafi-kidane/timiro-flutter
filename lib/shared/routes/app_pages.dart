import 'package:app/ui/bindings/auth_binding.dart';
import 'package:app/ui/bindings/home_binding.dart';
import 'package:app/ui/bindings/main_binding.dart';
import 'package:app/ui/pages/auth/initial_page.dart';
import 'package:app/ui/pages/auth/login_page.dart';
import 'package:app/ui/pages/auth/signup_page.dart';
import 'package:app/ui/pages/auth/verification_page.dart';
import 'package:app/ui/pages/auth/welcome_page.dart';
import 'package:app/ui/pages/course/article_page.dart';
import 'package:app/ui/pages/course/contents_page.dart';
import 'package:app/ui/pages/course/course_profile_page.dart';
import 'package:app/ui/pages/course/create_course_page.dart';
import 'package:app/ui/pages/chat/discussion_page.dart';
import 'package:app/ui/pages/course/evaluations_page.dart';
import 'package:app/ui/pages/course/lessons_page.dart';
import 'package:app/ui/pages/course/questions_page.dart';
import 'package:app/ui/pages/course/results_page.dart';
import 'package:app/ui/pages/course/students_page.dart';
import 'package:app/ui/pages/course/submission_lessons_page.dart';
import 'package:app/ui/pages/course/submissions_page.dart';
import 'package:app/ui/pages/course/video_page.dart';
import 'package:app/ui/pages/home/home_page.dart';
import 'package:app/ui/pages/main/main_page.dart';
import 'package:app/ui/pages/profile/profile_page.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

abstract class AppPages {
  static final pages = [
    //Auth Pages
    GetPage(
      name: Routes.INITIAL,
      page: () => InitialPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.WELCOME,
      page: () => WelcomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.SIGNUP,
      page: () => SignupPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.VERIFY,
      page: () => VerificationPage(),
      binding: AuthBinding(),
    ),

    //main pages
    GetPage(
      name: Routes.MAIN,
      page: () => MainPage(),
      binding: MainBinding(),
    ),

    //course pages
    // GetPage(
    //   name: Routes.COURSEINFO,
    //   page: () => CourseInfoPage(),
    // ),
    // course pages
    GetPage(
      name: Routes.CREATECOURSE,
      page: () => CreateCoursePage(),
    ),
    GetPage(
      name: Routes.COURSELESSON,
      page: () => LessonsPage(),
    ),
    GetPage(
      name: Routes.COURSECONTENT,
      page: () => ContentsPage(),
    ),

    GetPage(
      name: Routes.COURSEVIDEO,
      page: () => VideoPage(),
    ),
    GetPage(
      name: Routes.ARTICLE,
      page: () => ArticlePage(),
    ),
    GetPage(
      name: Routes.QUESTIONS,
      page: () => QuestionsPage(),
    ),

    GetPage(
      name: Routes.COURSEPROFILE,
      page: () => CourseProfilePage(),
      binding: MainBinding(),
    ),
    GetPage(
      name: Routes.DISCUSSION,
      page: () => DiscussionPage(),
    ),
    GetPage(
      name: Routes.STUDENTS,
      page: () => Studentspage(),
    ),
    GetPage(
      name: Routes.SUBMISSION_LESSONS,
      page: () => SubmissionLessonPage(),
    ),
    GetPage(
      name: Routes.SUBMISSIONS,
      page: () => SubmissionsPage(),
    ),

    GetPage(
      name: Routes.EVALUATIONS,
      page: () => EvaluationPage(),
    ),

    GetPage(
      name: Routes.RESULTS,
      page: () => ResultsPage(),
    ),
  ];
}
