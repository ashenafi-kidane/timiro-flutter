final Map<String, String> enUs = {
  'app_name': 'Timiro',

  //Auth Strings
  'login': 'Login',
  'signup': 'SignUp',
  'fullname': 'Full name',

  'email': 'Email',
  'error_email': 'Invalid email address',
  'password': 'Password',
  'confirm_password': 'Confirm password',

  'already_memeber': 'Already a member? Login',
  'language': 'Language',
  'forgot_password': 'forgot password?',

  'not_memeber': 'Not a member? Signup',
  'invalid_password':
      'A password should be at least 5 characters long and contain at least 1 number',
  'error_name': 'Please add full name',

  'error_confirm_password': 'Password did not match',
  'verification_info': 'Please check your email for your Verification Code',
  'hint_verification_code': '6 digit code',
  'resend_verification': 'Resend Verification code?',
  'submit': 'Submit',
  'cancel': 'Cancel',

  //Home Strings
  'pick_image_title': 'Pick image from',
  'camera': 'Camera',
  'gallery': 'Gallery',

  //main strings
  'home': 'Home',
  'course': 'Courses',
  'add': 'Add',

  'done': 'Done',
  'setting': 'Setting',
  'profile': 'Profile',

  //course Strings
  'join_course': 'Join Course',
  'empty_course_title': 'You have no courses\nyet ..',
  'empty_course_description':
      'If you are here to take courses go to the course screen and enroll to one.\nIf you\'re thinking of giving out a course',

  'error_unauthorized': 'Please sign in first',
  'course_enroll_success': 'You have successfully enrolled to this course!',

  'click_here': 'Click here',
  'create_a_course': 'Create a course',
  'course_title': 'Course title',
  'course_description': 'Course description',

  'category': 'Category',
  'error_input': 'Input required!',
  'invalid_code': 'Invalid Code',
  'select_category': 'Select category',
  'create_course': 'Create course',

  'empty_lesson_title': 'You have not added \na lesson yet',
  'empty_lesson_title_student': 'There are no lessons \nin this course yet',
  'empty_lesson_description':
      'A lesson is a Topic in your course which talks about specific content of the course. You can create one here',

  'lessons': 'Lessons',
  'add_lesson': 'Add Lesson',
  'lesson': 'Lesson',
  'lesson_title': 'Lesson tittle',
  'lesson_description': 'Lesson description(optional)',
  'lesson_info':
      'A lesson is a Topic in a course which talks about specific content of the course.',

  'empty_content_title': 'You have not added \na content yet',
  'empty_content_title_student': 'There is no content \nin this lesson yet',
  'empty_content_description':
      'A content is a way for you describe the topics in a more detailed way. You can create one here.',
  'add_content': 'Add content',

  'title': 'title',
  'description': 'description',
  'description_optional': 'description (optional)',
  'your_courses': 'Your courses',

  'lesson_contents': 'Lesson contents',
  'contents_info':
      'A content is a way for you describe the topics in amore detailed way.',

  'pick_content_title': 'Choose content type',
  'video': 'Video',
  'article': 'Article',
  'questions': 'Questions',

  'add_video_url': 'Add video url',
  'video_url_hint': 'video url',
  'or_upload_from': 'OR upload from',

  'empty_article_title': 'You have not added \na content yet',
  'empty_article_title_viewer': 'The article is empty.',
  'empty_article_description':
      'An article is a way of giving course using text paragraphs and images. You can create one here.',
  'add_article': 'Add article',

  'pick_article_title': 'Choose article content',
  'paragraph': 'Paragraph',
  'image': 'Image',

  'empty_question_title': 'You have not added \nquestions yet',
  'empty_question_title_student': 'No questions yet',
  'empty_question_description': 'And You can create one here.',
  'add_question': 'Add question',

  'true_false': 'True/False',
  'multiple_choice': 'Multiple Choice',
  'short_answer': 'Short Answer',
  'file_submission': 'File Submission',

  'error_question_type': 'Please select queston type',
  'enter_value': 'Enter value',
  'add_choices': 'Add choices',
  'error_choices': 'Please add atleast two choices',
  'error_selection': 'Please select the answer',
  'point': 'Point',
  'error': 'Error',

  'add_short_answer': 'Add short answer',
  'short_answer': 'Short answer(optional)',
  'attach_answer': 'Attach Answer',
  'pick_file': 'Pick file',

  'video_error': 'Video Error',
  'video_error_info': 'Please add video url or upload a video.',

  'rating': 'Rating',
  'instructor': 'Instructor',
  'discussion': 'Discussion',
  'message': 'Message',

  'help': 'Help',
  'feedback': 'FeedBack',
  'invite_friend': 'Invite Friend',
  'rate_app': 'Rate the app',
  'about_us': 'About Us',
  'logout': 'Logout',

  'help_title': 'How can we help you?',
  'help_info':
      'It looks like you are experiencing problems. \nWe are here to help so \nplease get in touch with us',
  'chat_with_us': 'Chat with us',

  'leave_feedback': 'Leave a feedback',
  'rate_course': 'Rate the course',
  'course_rate': 'Course Rate',

  'rating_succes': 'Rating success',
  'course_rating_succes': 'You have succefully rated the course!',
  'rated': 'rated.',
  'tell_us_more': 'Tell us more',

  'share_subject': 'Share App!',
  'share_info': 'check out this app https://example.com',
  'about_us_info': 'We are here to ease the \n the eduction process!',

  'logging_out': 'Loging out ...',
  'are_you_sure': 'Are you sure?',
  'change_language': 'Change Language',

  'select_language': 'Select Language',
  'continue': 'Continue',
  'congratulations': 'Congratulations!',
  'course_success_info': 'You have created your course successfully!',

  'students': 'Students',
  'students_info':
      'Here are students who have subscribed to this course. You can checkout their submissions for any exams.',

  'submissions': 'Submissions',
  'submissions_info': 'List of this students submission for you to evaluate.',

  'empty_student_title':
      'There are no students enrolled \n in this course yet.',
  'empty_submissions_title': 'There are no submissions for this user yet',
  'error_point': 'Point should be betwen 0 and ',
  'give_point': 'Give Point',

  'give_answer': 'Give answer',
  'answer': 'Answer',
  'error_file_pick': 'File pick Error',
  'submit_answer': 'Submit answer',
  'error_submit_answer': 'Please answer all questions.',

  'enrolled': 'Enrolled',
  'add_instructor': 'Add Instructor',
  'price': 'Price',
  'save': 'Save',

  'error_course_title': 'Invalid course title',
  'error_course_description': 'Invalid course description',
  'error_course_price': 'Invalid course price',

  'student': 'Student',
  'submitted_lessons': 'Submitted for lessons',
  'submitted_lessons_info':
      'The user have submitted for the following lessons.',

  'submission_info':
      'This is a one time step. Make sure you have put your answers correctly!',
  'submision_success': 'You have successfully submitted!',
};
